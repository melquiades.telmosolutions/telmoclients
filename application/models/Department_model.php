<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Department_model extends App_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get_department()
    {
        return $this->db->get(db_prefix() . 'department')->result_array();
    }

    public function insert_dept($data=NULL)
    {
         $success=$this->db->insert(db_prefix().'department', $data); 
          $result =  $this->db->affected_rows();
          if($result == 1)
          {
            return true;
          }
          else
          {
            return FALSE ;
          }
    }

     public function fetch_dept($id){

      return $this->db->get_where(db_prefix().'department', array('dept_id' => $id))->row();

    }
    
     public function update_dept($data,$id){

      $this->db->where("dept_id", $id);  
      return $this->db->update(db_prefix().'department', $data); 

    }

    public function delete_dept($id){

         $this->db->where("dept_id", $id);  
        return $this->db->delete(db_prefix().'department'); 


    }

}
