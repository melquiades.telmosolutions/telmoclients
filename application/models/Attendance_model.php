<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Attendance_model extends App_Model 
{

     public function __construct()
    {
        parent::__construct();
    }

    public function show_all_data() {

      $query = $this->db->query("SELECT *,COUNT(DISTINCT(uid)) as no_members, WEEK(att_time,2) as week,MIN(att_time) as min_time,MAX(att_time) as max_time,MONTH(att_time) as month_log FROM tblattendance_history WHERE period!='' GROUP BY uid,MONTH(att_time) ORDER BY atten_his_id DESC");

        if ($query->num_rows() > 0) {
          return $query->result();
        } else {
          return false;
        }
      }

    public function show_data_pdf($data) {

      $query = $this->db->query("SELECT *,MIN(att_time) as min_time,MAX(att_time) as max_time,MONTH(att_time) as month_log FROM tblattendance_history WHERE MONTH(att_time)='".$data['month_log']."' AND uid='".$data['uid']."' AND tblattendance_history.number='".$data['att_number']."' ");


        if ($query->num_rows() > 0) {
        return $query->row();
        } else {
        return false;
        }
    }

    public function show_data_pdf_result($month_log,$uid,$att_number) {

      $query = $this->db->query("SELECT DISTINCT(uid) FROM tblattendance_history WHERE MONTH(att_time)='".$month_log."' AND uid='".$uid."' AND number='".$att_number."' ");

        if ($query->num_rows() > 0) {
        return $query->result();
        } else {
        return false;
        }
    }


      public function show_data_by_date_range($data) {


        $condition1 = "att_time BETWEEN " . "'" . $data['date1'] . "'" . " AND " . "'" . $data['date2'] . "'";
        $group_mon  = "MONTH(att_time)";
        
          $this->db->select('*');
          $this->db->select("COUNT(DISTINCT(uid)) as no_members",  FALSE );
          $this->db->select("WEEK(att_time,2) as week",  FALSE );
          $this->db->select("MIN(att_time) as min_time",  FALSE );
          $this->db->select("MAX(att_time) as max_time",  FALSE );
          $this->db->select("MONTH(att_time) as month_log",  FALSE );
          $this->db->from('tblattendance_history');
          $this->db->where($condition1);
          $this->db->group_by('uid');
          $this->db->group_by($group_mon);
          $this->db->order_by('atten_his_id','DESC');
          $query = $this->db->get();

          if ($query->num_rows() > 0) {
          return $query->result();
          } else {
          return false;
          }
        
        
      }

     
    
}
    
    