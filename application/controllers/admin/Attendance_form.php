<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Attendance_form extends AdminController
{
     public function __construct()
    {
        parent::__construct();
        $this->load->model('Employee_model');  
        $this->load->model('Attendance_model');  
        $this->load->model('Projects_model');  

    }
    
  public function atten_form() {
        $data['title']            = _l('attendance_list');
        $data['employee']       = $this->Employee_model->get_employee();
        $data['projects']       = $this->Projects_model->fetch_projects();
        $this->load->view('admin/attendance/attendance_form',$data);
    }

public function checkweeklylog(){
        $loc_id = $this->input->post('loid',TRUE);
        $emp_id = $this->input->post('u_id',TRUE);
        $att_time = $this->input->post('att_time',TRUE);
        $data = $this->Projects_model->checkweeklylog($loc_id,$emp_id,$att_time)->result();
        echo json_encode($data);
    }

 public function saveweeklylog(){
        $proj_id  = $this->input->post('proj_id');
        $loc_id   = $this->input->post('loc_id');
        $emp_id   = $this->input->post('emp_id');
        $att_time = $this->input->post('att_time');
        $number   = $this->input->post('num');
        $prefix   = $this->input->post('pre');

        $check = $this->Projects_model->savecheckweeklylog($proj_id,$emp_id,$att_time)->result();

        $checkcount = count($check);
        if ($checkcount > 0) {
            set_alert('danger', 'Date already exist. Try again!');
            redirect(admin_url('attendance_form/atten_form'));
        }
        else{
            $this->Projects_model->deletexisting($loc_id,$emp_id,$att_time);
            $this->Projects_model->deletexisting_proj($proj_id,$emp_id,$att_time);
            $data['savedlog']      = $this->Projects_model->saveweeklylog($loc_id,$emp_id,$att_time,$number,$prefix)->result();
            $data['savedlog_proj'] = $this->Projects_model->saveweeklylog_proj($proj_id,$emp_id,$att_time)->result();
            $data['datalength']    = count($data['savedlog']);
            $data['title']         = _l('attendance_list');
            $data['employee']      = $this->Employee_model->get_employee();
            $data['projects']      = $this->Projects_model->fetch_projects();
            $this->load->view('admin/attendance/attendance_form',$data); 
        }

    }

public function updateweeklylog(){

     if ($this->input->post('butsave')) {
         $atten_id = $this->input->post('atten_his_id');
         $overtime = $this->input->post('overtime');
         $period = $this->input->post('period');
         $absent = $this->input->post('absent');
         $holiday = $this->input->post('holiday');
    
         $att_date = $this->input->post('att_date');
         $proj_id = $this->input->post('proid');
         $loc_id  = $this->input->post('loid');
         $emp_id  = $this->input->post('u_id');
         $num_ber  = $this->input->post('number_num');
         $pre_fix  = $this->input->post('prefix_pre');

         $att_time2 = $this->input->post('att_time2');
         $period2 = $this->input->post('period2');
         $absent2 = $this->input->post('absent2');
         $overtime2 = $this->input->post('overtime2');
         $holiday2 = $this->input->post('holiday2');
         $ival2 = $this->input->post('ival2');
         $removeid2 = $this->input->post('removeid2');

         $att_time3 = $this->input->post('att_time3');
         $period3 = $this->input->post('period3');
         $absent3 = $this->input->post('absent3');
         $overtime3 = $this->input->post('overtime3');
         $holiday3 = $this->input->post('holiday3');
         $ival3 = $this->input->post('ival3');
         $removeid3 = $this->input->post('removeid3');

         $att_time4 = $this->input->post('att_time4');
         $period4 = $this->input->post('period4');
         $absent4 = $this->input->post('absent4');
         $overtime4 = $this->input->post('overtime4');
         $holiday4 = $this->input->post('holiday4');
         $ival4 = $this->input->post('ival4');
         $removeid4 = $this->input->post('removeid4');

         $att_time5 = $this->input->post('att_time5');
         $period5 = $this->input->post('period5');
         $absent5 = $this->input->post('absent5');
         $overtime5 = $this->input->post('overtime5');
         $holiday5 = $this->input->post('holiday5');
         $ival5 = $this->input->post('ival5');
         $removeid5 = $this->input->post('removeid5');

         $att_time6 = $this->input->post('att_time6');
         $period6 = $this->input->post('period6');
         $absent6 = $this->input->post('absent6');
         $overtime6 = $this->input->post('overtime6');
         $holiday6 = $this->input->post('holiday6');
         $ival6 = $this->input->post('ival6');
         $removeid6 = $this->input->post('removeid6');

         $att_time7 = $this->input->post('att_time7');
         $period7 = $this->input->post('period7');
         $absent7 = $this->input->post('absent7');
         $overtime7 = $this->input->post('overtime7');
         $holiday7 = $this->input->post('holiday7');
         $ival7 = $this->input->post('ival7');
         $removeid7 = $this->input->post('removeid7');

        $att_time8 = $this->input->post('att_time8');
        $period8   = $this->input->post('period8');
        $absent8   = $this->input->post('absent8');
        $overtime8 = $this->input->post('overtime8');
        $holiday8  = $this->input->post('holiday8');
        $ival8     = $this->input->post('ival8');
        $removeid8 = $this->input->post('removeid8');

        $att_time9 = $this->input->post('att_time9');
        $period9   = $this->input->post('period9');
        $absent9   = $this->input->post('absent9');
        $overtime9 = $this->input->post('overtime9');
        $holiday9  = $this->input->post('holiday9');
        $ival9     = $this->input->post('ival9');
        $removeid9 = $this->input->post('removeid9');

        $att_time10 = $this->input->post('att_time10');
        $period10   = $this->input->post('period10');
        $absent10   = $this->input->post('absent10');
        $overtime10 = $this->input->post('overtime10');
        $holiday10  = $this->input->post('holiday10');
        $ival10     = $this->input->post('ival10');
        $removeid10 = $this->input->post('removeid10');

        $att_time11 = $this->input->post('att_time11');
        $period11   = $this->input->post('period11');
        $absent11   = $this->input->post('absent11');
        $overtime11 = $this->input->post('overtime11');
        $holiday11  = $this->input->post('holiday11');
        $ival11     = $this->input->post('ival11');
        $removeid11 = $this->input->post('removeid11');

        $att_time12 = $this->input->post('att_time12');
        $period12   = $this->input->post('period12');
        $absent12   = $this->input->post('absent12');
        $overtime12 = $this->input->post('overtime12');
        $holiday12  = $this->input->post('holiday12');
        $ival12     = $this->input->post('ival12');
        $removeid12 = $this->input->post('removeid12');

        $att_time13 = $this->input->post('att_time13');
        $period13   = $this->input->post('period13');
        $absent13   = $this->input->post('absent13');
        $overtime13 = $this->input->post('overtime13');
        $holiday13  = $this->input->post('holiday13');
        $ival13     = $this->input->post('ival13');
        $removeid13 = $this->input->post('removeid13');

        $att_time14 = $this->input->post('att_time14');
        $period14   = $this->input->post('period14');
        $absent14   = $this->input->post('absent14');
        $overtime14 = $this->input->post('overtime14');
        $holiday14  = $this->input->post('holiday14');
        $ival14     = $this->input->post('ival14');
        $removeid14 = $this->input->post('removeid14');

        $att_time15 = $this->input->post('att_time15');
        $period15   = $this->input->post('period15');
        $absent15   = $this->input->post('absent15');
        $overtime15 = $this->input->post('overtime15');
        $holiday15  = $this->input->post('holiday15');
        $ival15     = $this->input->post('ival15');
        $removeid15 = $this->input->post('removeid15');

        $this->Projects_model->updateweeklylog($atten_id,$overtime,$period,$absent,$holiday);
        $this->Projects_model->saveweeklylogall_proj($proj_id,$emp_id,$att_date,$att_date,$ival2,$removeid2);

         if ($att_time2 != '') {
             $this->Projects_model->saveweeklylogall($loc_id,$emp_id,$num_ber,$pre_fix,$att_time2,$period2,$absent2,$overtime2,$holiday2,$ival2,$removeid2);
             $this->Projects_model->saveweeklylogall_proj($proj_id,$emp_id,$att_date,$att_time2,$ival2,$removeid2); }
         if ($att_time3 != '') {
             $this->Projects_model->saveweeklylogall($loc_id,$emp_id,$num_ber,$pre_fix,$att_time3,$period3,$absent3,$overtime3,$holiday3,$ival3,$removeid3);
             $this->Projects_model->saveweeklylogall_proj($proj_id,$emp_id,$att_date,$att_time3,$ival3,$removeid3); }
         if ($att_time4 != '') {
             $this->Projects_model->saveweeklylogall($loc_id,$emp_id,$num_ber,$pre_fix,$att_time4,$period4,$absent4,$overtime4,$holiday4,$ival4,$removeid4);
             $this->Projects_model->saveweeklylogall_proj($proj_id,$emp_id,$att_date,$att_time4,$ival4,$removeid4); }
         if ($att_time5 != '') {
             $this->Projects_model->saveweeklylogall($loc_id,$emp_id,$num_ber,$pre_fix,$att_time5,$period5,$absent5,$overtime5,$holiday5,$ival5,$removeid5);
             $this->Projects_model->saveweeklylogall_proj($proj_id,$emp_id,$att_date,$att_time5,$ival5,$removeid5); }
         if ($att_time6 != '') {
             $this->Projects_model->saveweeklylogall($loc_id,$emp_id,$num_ber,$pre_fix,$att_time6,$period6,$absent6,$overtime6,$holiday6,$ival6,$removeid6);
             $this->Projects_model->saveweeklylogall_proj($proj_id,$emp_id,$att_date,$att_time6,$ival6,$removeid6); }
         if ($att_time7 != '') {
             $this->Projects_model->saveweeklylogall($loc_id,$emp_id,$num_ber,$pre_fix,$att_time7,$period7,$absent7,$overtime7,$holiday7,$ival7,$removeid7);
             $this->Projects_model->saveweeklylogall_proj($proj_id,$emp_id,$att_date,$att_time7,$ival7,$removeid7); }
        if ($att_time8 != '') {
            $this->Projects_model->saveweeklylogall($loc_id,$emp_id,$num_ber,$pre_fix,$att_time8,$period8,$absent8,$overtime8,$holiday8,$ival8,$removeid8);
             $this->Projects_model->saveweeklylogall_proj($proj_id,$emp_id,$att_date,$att_time8,$ival8,$removeid8); }
        if ($att_time9 != '') {
            $this->Projects_model->saveweeklylogall($loc_id,$emp_id,$num_ber,$pre_fix,$att_time9,$period9,$absent9,$overtime9,$holiday9,$ival9,$removeid9);
             $this->Projects_model->saveweeklylogall_proj($proj_id,$emp_id,$att_date,$att_time9,$ival9,$removeid9); }
        if ($att_time10 != '') {
            $this->Projects_model->saveweeklylogall($loc_id,$emp_id,$num_ber,$pre_fix,$att_time10,$period10,$absent10,$overtime10,$holiday10,$ival10,$removeid10);
             $this->Projects_model->saveweeklylogall_proj($proj_id,$emp_id,$att_date,$att_time10,$ival10,$removeid10); }
        if ($att_time11 != '') {
            $this->Projects_model->saveweeklylogall($loc_id,$emp_id,$num_ber,$pre_fix,$att_time11,$period11,$absent11,$overtime11,$holiday11,$ival11,$removeid11);
             $this->Projects_model->saveweeklylogall_proj($proj_id,$emp_id,$att_date,$att_time11,$ival11,$removeid11); }
        if ($att_time12 != '') {
            $this->Projects_model->saveweeklylogall($loc_id,$emp_id,$num_ber,$pre_fix,$att_time12,$period12,$absent12,$overtime12,$holiday12,$ival12,$removeid12);
             $this->Projects_model->saveweeklylogall_proj($proj_id,$emp_id,$att_date,$att_time12,$ival12,$removeid12); }
        if ($att_time13 != '') {
            $this->Projects_model->saveweeklylogall($loc_id,$emp_id,$num_ber,$pre_fix,$att_time13,$period13,$absent13,$overtime13,$holiday13,$ival13,$removeid13);
             $this->Projects_model->saveweeklylogall_proj($proj_id,$emp_id,$att_date,$att_time13,$ival13,$removeid13); }
        if ($att_time14 != '') {
            $this->Projects_model->saveweeklylogall($loc_id,$emp_id,$num_ber,$pre_fix,$att_time14,$period14,$absent14,$overtime14,$holiday14,$ival14,$removeid14);
             $this->Projects_model->saveweeklylogall_proj($proj_id,$emp_id,$att_date,$att_time14,$ival14,$removeid14); }
        if ($att_time15 != '') {
            $this->Projects_model->saveweeklylogall($loc_id,$emp_id,$num_ber,$pre_fix,$att_time15,$period15,$absent15,$overtime15,$holiday15,$ival15,$removeid15);
             $this->Projects_model->saveweeklylogall_proj($proj_id,$emp_id,$att_date,$att_time15,$ival15,$removeid15); }


        set_alert('success', 'Attendance added successfully!');
        redirect(admin_url('attendance_form/atten_form'));
        
    }

}
public function cancelweeklylog(){

    $proj_id    = $this->input->post('proid');
    $loc_id     = $this->input->post('loid');
    $emp_id     = $this->input->post('uid');
    $att_time   = $this->input->post('atttime');

    foreach ($emp_id as $key => $employeeid) {
        $this->db->where('location_id', $loc_id[$key]);
        $this->db->where('uid', $employeeid);
        $this->db->where('att_time', $att_time[$key]);
        $this->db->delete(db_prefix() . 'attendance_history');
    }

    foreach($proj_id as $key => $projectid) {
        $this->db->where('project_id', $projectid);
        $this->db->where('att_time_min', $att_time[0]);
        $this->db->where('att_time_max', '0000-00-00');
        $this->db->delete(db_prefix() . 'attendance_project');
    }
    set_alert('danger', 'Attendance canceled!');
    redirect(admin_url('attendance_form/atten_form'));
}


}

