<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Employee extends AdminController
{
    
    public function __construct()
    {
        parent::__construct();
        
        $this->load->model('employee_model');
        $this->load->model('department_model');
        $this->load->model('position_model');
        $this->load->model('currencies_model');
        $this->load->model('staff_model');

    }

    public function create_position()
    {
        $data['title'] = _l('lead_title');

        $new_position = [
            'position_name'     => $this->input->post('position_name', true),
            'position_details'  => $this->input->post('position_details', true),
        ];

        if($this->employee_model->add_position($new_position)) {
            set_alert('success', _l('added_successfully', _l('position')));
            redirect(admin_url('employee/create_position'));
        }

        $this->load->view('admin/employee/position_form', $data);
    }

    public function manageemployee()
    {
        $data['title'] = _l('employee');
        
        if ($this->input->is_ajax_request()) {
            $this->app->get_table_data('employee');
        }
        $this->load->view('admin/employee/employee_view', $data);
    }

    public function table()
    {
        $this->app->get_table_data('position');
    }

    public function viewEmhistory()
    {
        $data['title'] = _l('employee');

        $data['department']      = $this->department_model->get_department();
        $data['position']        = $this->position_model->get_all();
        $data['country']         = $this->employee_model->get_country();
        $data['duty_type']       = $this->employee_model->get_duty_type();
        $data['employee']        = $this->employee_model->get_employee();
        $data['marital_status']  = $this->employee_model->get_marital_status();
        $data['benefit']         = $this->employee_model->get_emp_benefit();
        $data['staff_admin']     = $this->staff_model->get_admin();

        $this->load->view('admin/employee/employee_form', $data);
    }

    public function create_employee() 
    {
        $data['title'] = _l('employee');

        if($this->input->post('e_picture') != '') {
            $this->load->library('emp_profile');
            $img = $this->emp_profile->do_upload(
                './uploads/employees/', 'picture'
            );
        } else {
            $img = '';
        }
        
        $dobdate  = $this->input->post('dob', true);
        $dobdate == 'mm-dd-yyyy' || $dobdate == '' ? $dobdate = '00-00-0000' : $dobdate;
        $dob_date = DateTime::createFromFormat('m-d-Y', $dobdate)->format('Y-m-d');
        
        $hddate   = $this->input->post('hire_date',true);
        $hddate == 'mm-dd-yyyy' || $hddate == '' ? $hddate = '00-00-0000' : $hddate;
        $hd_date  = DateTime::createFromFormat('m-d-Y', $hddate)->format('Y-m-d');
        
        $tddate   = $this->input->post('termination_date',true);
        $tddate == 'mm-dd-yyyy' || $tddate == '' ? $tddate = '00-00-0000' : $tddate;
        $td_date  = DateTime::createFromFormat('m-d-Y', $tddate)->format('Y-m-d');
        
        $rddate   = $this->input->post('rehire_date',true);
        $rddate == 'mm-dd-yyyy' || $rddate == '' ? $rddate = '00-00-0000' : $rddate;
        $rd_date  = DateTime::createFromFormat('m-d-Y', $rddate)->format('Y-m-d');

        $empData = [
            'emp_id_number'           => $this->input->post('id_number', true),
            'picture'                 => $img,
            'first_name'              => $this->input->post('first_name', true),
            'middle_name'             => $this->input->post('middle_name', true),
            'last_name'               => $this->input->post('last_name', true),
            'email'                   => $this->input->post('email', true),
            'phone'                   => $this->input->post('phone', true),
            'alter_phone'             => $this->input->post('alter_phone',true),
            'dob'                     => $dob_date,
            'gender'                  => $this->input->post('gender', true),
            'marital_status'          => $this->input->post('marital_status', true),
            'blood_type'              => $this->input->post('blood_type', true),
            'no_children'             => $this->input->post('no_children', true),
            'spouse_fname'            => $this->input->post('spouse_fname', true),
            'spouse_lname'            => $this->input->post('spouse_lname', true),

            'city'                    => $this->input->post('city', true),
            'zip'                     => $this->input->post('zip', true),
            'house_no'                => $this->input->post('house_no', true),
            'street_name'             => $this->input->post('street_name', true),
            'subdivision'             => $this->input->post('subdivision', true),
            'barangay'                => $this->input->post('barangay', true),
            'province'                => $this->input->post('province', true),
            
            'emrg_contact_person'     => $this->input->post('emrg_contact_person',true),
            'emerg_contct'            => $this->input->post('emerg_contct',true),
            'emgr_contct_relation'    => $this->input->post('emgr_contct_relation',true),
            'alt_em_contct_person'    => $this->input->post('alt_em_contct_person',true),
            'alt_em_contct'           => $this->input->post('alt_em_contct',true),
            'alt_emg_contact_rel'     => $this->input->post('alt_emg_contact_rel',true),

            'dept_id'                 => $this->input->post('dept_id',true),
            'position_id'             => $this->input->post('position_id',true),
            'duty_type'               => $this->input->post('duty_type',true),
            'hire_date'               => $hd_date,
            'termination_date'        => $td_date,
            'voluntary_termination'   => $this->input->post('voluntary_termination',true),
            'termination_reason'      => $this->input->post('termination_reason',true),
            'rehire_date'             => $rd_date,
            'rate_type'               => $this->input->post('rate_type',true),
            'rate'                    => $this->input->post('rate',true),
            'skill_index'             => $this->input->post('skill_index',true),
            'currency'                => 3,

            'tin_number'              => $this->input->post('tax_tin', true),
            'tax_percentage'          => $this->input->post('tax_percentage', true),
            'tax_value'               => $this->input->post('tax_value', true),

            'supervisor_status'       => $this->input->post('supervisor_status',true),
            'super_visor_id'          => $this->input->post('super_visor_id',true),
            'supervisor_report'       => $this->input->post('supervisor_report',true),
        ];

        if($this->employee_model->add_empp($empData)) {

            $insert_id = $this->db->insert_id();
            if (!empty($insert_id)) {
                log_activity('New Employee Added [ID: ' . $insert_id . ']');
            }

            $benefit_sss_number = $this->input->post('benefit_sss_number',true);
            $benefit_sss_date   = $this->input->post('benefit_sss_date',true);
            $benefit_sss_status = $this->input->post('benefit_sss_status',true);

            $benefit_pi_number  = $this->input->post('benefit_pi_number',true);
            $benefit_pi_date    = $this->input->post('benefit_pi_date',true);
            $benefit_pi_status  = $this->input->post('benefit_pi_status',true);

            $benefit_ph_number  = $this->input->post('benefit_ph_number',true);
            $benefit_ph_date    = $this->input->post('benefit_ph_date',true);
            $benefit_ph_status  = $this->input->post('benefit_ph_status',true);

            $benefit = [
                'employee_id'           => $insert_id,

                'benefit_sss_number'    => $benefit_sss_number,
                'benefit_sss_date'      => $benefit_sss_date,
                'benefit_sss_status'    => $benefit_sss_status,

                'benefit_pi_number'     => $benefit_pi_number,
                'benefit_pi_date'       => $benefit_pi_date,
                'benefit_pi_status'     => $benefit_pi_status,

                'benefit_ph_number'     => $benefit_ph_number,
                'benefit_ph_date'       => $benefit_ph_date,
                'benefit_ph_status'     => $benefit_ph_status,
            ];

            if(!empty($benefit)) {
                $this->db->insert(db_prefix() . 'employee_benefit', $benefit);
            }

            set_alert('success', _l('added_successfully', _l('employee')));
            redirect(admin_url('employee/manageemployee'));
        } else {
            set_alert('danger', _l('emp_record_failed'));
            redirect(admin_url('employee/viewEmhistory'));
        }
    }

    public function edit_emp($id = '') 
    {
        $data['title'] = _l('employee');

        $data['employees']       = $this->employee_model->get_all_empdata($id);
        $data['emp_benefit']     = $this->employee_model->get_empbenefit($id);
        $data['benefit']         = $this->employee_model->get_emp_benefit();
        $data['country']         = $this->employee_model->get_country();
        $data['department']      = $this->department_model->get_department();
        $data['position']        = $this->position_model->get_all();
        $data['duty_type']       = $this->employee_model->get_duty_type();
        $data['employee']        = $this->employee_model->get_employee();
        $data['marital_status']  = $this->employee_model->get_marital_status();
        $data['staff_admin']     = $this->staff_model->get_admin();

        $this->load->view('admin/employee/employee_form', $data);
    }

    public function delete_emp($id = '')
    {
        $del = $this->employee_model->delete_emp($id);
        if($del) {
            $this->db->where("employee_id", $id)->delete(db_prefix().'employee_benefit'); 
        }
        
        set_alert('success', _l('deleted', _l('employee')));
        redirect(admin_url('employee/manageemployee'));
    }

    public function update_emp($id = '')
    {
        $data['title'] = _l('employee');

        $pic  = $this->input->post('e_picture', true);
        $pic2 = $this->input->post('e_picture_db', true);
        if(empty($pic)) {
            $img = $pic2;
        } elseif (!empty($pic)) {
            $this->load->library('emp_profile');
            $img = $this->emp_profile->do_upload(
                './uploads/employees/', 'picture'
            );
            // unlink($pic2);
        } else {
            $img = $pic2;
        }
        
        $dobdate  = $this->input->post('dob', true);
        $dobdate == 'mm-dd-yyyy' || $dobdate == '' ? $dobdate = '00-00-0000' : $dobdate = date('m-d-Y', strtotime($dobdate));
        $dob_date = date('Y-m-d', strtotime($dobdate));
        
        $hddate   = $this->input->post('hire_date',true);
        $hddate == 'mm-dd-yyyy' || $hddate == '' ? $hddate = '00-00-0000' : $hddate = date('m-d-Y', strtotime($hddate));
        $hd_date = date('Y-m-d', strtotime($hddate));
        
        $tddate   = $this->input->post('termination_date',true);
        $tddate == 'mm-dd-yyyy' || $tddate == '' ? $tddate = '00-00-0000' : $tddate = date('m-d-Y', strtotime($tddate));
        $td_date  = date('Y-m-d', strtotime($tddate));
        
        $rddate   = $this->input->post('rehire_date',true);
        $rddate == 'mm-dd-yyyy' || $rddate == '' ? $rddate = '00-00-0000' : $rddate = date('m-d-Y', strtotime($rddate));
        $rd_date  = date('Y-m-d', strtotime($rddate));

        $empData = [
            'emp_id_number'           => $this->input->post('id_number', true),
            'picture'                 => $img,
            'first_name'              => $this->input->post('first_name', true),
            'middle_name'             => $this->input->post('middle_name', true),
            'last_name'               => $this->input->post('last_name', true),
            'email'                   => $this->input->post('email', true),
            'phone'                   => $this->input->post('phone', true),
            'alter_phone'             => $this->input->post('alter_phone',true),
            'dob'                     => $dob_date,
            'gender'                  => $this->input->post('gender', true),
            'marital_status'          => $this->input->post('marital_status', true),
            'blood_type'              => $this->input->post('blood_type', true),
            'no_children'             => $this->input->post('no_children', true),
            'spouse_fname'            => $this->input->post('spouse_fname', true),
            'spouse_lname'            => $this->input->post('spouse_lname', true),

            'city'                    => $this->input->post('city', true),
            'zip'                     => $this->input->post('zip', true),
            'house_no'                => $this->input->post('house_no', true),
            'street_name'             => $this->input->post('street_name', true),
            'subdivision'             => $this->input->post('subdivision', true),
            'barangay'                => $this->input->post('barangay', true),
            'province'                => $this->input->post('province', true),
            
            'emrg_contact_person'     => $this->input->post('emrg_contact_person',true),
            'emerg_contct'            => $this->input->post('emerg_contct',true),
            'emgr_contct_relation'    => $this->input->post('emgr_contct_relation',true),
            'alt_em_contct_person'    => $this->input->post('alt_em_contct_person',true),
            'alt_em_contct'           => $this->input->post('alt_em_contct',true),
            'alt_emg_contact_rel'     => $this->input->post('alt_emg_contact_rel',true),

            'dept_id'                 => $this->input->post('dept_id',true),
            'position_id'             => $this->input->post('position_id',true),
            'duty_type'               => $this->input->post('duty_type',true),
            'hire_date'               => $hd_date,
            'termination_date'        => $td_date,
            'voluntary_termination'   => $this->input->post('voluntary_termination',true),
            'termination_reason'      => $this->input->post('termination_reason',true),
            'rehire_date'             => $rd_date,
            'rate_type'               => $this->input->post('rate_type',true),
            'rate'                    => $this->input->post('rate',true),
            'skill_index'             => $this->input->post('skill_index',true),

            'tin_number'              => $this->input->post('tax_tin', true),
            'tax_percentage'          => $this->input->post('tax_percentage', true),
            'tax_value'               => $this->input->post('tax_value', true),
            
            'supervisor_status'       => $this->input->post('supervisor_status',true),
            'super_visor_id'          => $this->input->post('super_visor_id',true),
            'supervisor_report'       => $this->input->post('supervisor_report',true),
        ];

        if($this->employee_model->update_empp($empData, $id)) {

            $employee_id        = $id;
            if (!empty($employee_id)) {
                log_activity('Employee Information Updated [ID: ' . $employee_id . ']');
            }

            $benefit_sss_number = $this->input->post('benefit_sss_number',true);
            $benefit_sss_date   = $this->input->post('benefit_sss_date',true);
            $benefit_sss_status = $this->input->post('benefit_sss_status',true);

            $benefit_pi_number  = $this->input->post('benefit_pi_number',true);
            $benefit_pi_date    = $this->input->post('benefit_pi_date',true);
            $benefit_pi_status  = $this->input->post('benefit_pi_status',true);

            $benefit_ph_number  = $this->input->post('benefit_ph_number',true);
            $benefit_ph_date    = $this->input->post('benefit_ph_date',true);
            $benefit_ph_status  = $this->input->post('benefit_ph_status',true);

            $benefit_sss_date == 'mm-dd-yyyy' || $benefit_sss_date == '' ? $benefit_sss_date = '00-00-0000' : $benefit_sss_date = date('m-d-Y', strtotime($benefit_sss_date));
            $sss_date  = date('Y-m-d', strtotime($benefit_sss_date));

            $benefit_pi_date == 'mm-dd-yyyy' || $benefit_pi_date == '' ? $benefit_pi_date = '00-00-0000' : $benefit_pi_date = date('m-d-Y', strtotime($benefit_pi_date));
            $pi_date  = date('Y-m-d', strtotime($benefit_pi_date));

            $benefit_ph_date == 'mm-dd-yyyy' || $benefit_ph_date == '' ? $benefit_ph_date = '00-00-0000' : $benefit_ph_date = date('m-d-Y', strtotime($benefit_ph_date));
            $ph_date  = date('Y-m-d', strtotime($benefit_ph_date));

            $benefit = [
                'employee_id'           => $employee_id,

                'benefit_sss_number'    => $benefit_sss_number,
                'benefit_sss_date'      => $sss_date,
                'benefit_sss_status'    => $benefit_sss_status,

                'benefit_pi_number'     => $benefit_pi_number,
                'benefit_pi_date'       => $pi_date,
                'benefit_pi_status'     => $benefit_pi_status,

                'benefit_ph_number'     => $benefit_ph_number,
                'benefit_ph_date'       => $ph_date,
                'benefit_ph_status'     => $benefit_ph_status,
            ];

            if(!empty($benefit)) {
                $this->db->where('employee_id', $employee_id)->update(db_prefix() . 'employee_benefit', $benefit);
            }

            set_alert('success', _l('updated_successfully', _l('employee')));
            redirect(admin_url('employee/manageemployee'));
        } else {
            set_alert('danger', _l('u_emp_record_failed'));
            redirect(admin_url('employee/viewEmhistory/'. $id));
        }
    }

    public function edit_position($id = '')
    {
        $data['title'] = _l('lead_title');

        $data['position'] = $this->employee_model->get_pos($id);

        $this->load->view('admin/employee/position_form', $data);
    }

    public function update_pos($id = '')
    {
        $data['title'] = _l('lead_title');

        $position = [
            'position_name'     => $this->input->post('position_name',true),
            'position_details'  => $this->input->post('position_details',true),
        ];

        if($this->employee_model->update_pos($position, $id)) {
            set_alert('success', _l('updated_successfully', _l('position')));
            redirect(admin_url('employee/create_position'));
        }

    }

    public function delete_position($id = '')
    {
        $del = $this->employee_model->delete_pos($id);
        if($del) {
            set_alert('success', _l('deleted', _l('position')));
            redirect(admin_url('employee/create_position'));
        }
    }

    public function view_emp($id = '')
    {
        $data['title'] = _l('employee');

        $data['employees']       = $this->employee_model->get_all_empdata($id);
        $data['emp_benefit']     = $this->employee_model->get_empbenefit($id);
        $data['benefit']         = $this->employee_model->get_emp_benefit();
        $data['country']         = $this->employee_model->get_country();
        $data['department']      = $this->department_model->get_department();
        $data['position']        = $this->position_model->get_all();
        $data['duty_type']       = $this->employee_model->get_duty_type();
        $data['employee']        = $this->employee_model->get_employee();
        $data['marital_status']  = $this->employee_model->get_marital_status();
        $data['currency']        = $this->currencies_model->get_base_currency();

        $this->load->view('admin/employee/employee', $data);
    }

    public function export_emp($id)
    {
        $data['title'] = _l('employee');

        require_once APPPATH . '/third_party/mpdf/vendor/autoload.php';

        $mpdf = new \Mpdf\Mpdf();

        $data['employees']       = $this->employee_model->get_all_empdata($id);
        $data['emp_benefit']     = $this->employee_model->get_empbenefit($id);
        $data['benefit']         = $this->employee_model->get_emp_benefit();
        $data['country']         = $this->employee_model->get_country();
        $data['department']      = $this->department_model->get_department();
        $data['position']        = $this->position_model->get_all();
        $data['duty_type']       = $this->employee_model->get_duty_type();
        $data['employee']        = $this->employee_model->get_employee();
        $data['marital_status']  = $this->employee_model->get_marital_status();
        $data['currency']        = $this->currencies_model->get_base_currency();

        $html = $this->load->view('admin/employee/export_emp', $data, true);
        $title = $data['employees']->first_name . ' ' . $data['employees']->last_name;
        $mpdf->AddPage('P','','','','',12,10,5,5);
        $mpdf->WriteHTML($html);
        $output = $title . '.pdf';
        $mpdf->Output($output, 'I');
    }
}