<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Attendance_log extends AdminController
{

    public function __construct() {
        parent::__construct();
        $this->load->model('Attendance_model');
        $this->load->model('Employee_model');
        $this->load->helper('url');
    }

    public function atten_log()
    { 
        if ($this->input->post('filter')) {

            $date1 = $this->input->post('date_from');
            $date2 = $this->input->post('date_to');
            $data = array(
            'date1' => $date1,
            'date2' => $date2
            );
            if ($date1 == "" || $date2 == "") {
                $data['date_range_error_message'] = "Both date fields are required";
            } else {
            $result = $this->Attendance_model->show_data_by_date_range($data);
            if ($result != false) {
            $data['result_display'] = $result;
            } else {
            $data['result_display'] = "No record found !";
            }
            }
            $data['display_date'] = array(
            'date1' => $date1,
            'date2' => $date2
            );
            $data['title']            = 'Search Result/s - Weekly Logsheet PDF';
            $this->load->view('admin/attendance/attendancelog', $data);

        }
        else{

            $data['title']            = 'Weekly Logsheet PDF';
            $data['show_table'] = $this->view_table();
            $this->load->view('admin/attendance/attendancelog', $data);
        }
        
       
    }

    public function view_table(){
        $result = $this->Attendance_model->show_all_data();
        if ($result != false) {
        return $result;
        } else {
        return 'Database is empty !';
        }
    }

    public function fetchdetailspdf(){

        require_once APPPATH . '/third_party/mpdf/vendor/autoload.php';

        $month_log  = $this->uri->segment(4);
        $uid        = $this->uri->segment(5); 
        $att_number = $this->uri->segment(6);

        $data= array(
            'month_log'  => $month_log,
            'uid'        => $uid,
            'att_number' => $att_number
        );

        $data1['show_pdf'] = $this->Attendance_model->show_data_pdf($data);
        $data1['show_pdf_result'] = $this->Attendance_model->show_data_pdf_result($data1['show_pdf']->month_log,$data1['show_pdf']->uid,$data1['show_pdf']->number);

        $title = 'MONTHLY LOGSHEET ' . date("M d, Y", strtotime($data1['show_pdf']->min_time)) . ' - ' . date("M d, Y", strtotime($data1['show_pdf']->max_time));
       
        $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'orientation' => 'L']);
        $html = $this->load->view('admin/attendance/pdfweeklylog', $data1, true);
        // $mpdf->AddPage('L');
        $mpdf->WriteHTML($html);
        $output = $title . '.pdf';
        $mpdf->Output($output, 'I');
    }

    public function deletepdf(){
        
        $month_log  = $this->uri->segment(4);
        $uid        = $this->uri->segment(5); 
        $att_number = $this->uri->segment(6);

        $get_att = $this->db->query("SELECT MIN(att_time) as min_time,MAX(att_time) as max_time FROM tblattendance_history WHERE MONTH(att_time)='".$month_log."' AND uid='".$uid."' AND tblattendance_history.number='".$att_number."' ")->row();

        $this->db->where('att_time_min', $get_att->min_time)->where('att_time_max', $get_att->max_time)->where('uid', $uid)->delete(db_prefix() . 'attendance_project');

        $month_atttime = "MONTH(att_time)";
        $q = $this->db->where($month_atttime, $month_log)->where('uid', $uid)->where('number', $att_number)->delete(db_prefix() . 'attendance_history');
        if(count($q) > 0) {
            set_alert('danger', 'Weekly Logsheet Deleted!');
            redirect(admin_url('attendance_log/atten_log'));
        }
          
    }

    public function compensationpdf(){

        $this->load->library('weekpdf');
        $pdf = $this->weekpdf->load();
        
        ini_set('memory_limit', '256M'); 
        
        $html = $this->load->view('admin/attendance/pdfcompensation', [], true);
        $pdf = new mPDF('c', 'Legal', '', '', 15, 15, 16, 16, 9, 9, 'L');
        $pdf->AddPage('L','','','','',15, 15, 15, 15, 8, 8);
        $pdf->WriteHTML($html); 
        $output = 'Compensation.pdf';
        $pdf->Output("$output", 'I'); 
        exit();
       


        
    }



}
