<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Payroll extends AdminController
{
    
    public function __construct()
    {
        parent::__construct();

        $this->load->model('employee_model');
        $this->load->model('payroll_model');
        $this->load->model('currencies_model');
        $this->load->model('payment_modes_model');
    }

    public function create_salary_setup()
    {
        $data['title'] = _l('salary_type_setup');
        
        if ($this->input->is_ajax_request()) {
            $this->app->get_table_data('benefit');
        }

        $this->load->view('admin/payroll/emp_salarysetup_form', $data);
    }

    public function edit_salary_setup($id)
    {
        $data['title'] = _l('salary_type_setup');
        
        if ($this->input->is_ajax_request()) {
            $this->app->get_table_data('benefit');
        }

        $data['benefit']    = $this->payroll_model->benefit($id);
        $data['currency']   = $this->currencies_model->get_base_currency();

        $this->load->view('admin/payroll/emp_salarysetup_form', $data);
    }

    public function delete_salary_setup($id)
    {
        $del = $this->payroll_model->delete_ben($id);
        if($del) {
            set_alert('success', _l('deleted', _l('salary_type')));
            redirect(admin_url('payroll/create_salary_setup'));
        }
    }

    public function create_s_setup()
    { 
        $data['title'] = _l('salary_setup');

        if ($this->input->is_ajax_request()) {
            $this->app->get_table_data('salarysetup');
        }

        $emp_id_ss = $this->input->post('employee_name');
        if(!empty($emp_id_ss)) {
            $check = $this->payroll_model->check_salary_setup($emp_id_ss);
            if(count($check) > 0) {
                set_alert('danger', _l('failed_ss'));
                redirect(admin_url('payroll/create_s_setup'));
            } else {
                $bonus = $this->input->post('bonus');
                foreach($bonus as $key=>$value) {
                    $bonus_save = [
                        'employee_id'       => $emp_id_ss,
                        'sal_type'          => $this->input->post('setup_rate_type_id', true),
                        'salary_type_id'    => $key,
                        'type'              => 'bonus',
                        'amount'            => $value,
                        'total'             => $this->input->post('total', true),
                        'create_date'       => date('Y-m-d'),
                        'paymentmode'       => $this->input->post('payment_modes', true),
                        'currency'          => 3,
                    ];
                    $inss = $this->payroll_model->create_salary_setup($bonus_save);
                }

                $amount = $this->input->post('amount');
                foreach($amount as $key=>$value) {
                    $salary_setup = [
                        'employee_id'       => $emp_id_ss,
                        'sal_type'          => $this->input->post('setup_rate_type_id', true),
                        'salary_type_id'    => $key,
                        'amount'            => $value,
                        'total'             => $this->input->post('total', true),
                        'create_date'       => date('Y-m-d'),
                        'paymentmode'       => $this->input->post('payment_modes', true),
                        'currency'          => 3,
                    ];

                    $ins = $this->payroll_model->create_salary_setup($salary_setup);
                    if($ins) {
                        set_alert('success', _l('added_successfully', _l('salary_setup')));
                    }
                    else {
                        set_alert('danger', _l('failed_s'));
                    }
                }
                set_alert('success', _l('added_successfully', _l('salary_setup')));
            }
        }

        $data['pay_modes']     = $this->payment_modes_model->get();
        $data['ss_employee']   = $this->employee_model->get_employee();
        $data['salary_type_o'] = $this->payroll_model->get_salary_type_o();
        $data['salary_type_z'] = $this->payroll_model->get_salary_type_z();
        $data['currency']      = $this->currencies_model->get_base_currency();
        
        $this->load->view('admin/payroll/salarysetup_form', $data);
    }
    public function create_s_setup_()
    {
        // $this->create_s_setup();
        $data['title'] = _l('salary_setup');

        if ($this->input->is_ajax_request()) {
            $this->app->get_table_data('salarysetup');
        }

        $data['pay_modes']     = $this->payment_modes_model->get();
        $data['ss_employee']   = $this->employee_model->get_employee();
        $data['salary_type_o'] = $this->payroll_model->get_salary_type_o();
        $data['salary_type_z'] = $this->payroll_model->get_salary_type_z();
        $data['currency']      = $this->currencies_model->get_base_currency();
        
        $this->load->view('admin/payroll/salarysetup_form_', $data);
    }

    public function create_salary_generate()
    { 
        $data['title'] = _l('salary_gen');

        if ($this->input->is_ajax_request()) {
            $this->app->get_table_data('salarygenerate');
        }

        $this->load->view('admin/payroll/salary_generate_form', $data);
    }

    public function delete_salaryname($id)
    {
        $del = $this->payroll_model->delete_salaryname($id);
        if($del) {
            set_alert('success', _l('deleted_suc'));
            redirect(admin_url('payroll/create_salary_generate'));
        }
    }

    public function salary_date($id)
    { 
        $data['title'] = _l('salary_gen');

        if ($this->input->is_ajax_request()) {
            $this->app->get_table_data('salarydate');
        }

        $this->load->view('admin/payroll/salary_generate_date', $data);
    }

    public function generate_salary_month()
    {
        list($month,$year) = explode(' ',$this->input->post('salary_name'));
        $query = $this->db->select('*')->from(db_prefix() . 'salary_sheet_generate')->where('name',$this->input->post('salary_name'))->get()->num_rows();
        if ($query > 0) {
            set_alert('danger', _l('already_generated'));
            redirect(admin_url('payroll/create_salary_generate'));
        }

        switch ($month)
        {
            case "January":
                $month = '1';
                break;
            case "February":
                $month = '2';
                break;
            case "March":
                $month = '3';
                break;
            case "April":
                $month = '4';
                break;
            case "May":
                $month = '5';
                break;
            case "June":
                $month = '6';
                break;
            case "July":
                $month = '7';
                break;
            case "August":
                $month = '8';
                break;
            case "September":
                $month = '9';
                break;
            case "October":
                $month = '10';
                break;
            case "November":
                $month = '11';
                break;
            case "December":
                $month = '12';
                break;
        }
        $fdate   = $year.'-'.$month.'-'.'1';
        $lastday = date('t',strtotime($fdate));
        
        $startd  = $fdate;
        $edate   = $year.'-'.$month.'-'.$lastday;

        $salary_generate = [
            'name'           => $this->input->post('salary_name', true),
            'start_date'     => $startd,
            'end_date'       => $edate,
            'generated_by'   => get_staff_user_id(),
            'generated_date' => date('Y-m-d'),
        ];

        $salary_insert = $this->payroll_model->insert_salary_g($salary_generate);

        $salaryname = $this->db->insert_id();

        // calculate salary

        $emp_proj   =   $this->db->select('*')
                                 ->where('MONTH(att_time)', $month)
                                 ->where('YEAR(att_time)', $year)
                                 ->group_by('uid')
                                 ->get(db_prefix() . 'attendance_history')
                                 ->result_array();

        if(sizeof($emp_proj) > 0) {
            foreach($emp_proj as $proj_value) 
            {
                // employee total salary
                $employee = $this->db->select('employee_id')->where('employee_id', $proj_value['uid'])->group_by('employee_id')->get(db_prefix() . 'employee_salary_setup')->result_array();
                if (sizeof($employee) > 0) {
                    foreach($employee as $value) {
                        $aAmount   = $this->db->join(db_prefix() . 'employee', db_prefix() . 'employee.emp_id=' . db_prefix() . 'employee_salary_setup.employee_id')->where('employee_id', $value['employee_id'])->get(db_prefix() . 'employee_salary_setup')->row();

                        $tot_benefits = $this->db->where('employee_id', $value['employee_id'])->where('type', null)->get(db_prefix() . 'employee_salary_setup')->result_array();
                        $tot_bonus    = $this->db->where('employee_id', $value['employee_id'])->where('type', 'bonus')->get(db_prefix() . 'employee_salary_setup')->result_array();

                        $Amount_benefits = 0;
                        foreach($tot_benefits as $ben) {
                            $ben             = $ben['amount'];
                            $Amount_benefits = $Amount_benefits + $ben;
                        }
                        $Amount_bonus = 0;
                        foreach($tot_bonus as $bon) {
                            $bon          = $bon['amount'];
                            $Amount_bonus = $Amount_bonus + $bon;
                        }
                        
                        $Amount  = $aAmount->total;
                        $startd  = $startd;
                        $end     = $edate;
                        
                        // All
                        $atten_in   =   $this->db->select('*')
                                                ->where('uid', $value['employee_id'])
                                                ->where('MONTH(att_time)', $month)
                                                ->where('YEAR(att_time)', $year)
                                                ->group_by('att_time')
                                                ->get(db_prefix() . 'attendance_history')
                                                ->result_array();
                        // AM & PM
                        $att_in_in =  $this->db->query('SELECT * FROM ' . db_prefix() . 'attendance_history WHERE uid =' . $value['employee_id'] . ' AND (MONTH(att_time) =' . $month . ' AND YEAR(att_time) =' . $year . ') AND (period = "AM" OR period = "PM") GROUP BY att_time')->result_array();
                        // Regular holiday AMPM
                        $att_in_reg =  $this->db->query('SELECT * FROM ' . db_prefix() . 'attendance_history WHERE uid =' . $value['employee_id'] . ' AND (MONTH(att_time) =' . $month . ' AND YEAR(att_time) =' . $year . ') AND (period = "AM" OR period = "PM" ) AND holiday_type = "Regular" GROUP BY att_time')->result_array();
                        // Special holiday AMPM
                        $att_in_spe =  $this->db->query('SELECT * FROM ' . db_prefix() . 'attendance_history WHERE uid =' . $value['employee_id'] . ' AND (MONTH(att_time) =' . $month . ' AND YEAR(att_time) =' . $year . ') AND (period = "AM" OR period = "PM" ) AND holiday_type = "Special Non-Working" GROUP BY att_time')->result_array();

                        // Whole Day
                        $att_inn    = $this->db->query('SELECT * FROM ' . db_prefix() . 'attendance_history WHERE uid =' . $value['employee_id'] . ' AND (MONTH(att_time) =' . $month . ' AND YEAR(att_time) =' . $year . ') AND (period = "Whole Day" OR period = "sick" OR period = "onleave") GROUP BY att_time')->result_array();
                        // $att_inn    =   $this->db->select('*')
                        //                         ->where('uid', $value['employee_id'])
                        //                         ->where('MONTH(att_time)', $month)
                        //                         ->where('YEAR(att_time)', $year)
                        //                         ->where('period', 'Whole Day')
                        //                         ->group_by('att_time')
                        //                         ->get(db_prefix() . 'attendance_history')
                        //                         ->result_array();
                        // Regular Holiday Whole Day
                        $att_inn_reg    =   $this->db->select('*')
                                                ->where('uid', $value['employee_id'])
                                                ->where('MONTH(att_time)', $month)
                                                ->where('YEAR(att_time)', $year)
                                                ->where('period', 'Whole Day')
                                                ->where('holiday_type', 'Regular')
                                                ->group_by('att_time')
                                                ->get(db_prefix() . 'attendance_history')
                                                ->result_array();
                        // Special Holiday Whole Day
                        $att_inn_spe    =   $this->db->select('*')
                                                ->where('uid', $value['employee_id'])
                                                ->where('MONTH(att_time)', $month)
                                                ->where('YEAR(att_time)', $year)
                                                ->where('period', 'Whole Day')
                                                ->where('holiday_type', 'Special Non-Working')
                                                ->group_by('att_time')
                                                ->get(db_prefix() . 'attendance_history')
                                                ->result_array();

                        $att_in_    =   $this->db->where('uid', $value['employee_id'])
                                                ->where('MONTH(att_time)', $month)
                                                ->where('YEAR(att_time)', $year)
                                                ->where('period !=', '---')
                                                ->group_by('att_time')
                                                ->get(db_prefix() . 'attendance_history')
                                                ->result_array();

                        $att_in_ampm   = count($att_in_in) * 4;
                        $total_wd      = count($att_inn) * 8;

                        // holiday
                        $att_in_reg    = count($att_in_reg) * 4;
                        $att_in_spe    = count($att_in_spe) * 4;
                        // holiday
                        $total_wdreg   = count($att_inn_reg) * 8;
                        $total_wdspe   = count($att_inn_spe) * 8;
                        // total holidays
                        $total_reg_holiday_ = $att_in_reg + $total_wdreg;
                        $total_spe_holiday_ = $att_in_spe + $total_wdspe;

                        // total hours
                        $total_hours = $att_in_ampm + $total_wd;

                        $tot_absent = 0;
                        foreach($atten_in as $tot_abst) {
                            $tot_absent = $tot_absent + $tot_abst['absent'];
                        }
                        $tot_absent_reg = 0;
                        foreach($att_inn_reg as $tot_abst_reg) {
                            $tot_absent_reg = $tot_absent_reg + $tot_abst_reg['absent'];
                        }
                        $tot_absent_spe = 0;
                        foreach($att_inn_spe as $tot_abst_spe) {
                            $tot_absent_spe = $tot_absent_spe + $tot_abst_spe['absent'];
                        }

                        // total work in hours
                        $worhour = $total_hours - $tot_absent;
                        // total regular holiday
                        $total_reg_holiday = $total_reg_holiday_ - $tot_absent_reg;
                        // total special holiday
                        $total_spe_holiday = $total_spe_holiday_ - $tot_absent_spe;

                        // addition or deduction amount
                        $netAmount = $Amount;

                        $ra = $aAmount->rate;
                        $salary_perhour = $ra / 8;

                        $ssx = 0;
                        foreach($atten_in as $row_wd) {
                            $sx = date('l', strtotime($row_wd['att_time']));
                            if($sx == 'Sunday') {
                                $ssx = $ssx + 8;
                            }
                        }
                        $sund_abs = 0;
                        foreach($atten_in as $row_wd) {
                            $tot_sund_absent = $sund_abs + $row_wd['absent'];
                        }
                        $total_sunday_hr = $ssx - $tot_sund_absent; // total sunday in hours

                        // overtime
                        $otx = 0;
                        $sss = 0;
                        foreach($atten_in as $row_attin) {
                            $otx = $row_attin['overtime'] + $otx;

                            $ss = date('l', strtotime($row_attin['att_time']));

                            if($ss == 'Sunday') {
                                $sss = $sss + 1;
                            }
                        }
                        $ott = $otx;
                        $ot = $salary_perhour * $ott;

                        // total work in days
                        $workingper = count($att_in_);

                        // total sunday
                        $tot_sun = 0;
                        foreach($att_in_ as $row_attin_) {
                            $sun = date('l', strtotime($row_attin_['att_time']));

                            if($sun == 'Sunday') {
                                $tot_sun = $tot_sun + 1;
                            }
                        }

                        // total regular days
                        $_days = $workingper - $tot_sun;
                        // total regular hours
                        $_hour = $worhour - $total_sunday_hr;

                        if($aAmount->sal_type == 2) {
                            if($Amount < 0) {
                                $totsal  = $salary_perhour * $worhour;
                                $aa = substr($Amount, 1);
                                $tot_sal = $totsal + $ot; // ($totsal - $aa) + $ot;
                            } elseif($Amount > 0) {
                                $totsal  = $salary_perhour * $worhour;
                                $tot_sal = $totsal + $ot; // $totsal + $Amount + $ot;
                            } else {
                                $tot_sal  = ($salary_perhour * $worhour) + $ot;
                            }
                        } elseif($aAmount->sal_type == 1) {
                            if($Amount < 0) {
                                $totsal  = $ra * $worhour;
                                $aa = substr($Amount, 1);
                                $tot_sal = $totsal + $ot; // ($totsal - $aa) + $ot;
                            } elseif($Amount > 0) {
                                $totsal  = $ra * $worhour;
                                $tot_sal = $totsal + $ot; // $totsal + $Amount + $ot;
                            } else {
                                $tot_sal  = ($ra * $worhour) + $ot;
                            }
                        }

                        $validate = $this->db->where('employee_id', $value['employee_id'])->where('salary_name_id', $salaryname)->get(db_prefix() . 'employee_salary_payment')->row();
                        if($validate == 0) {
                            $paymentData = [
                                'employee_id'           => $value['employee_id'],
                                'salary_setup'          => $netAmount,
                                'salary_setup_benefits' => $Amount_benefits,
                                'salary_setup_bonus'    => $Amount_bonus,
                                'total_salary'          => $tot_sal,
                                'total_sunday'          => $tot_sun,
                                'regular_holiday'       => $total_reg_holiday,
                                'special_holiday'       => $total_spe_holiday,
                                'total_working_hours'   => $worhour,
                                'working_period'        => $workingper,
                                'overtime'              => $ott,
                                'salary_name_id'        => $salaryname,
                                'paymentmode'           => $aAmount->paymentmode,
                                'currency'              => 3,
                                'created_date'          => date('Y-m-d'),
                            ];

                            if(!empty($paymentData) && $worhour != 0 && $workingper != 0) {
                                $this->db->insert(db_prefix() . 'employee_salary_payment', $paymentData);

                                $emp_sal_pay_id = $this->db->insert_id();

                                if(!empty($emp_sal_pay_id)) {
                                    $proj = $this->db->select('*')
                                    ->where('uid', $value['employee_id'])
                                    ->where('MONTH(att_time_min)', $month)
                                    ->where('YEAR(att_time_min)', $year)
                                    ->where('MONTH(att_time_max)', $month)
                                    ->where('YEAR(att_time_max)', $year)
                                    ->group_by('project_id')
                                    ->get(db_prefix() . 'attendance_project')
                                    ->result_array();
                                    
                                    foreach($proj as $row_proj) {
                                        $esp_p = [
                                            'emp_sal_pay_id' => $emp_sal_pay_id,
                                            'project_id'     => $row_proj['project_id'],
                                        ];
                                        $this->db->insert(db_prefix() . 'employeesalarypayment_project', $esp_p);
                                    }
                                }
                            }
                        }
                    }
                } else {
                    set_alert('danger', 'Please enter data in Salary Setup first');
                    $del_sg = $this->payroll_model->del_salary_generate($salaryname);
                    if($del_sg) {
                        redirect(admin_url('payroll/create_s_setup'));
                    }
                }
            }
        } else {
            set_alert('danger', 'No Attendance');
            $del_sg = $this->payroll_model->del_salary_generate($salaryname);
            if($del_sg) {
                redirect(admin_url('attendance_form/atten_form'));
            }
        }

        set_alert('success', _l('added_successfully', _l('salary')));
        redirect(admin_url('payroll/emp_payment_view'));
    }

    public function generate_salary_period()
    {
        $date1_ = $this->input->post('date1', true);
        $date2_ = $this->input->post('date2', true);
        
        $date1 = DateTime::createFromFormat('m-d-Y', $date1_)->format('Y-m-d');
        $date2 = DateTime::createFromFormat('m-d-Y', $date2_)->format('Y-m-d');

        $q = $this->db->query('SELECT * FROM ' . db_prefix() . 'salary_sheet_generate WHERE "' . $date1 . '" BETWEEN start_date AND end_date OR "' . $date2 . '" BETWEEN start_date AND end_date')->num_rows();
        if ($q > 0) {
            set_alert('danger', _l('already_generated'));
            redirect(admin_url('payroll/create_salary_generate'));
        }

        list($year1,$month1,$day1) = explode('-',$date1);
        list($year2,$month2,$day2) = explode('-',$date2);
        switch ($month1)
        {
            case "1":
                $mm1 = 'January';
                break;
            case "2":
                $mm1 = 'February';
                break;
            case "3":
                $mm1 = 'March';
                break;
            case "4":
                $mm1 = 'April';
                break;
            case "5":
                $mm1 = 'May';
                break;
            case "6":
                $mm1 = 'June';
                break;
            case "7":
                $mm1 = 'July';
                break;
            case "8":
                $mm1 = 'August';
                break;
            case "9":
                $mm1 = 'September';
                break;
            case "10":
                $mm1 = 'October';
                break;
            case "11":
                $mm1 = 'November';
                break;
            case "12":
                $mm1 = 'December';
                break;
        }
        switch ($month2)
        {
            case "1":
                $mm2 = 'January';
                break;
            case "2":
                $mm2 = 'February';
                break;
            case "3":
                $mm2 = 'March';
                break;
            case "4":
                $mm2 = 'April';
                break;
            case "5":
                $mm2 = 'May';
                break;
            case "6":
                $mm2 = 'June';
                break;
            case "7":
                $mm2 = 'July';
                break;
            case "8":
                $mm2 = 'August';
                break;
            case "9":
                $mm2 = 'September';
                break;
            case "10":
                $mm2 = 'October';
                break;
            case "11":
                $mm2 = 'November';
                break;
            case "12":
                $mm2 = 'December';
                break;
        }

        $salary_name = $mm1.' '.$day1.', '.$year1.' — '.$mm2.' '.$day2.', '.$year2;
        $startd      = $date1;
        $edate       = $date2;

        $salary_generate = [
            'name'          => $salary_name,
            'start_date'    => $startd,
            'end_date'      => $edate,
            'generated_by'  => get_staff_user_id(),
            'generated_date' => date('Y-m-d'),
        ];

        $salary_insert = $this->payroll_model->insert_salary_g($salary_generate);

        $salaryname = $this->db->insert_id();

        // calculate salary

        $emp_proj   =   $this->db->select('*')
                                 ->where('DATE(att_time) >=',date('Y-m-d', strtotime($startd)))
                                 ->where('DATE(att_time) <=',date('Y-m-d', strtotime($edate)))
                                 ->group_by('uid')
                                 ->get(db_prefix() . 'attendance_history')
                                 ->result_array();

        if(sizeof($emp_proj) > 0) {
            foreach($emp_proj as $proj_value) 
            {
                // employee total salary
                $employee = $this->db->select('employee_id')->where('employee_id', $proj_value['uid'])->group_by('employee_id')->get(db_prefix() . 'employee_salary_setup')->result_array();
                if (sizeof($employee) > 0) {
                    foreach($employee as $value) {
                        $aAmount   = $this->db->join(db_prefix() . 'employee', db_prefix() . 'employee.emp_id=' . db_prefix() . 'employee_salary_setup.employee_id')->where('employee_id', $value['employee_id'])->get(db_prefix() . 'employee_salary_setup')->row();

                        $tot_benefits = $this->db->where('employee_id', $value['employee_id'])->where('type', null)->get(db_prefix() . 'employee_salary_setup')->result_array();
                        $tot_bonus    = $this->db->where('employee_id', $value['employee_id'])->where('type', 'bonus')->get(db_prefix() . 'employee_salary_setup')->result_array();

                        $Amount_benefits = 0;
                        foreach($tot_benefits as $ben) {
                            $ben             = $ben['amount'];
                            $Amount_benefits = $Amount_benefits + $ben;
                        }
                        $Amount_bonus = 0;
                        foreach($tot_bonus as $bon) {
                            $bon          = $bon['amount'];
                            $Amount_bonus = $Amount_bonus + $bon;
                        }
                        
                        $Amount  = $aAmount->total;
                        $startd  = $startd;
                        $end     = $edate;
                        
                        // All
                        $atten_in   =   $this->db->select('*')
                                                ->where('uid', $value['employee_id'])
                                                ->where('DATE(att_time) >=',date('Y-m-d', strtotime($startd)))
                                                ->where('DATE(att_time) <=',date('Y-m-d', strtotime($end)))
                                                ->group_by('att_time')
                                                ->get(db_prefix() . 'attendance_history')
                                                ->result_array();
                        // AM & PM
                        $att_in_in =  $this->db->query('SELECT * FROM ' . db_prefix() . 'attendance_history WHERE uid =' . $value['employee_id'] . ' AND (DATE(att_time) >="' . date('Y-m-d', strtotime($startd)) . '" AND DATE(att_time) <="' . date('Y-m-d', strtotime($end)) . '") AND (period = "AM" OR period = "PM") GROUP BY att_time')->result_array();
                        // Regular holiday AMPM
                        $att_in_reg =  $this->db->query('SELECT * FROM ' . db_prefix() . 'attendance_history WHERE uid =' . $value['employee_id'] . ' AND (DATE(att_time) >="' . date('Y-m-d', strtotime($startd)) . '" AND DATE(att_time) <="' . date('Y-m-d', strtotime($end)) . '") AND (period = "AM" OR period = "PM" ) AND holiday_type = "Regular" GROUP BY att_time')->result_array();
                        // Special holiday AMPM
                        $att_in_spe =  $this->db->query('SELECT * FROM ' . db_prefix() . 'attendance_history WHERE uid =' . $value['employee_id'] . ' AND (DATE(att_time) >="' . date('Y-m-d', strtotime($startd)) . '" AND DATE(att_time) <="' . date('Y-m-d', strtotime($end)) . '") AND (period = "AM" OR period = "PM" ) AND holiday_type = "Special Non-Working" GROUP BY att_time')->result_array();

                        // Whole Day
                        $att_inn    = $this->db->query('SELECT * FROM ' . db_prefix() . 'attendance_history WHERE uid =' . $value['employee_id'] . ' AND (DATE(att_time) >="' . date('Y-m-d', strtotime($startd)) . '" AND DATE(att_time) <="' . date('Y-m-d', strtotime($end)) . '") AND (period = "Whole Day" OR period = "sick" OR period = "onleave") GROUP BY att_time')->result_array();
                        // Regular Holiday Whole Day
                        $att_inn_reg    =   $this->db->select('*')
                                                ->where('uid', $value['employee_id'])
                                                ->where('DATE(att_time) >=',date('Y-m-d', strtotime($startd)))
                                                ->where('DATE(att_time) <=',date('Y-m-d', strtotime($end)))
                                                ->where('period', 'Whole Day')
                                                ->where('holiday_type', 'Regular')
                                                ->group_by('att_time')
                                                ->get(db_prefix() . 'attendance_history')
                                                ->result_array();
                        // Special Holiday Whole Day
                        $att_inn_spe    =   $this->db->select('*')
                                                ->where('uid', $value['employee_id'])
                                                ->where('DATE(att_time) >=',date('Y-m-d', strtotime($startd)))
                                                ->where('DATE(att_time) <=',date('Y-m-d', strtotime($end)))
                                                ->where('period', 'Whole Day')
                                                ->where('holiday_type', 'Special Non-Working')
                                                ->group_by('att_time')
                                                ->get(db_prefix() . 'attendance_history')
                                                ->result_array();

                        $att_in_    =   $this->db->where('uid', $value['employee_id'])
                                                ->where('DATE(att_time) >=',date('Y-m-d', strtotime($startd)))
                                                ->where('DATE(att_time) <=',date('Y-m-d', strtotime($end)))
                                                ->where('period !=', '---')
                                                ->group_by('att_time')
                                                ->get(db_prefix() . 'attendance_history')
                                                ->result_array();

                        $att_in_ampm   = count($att_in_in) * 4;
                        $total_wd      = count($att_inn) * 8;

                        // holiday
                        $att_in_reg    = count($att_in_reg) * 4;
                        $att_in_spe    = count($att_in_spe) * 4;
                        // holiday
                        $total_wdreg   = count($att_inn_reg) * 8;
                        $total_wdspe   = count($att_inn_spe) * 8;
                        // total holidays
                        $total_reg_holiday_ = $att_in_reg + $total_wdreg;
                        $total_spe_holiday_ = $att_in_spe + $total_wdspe;

                        // total hours
                        $total_hours = $att_in_ampm + $total_wd;
                        
                        $tot_absent = 0;
                        foreach($atten_in as $tot_abst) {
                            $tot_absent = $tot_absent + $tot_abst['absent'];
                        }
                        $tot_absent_reg = 0;
                        foreach($att_inn_reg as $tot_abst_reg) {
                            $tot_absent_reg = $tot_absent_reg + $tot_abst_reg['absent'];
                        }
                        $tot_absent_spe = 0;
                        foreach($att_inn_spe as $tot_abst_spe) {
                            $tot_absent_spe = $tot_absent_spe + $tot_abst_spe['absent'];
                        }

                        // total work in hours
                        $worhour = $total_hours - $tot_absent;
                        // total regular holiday
                        $total_reg_holiday = $total_reg_holiday_ - $tot_absent_reg;
                        // total special holiday
                        $total_spe_holiday = $total_spe_holiday_ - $tot_absent_spe;

                        // addition or deduction amount
                        $netAmount = $Amount;

                        $ra = $aAmount->rate;
                        $salary_perhour = $ra / 8;

                        // sunday in hours
                        $tss = 0;
                        foreach($att_in_in as $row_ampm) {
                            $s = date('l', strtotime($row_ampm['att_time']));
                            if($s == 'Sunday') {
                                $tss = $tss + 4;
                            }
                        }
                        $ssx = 0;
                        foreach($att_inn as $row_wd) {
                            $sx = date('l', strtotime($row_wd['att_time']));
                            if($sx == 'Sunday') {
                                $ssx = $ssx + 8;
                            }
                        }
                        $sund_abs = 0;
                        foreach($atten_in as $row_wd) {
                            $tot_sund_absent = $sund_abs + $row_wd['absent'];
                        }
                        $total_sunday_hr = $ssx - $tot_sund_absent; // total sunday in hours

                        // overtime
                        $otx = 0;
                        $sss = 0;
                        foreach($atten_in as $row_attin) {
                            $otx = $row_attin['overtime'] + $otx;

                            $ss = date('l', strtotime($row_attin['att_time']));

                            if($ss == 'Sunday') {
                                $sss = $sss + 1;
                            }
                        }
                        $ott = $otx;
                        $ot = $salary_perhour * $ott;

                        // total work in days
                        $workingper = count($att_in_);

                        // total sunday
                        $tot_sun = 0;
                        foreach($att_in_ as $row_attin_) {
                            $sun = date('l', strtotime($row_attin_['att_time']));

                            if($sun == 'Sunday') {
                                $tot_sun = $tot_sun + 1;
                            }
                        }

                        // total regular days
                        $_days = $workingper - $tot_sun;
                        // total regular hours
                        $_hour = $worhour - $total_sunday_hr;

                        if($aAmount->sal_type == 2) {
                            if($Amount < 0) {
                                $totsal  = $salary_perhour * $worhour;
                                $aa = substr($Amount, 1);
                                $tot_sal = $totsal + $ot; // ($totsal - $aa) + $ot;
                            } elseif($Amount > 0) {
                                $totsal  = $salary_perhour * $worhour;
                                $tot_sal = $totsal + $ot; // $totsal + $Amount + $ot;
                            } else {
                                $tot_sal  = ($salary_perhour * $worhour) + $ot;
                            }
                        } elseif($aAmount->sal_type == 1) {
                            if($Amount < 0) {
                                $totsal  = $ra * $worhour;
                                $aa = substr($Amount, 1);
                                $tot_sal = $totsal + $ot; // ($totsal - $aa) + $ot;
                            } elseif($Amount > 0) {
                                $totsal  = $ra * $worhour;
                                $tot_sal = $totsal + $ot; // $totsal + $Amount + $ot;
                            } else {
                                $tot_sal  = ($ra * $worhour) + $ot;
                            }
                        }

                        $validate = $this->db->where('employee_id', $value['employee_id'])->where('salary_name_id', $salaryname)->get(db_prefix() . 'employee_salary_payment')->row();
                        if($validate == 0) {
                            $paymentData = [
                                'employee_id'           => $value['employee_id'],
                                'salary_setup'          => $netAmount,
                                'salary_setup_benefits' => $Amount_benefits,
                                'salary_setup_bonus'    => $Amount_bonus,
                                'total_salary'          => $tot_sal,
                                'total_sunday'          => $tot_sun,
                                'regular_holiday'       => $total_reg_holiday,
                                'special_holiday'       => $total_spe_holiday,
                                'total_working_hours'   => $worhour,
                                'working_period'        => $workingper, // $_days,
                                'overtime'              => $ott,
                                'salary_name_id'        => $salaryname,
                                'paymentmode'           => $aAmount->paymentmode,
                                'currency'              => 3,
                                'created_date'          => date('Y-m-d'),
                            ];

                            if(!empty($paymentData) && $worhour != 0 && $workingper != 0) {
                                $this->db->insert(db_prefix() . 'employee_salary_payment', $paymentData);

                                $emp_sal_pay_id = $this->db->insert_id();

                                if(!empty($emp_sal_pay_id)) {
                                    $proj = $this->db->select('*')
                                    ->where('uid', $value['employee_id'])
                                    ->where('DATE(att_time_min) >=', $startd)
                                    ->where('DATE(att_time_max) <=', $end)
                                    ->group_by('project_id')
                                    ->get(db_prefix() . 'attendance_project')
                                    ->result_array();
                                    
                                    foreach($proj as $row_proj) {
                                        $esp_p = [
                                            'emp_sal_pay_id' => $emp_sal_pay_id,
                                            'project_id'     => $row_proj['project_id'],
                                        ];
                                        $this->db->insert(db_prefix() . 'employeesalarypayment_project', $esp_p);
                                    }
                                }
                            }
                        }
                    }
                } else {
                    set_alert('danger', 'Please enter data in Salary Setup first');
                    $del_sg = $this->payroll_model->del_salary_generate($salaryname);
                    if($del_sg) {
                        redirect(admin_url('payroll/create_s_setup'));
                    }
                }
            }
        } else {
            set_alert('danger', 'No Attendance');
            $del_sg = $this->payroll_model->del_salary_generate($salaryname);
            if($del_sg) {
                redirect(admin_url('attendance_form/atten_form'));
            }
        }

        set_alert('success', _l('added_successfully', _l('salary')));
        redirect(admin_url('payroll/emp_payment_view'));
    }

    public function emp_rate_type()
    {
        $emp = $this->input->post('emp');
        if(!empty($emp)) {
            $emp_mod = $this->db->where('emp_id', $emp)->get(db_prefix() . 'employee')->row();

            $basic       = $emp_mod->rate;
            $rate_type   = $emp_mod->rate_type;
            
            $data_emp = [
                'rate'           => $basic,
                'rate_type'      => $rate_type,
            ];

            echo json_encode($data_emp);
        }
    }
    public function emp_rate_type_bonus()
    {
        $emp = $this->input->post('emp');
        if(!empty($emp)) {
            $where1 = date('m/Y');
            $where2 = date('m/Y', strtotime("-1 YEAR"));
            $bonus_mod = $this->db->where('bonus_date <=', $where2)->where('bonus_date <=', $where1)->where('bonus_emp_id', $emp)->get(db_prefix() . 'bonus_setup')->result_array();

            $html = '';
            foreach($bonus_mod as $key => $each_bonus) {
                $html .= '<tr class="tr_id tr_'.$key.'">';
                $html .= '<th style="padding: 10px;" class="bonusnameid">';
                $html .= $each_bonus['bonus_name'] . '</th>';
                $html .= '<td style="padding: 10px;" class="bonusamountid">';
                $html .= '<input type="text" name="bonus['.$each_bonus['bonus_id'].']" class="form-control addamount bonusid" id="bonusid" onkeyup="summary()" value="'.$each_bonus['bonus_amount'].'.00">';
                $html .= '</td><td><span style="cursor:pointer;display:inline-block;" onclick="removeBtn('.$key.')"><i class="fa fa-window-close" style="font-size:20px;color:red"></i></span></td></tr>';
            }
            echo $html;
            die();
        }
    }

    public function emp_payment_view()
    {   
        $data['title'] = _l('manage_emp');

        if ($this->input->is_ajax_request()) {
            $this->app->get_table_data('salary');
        }

        $this->load->view('admin/payroll/paymentview', $data);
    }

    public function delete_salary($id)
    {
        $del = $this->payroll_model->delete_salary($id);
        if($del) {
            set_alert('success', _l('deleted_suc'));
            redirect(admin_url('payroll/emp_payment_view'));
        }
    }
    
    public function paymentview_edit($id)
    {   
        $data['title'] = _l('manage_emp');

        if ($this->input->is_ajax_request()) {
            $this->app->get_table_data('salary');
        }

        $data['pv_id']      = $this->payroll_model->get_emp_salarypayment($id);
        $data['pay_modes']  = $this->payment_modes_model->get();

        $this->load->view('admin/payroll/paymentview', $data);
    }

    public function update_payment_view($id)
    {
        $emp_id = $this->input->post('pv_emp_id');

        $pv_up = [
            'paymentmode'         => $this->input->post('payment_modes', true),
            'total_working_hours' => $this->input->post('pv_total_working_hours', true),
            'working_period'      => $this->input->post('pv_working_period', true),
            'overtime'            => $this->input->post('pv_overtime', true),
            'regular_ot_rate'     => $this->input->post('pv_regular_ot_rate', true),
            'special_ot_rate'     => $this->input->post('pv_special_ot_rate', true),
            'total_salary'        => $this->input->post('pv_total_salary', true),
            'remarks'             => $this->input->post('remarks', true),
        ];

        if($this->payroll_model->update_pv_model($pv_up, $id)) {
            $pv_up_emp = [
                'skill_index'     => $this->input->post('pv_skillindex', true),
            ];
            if($this->employee_model->update_empp($pv_up_emp, $emp_id)) {
                set_alert('success', _l('updated_successfully'));
                redirect(admin_url('payroll/emp_payment_view'));
            } else {
                set_alert('success', _l('updated_successfully'));
                redirect(admin_url('payroll/emp_payment_view'));
            }
        } else {
            set_alert('danger', _l('failed_s'));
            redirect(admin_url('payroll/emp_payment_view'));
        }
    }

    public function create_benefits()
    {
        $ben = [
            'benefit_name'          => $this->input->post('benefit_name', true),
            'benefit_description'   => $this->input->post('benefit_desc', true),
            'benefit_type'          => $this->input->post('benefit_type', true),
            'default_amount'        => $this->input->post('benefit_d_amount', true),
            'status'                => $this->input->post('benefit_status', true),
            'currency'              => 3,
        ];

        if($this->payroll_model->add_benefit($ben)) {
            set_alert('success', _l('added_successfully', _l('salary_type')));
            redirect(admin_url('payroll/create_salary_setup'));
        } else {
            set_alert('danger', _l('failed_ben'));
            redirect(admin_url('payroll/create_salary_setup'));
        }

    }

    public function update_benefits($id)
    {
        $u_ben = [
            'benefit_name'          => $this->input->post('benefit_name', true),
            'benefit_description'   => $this->input->post('benefit_desc', true),
            'benefit_type'          => $this->input->post('benefit_type', true),
            'default_amount'        => $this->input->post('benefit_d_amount', true),
            'status'                => $this->input->post('benefit_status', true),
        ];

        if($this->payroll_model->update_benefit($u_ben, $id)) {
            set_alert('success', _l('added_successfully', _l('benefit')));
            redirect(admin_url('payroll/create_salary_setup'));
        }
    }

    public function view_ss($id)
    {
        $data['title'] = _l('salary_setup');

        if ($this->input->is_ajax_request()) {
            $this->app->get_table_data('salarysetup');
        }
        // view
        $data['emp_s_s']            = $this->payroll_model->get_emp_ss($id);
        $data['emp_ss_amount_o']    = $this->payroll_model->get_emp_ss_amount_o($id);
        $data['emp_ss_amount_z']    = $this->payroll_model->get_emp_ss_amount_z($id);

        $data['pay_modes']          = $this->payment_modes_model->get();
        $data['ss_employee']        = $this->employee_model->get_employee();
        $data['salary_type_o']      = $this->payroll_model->get_salary_type_o();
        $data['salary_type_z']      = $this->payroll_model->get_salary_type_z();
        $data['currency']           = $this->currencies_model->get_base_currency();

        $this->load->view('admin/payroll/salarysetup_form', $data);
    }

    public function edit_ss($id)
    {
        $data['title'] = _l('salary_setup');

        if ($this->input->is_ajax_request()) {
            $this->app->get_table_data('salarysetup');
        }
        // edit
        $data['e_emp_s_s']            = $this->payroll_model->get_emp_ss($id);
        $data['e_emp_ss_amount_o']    = $this->payroll_model->get_emp_ss_amount_o($id);
        $data['e_emp_ss_amount_z']    = $this->payroll_model->get_emp_ss_amount_z($id);

        $data['pay_modes']          = $this->payment_modes_model->get();
        $data['ss_employee']        = $this->employee_model->get_employee();
        $data['salary_type_o']      = $this->payroll_model->get_salary_type_o();
        $data['salary_type_z']      = $this->payroll_model->get_salary_type_z();
        $data['currency']           = $this->currencies_model->get_base_currency();

        $this->load->view('admin/payroll/salarysetup_form', $data);
    }

    public function update_s_setup($id)
    {
        $emp_id = $this->input->post('edit_id');
        // add/update bonus
        $bonus = $this->input->post('bonus');
        foreach($bonus as $key => $value) {
            $get_empbonus = $this->payroll_model->get_emp_bonus($emp_id, $key);
            if(count($get_empbonus) > 0) {
                $bonus_save = [
                    'employee_id'       => $emp_id,
                    'sal_type'          => $this->input->post('setup_rate_type_id', true),
                    'salary_type_id'    => $key,
                    'amount'            => $value,
                    'total'             => $this->input->post('total', true),
                    'paymentmode'       => $this->input->post('payment_modes', true),
                ];
                $upd = $this->payroll_model->update_salary_setup_bonus($bonus_save, $id);
            } 
            else {
                $bonus_save = [
                    'employee_id'       => $emp_id,
                    'sal_type'          => $this->input->post('setup_rate_type_id', true),
                    'salary_type_id'    => $key,
                    'type'              => 'bonus',
                    'amount'            => $value,
                    'total'             => $this->input->post('total', true),
                    'create_date'       => date('Y-m-d'),
                    'paymentmode'       => $this->input->post('payment_modes', true),
                    'currency'          => 3,
                ];
                $inss = $this->payroll_model->create_salary_setup($bonus_save);
            }
        }
        // remove bonus
        $remove_id = $this->input->post('remove_id');
        if(!empty($remove_id)) {
            foreach($remove_id as $key => $value) {
                $get_empbonus = $this->payroll_model->get_emp_bonus($emp_id, $value);
                if(count($get_empbonus) > 0) {
                    $this->db->where('employee_id', $emp_id)->where('salary_type_id', $value)->where('type', 'bonus')->delete(db_prefix().'employee_salary_setup');
                }
            }
        }
        $removeid = $this->input->post('removeid');
        if(!empty($removeid)) {
            foreach($removeid as $key => $value) {
                $get_empbonus = $this->payroll_model->get_emp_bonus($emp_id, $value);
                if(count($get_empbonus) > 0) {
                    $this->db->where('employee_id', $emp_id)->where('salary_type_id', $value)->where('type', 'bonus')->delete(db_prefix().'employee_salary_setup');
                }
            }
        }
        // update benefits
        $amount = $this->input->post('amount');
        foreach($amount as $key => $value) {
            $salary_setup = [
                'employee_id'       => $emp_id,
                'sal_type'          => $this->input->post('setup_rate_type_id', true),
                'salary_type_id'    => $key,
                'amount'            => $value,
                'total'             => $this->input->post('total', true),
                'paymentmode'       => $this->input->post('payment_modes', true),
            ];
            $upd = $this->payroll_model->update_salary_setup($salary_setup, $id);
        }

        set_alert('success', _l('updated_successfully', _l('salary_setup')));
        redirect(admin_url('payroll/view_ss/' . $emp_id));
    }

    public function delete_ss($id)
    {
        $del = $this->payroll_model->delete_emp_s_s($id);
        if($del) {
            set_alert('success', _l('deleted', _l('salary_setup')));
            redirect(admin_url('payroll/create_s_setup'));
        }
    }
    
    public function bonus_setup()
    {
        $data['title'] = _l('bonus_setup');
        
        if ($this->input->is_ajax_request()) {
            $this->app->get_table_data('bonus');
        }

        list($month,$year) = explode(' ',$this->input->post('month_year'));
        switch ($month)
        {
            case "January":
                $month = '01';
                break;
            case "February":
                $month = '02';
                break;
            case "March":
                $month = '03';
                break;
            case "April":
                $month = '04';
                break;
            case "May":
                $month = '05';
                break;
            case "June":
                $month = '06';
                break;
            case "July":
                $month = '07';
                break;
            case "August":
                $month = '08';
                break;
            case "September":
                $month = '09';
                break;
            case "October":
                $month = '10';
                break;
            case "November":
                $month = '11';
                break;
            case "December":
                $month = '12';
                break;
        }
        $month_year = $month . '/' . $year;
        $save_bonus = [
            'bonus_name'       => $this->input->post('bonus_name', true),
            'bonus_emp_id'     => $this->input->post('employee_name', true),
            'bonus_amount'     => $this->input->post('bonus_total_amount', true),
            'currency'         => 3,
            'bonus_date'       => $month_year,
            'bonus_date_added' => date('Y-m-d'),
        ];
        if($this->payroll_model->add_bonus($save_bonus)) {
            set_alert('success', _l('added_successfully', _l('bonus')));
            redirect(admin_url('payroll/bonus_setup'));
        }

        $data['employee'] = $this->employee_model->get_employee();

        $this->load->view('admin/payroll/bonus_setup', $data);
    }

    public function bonussetup_view($id)
    {
        $data['title'] = _l('bonus_setup');
        
        if ($this->input->is_ajax_request()) {
            $this->app->get_table_data('bonus');
        }

        $data['bonus_data'] = $this->payroll_model->get_bonus($id);
        $data['employee'] = $this->employee_model->get_employee();

        $this->load->view('admin/payroll/bonus_setup', $data);
    }

    public function bonussetup_edit($id)
    {
        $data['title'] = _l('bonus_setup');
        
        if ($this->input->is_ajax_request()) {
            $this->app->get_table_data('bonus');
        }

        $data['bonus_data_edit'] = $this->payroll_model->get_bonus($id);
        $data['employee'] = $this->employee_model->get_employee();

        $this->load->view('admin/payroll/bonus_setup', $data);
    }
    public function update_bonus($id)
    {   
        list($month,$year) = explode(' ',$this->input->post('month_year'));
        switch ($month)
        {
            case "January":
                $month = '01';
                break;
            case "February":
                $month = '02';
                break;
            case "March":
                $month = '03';
                break;
            case "April":
                $month = '04';
                break;
            case "May":
                $month = '05';
                break;
            case "June":
                $month = '06';
                break;
            case "July":
                $month = '07';
                break;
            case "August":
                $month = '08';
                break;
            case "September":
                $month = '09';
                break;
            case "October":
                $month = '10';
                break;
            case "November":
                $month = '11';
                break;
            case "December":
                $month = '12';
                break;
        }
        $month_year = $month . '/' . $year;
        $update_bonus = [
            'bonus_name'    => $this->input->post('bonus_name', true),
            'bonus_emp_id'  => $this->input->post('employee_name', true),
            'bonus_amount'  => $this->input->post('bonus_total_amount', true),
            'bonus_date'    => $month_year,
        ];
        if($this->payroll_model->update_bonus($update_bonus, $id)) {
            set_alert('success', _l('updated_successfully', _l('bonus')));
            redirect(admin_url('payroll/bonus_setup'));
        }   
    }

    public function delete_bonus($id)
    {
        $del = $this->payroll_model->delete_bonus($id);
        if($del) {
            set_alert('success', _l('deleted', _l('bonus')));
            redirect(admin_url('payroll/bonus_setup'));
        }
    }
}
