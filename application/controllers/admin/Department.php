<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Department extends AdminController
{
    public function __construct()
    {
        parent::__construct();  
        $this->load->model('Department_model');  
    }

    public function create_dept()
    {   
        $data['title'] = _l('department');
        $data['list_dept']       = $this->Department_model->get_department();
        $this->load->view('admin/department/manage',$data);
        
    }

    public function table() {

       $this->app->get_table_data('department');
    }

    public function dept_insert() {

        
        $dept_name=$this->input->post("dept_name");

        $data = array(
        'department_name' => $dept_name
        );
        $res = $this->Department_model->insert_dept($data);

            if($res == 1)
            {    
                set_alert('success', 'Department added successfully!');
                redirect(admin_url('department/create_dept'));
            }
    }

     public function fetch_dept($id){

        $data['title'] = _l('department');
        $data['show_dept'] = $this->Department_model->fetch_dept($id);
        $this->load->view('admin/department/edit',$data);


    }

    public function update_dept($id){

        $dept_name=$this->input->post("dept_name");

        $data = array(
        'department_name' => $dept_name
        );
        $this->Department_model->update_dept($data,$id);
        set_alert('success', 'Department updated successfully!');
        redirect(admin_url('department/create_dept'));


    }

    public function delete_dept($id){

        $this->Department_model->delete_dept($id); 
        set_alert('success', 'Department deleted successfully!');
        redirect(admin_url('department/create_dept'));


    }
}
