<?php

defined('BASEPATH') or exit('No direct script access allowed');

// $hasPermissionDelete = has_permission('department', '', 'delete');

// $custom_fields = get_table_custom_fields('department');
// $this->ci->db->query("SET sql_mode = ''");

$aColumns = [
    '1',
    db_prefix().'attendance_history.atten_his_id as atten_his_id',
    'att_time',
    db_prefix().'attendance_history.uid as uid',
    '(SELECT first_name FROM ' . db_prefix().'employee WHERE emp_id=uid) as fname',
    '(SELECT middle_name FROM ' . db_prefix().'employee WHERE emp_id=uid) as mname',
    '(SELECT last_name FROM ' . db_prefix().'employee WHERE emp_id=uid) as lname',
   
   
];

$sIndexColumn = 'atten_his_id';
$sTable       = db_prefix().'attendance_history';
$where        = [];
// Add blank where all filter can be stored
// $filter = [];

$join = [];

$result = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where, []);

$output  = $result['output'];
$rResult = $result['rResult'];

foreach ($rResult as $aRow) {
    $row = [];

    $row[] = $aRow['atten_his_id'];

    // Company
    $division_name  = $aRow['fname'].' '.$aRow['mname'].' '.$aRow['lname'];
    
    $url = admin_url('attendance_log/manage/' . $aRow['atten_his_id']);

    $division_name  = '<a href="' . $url . '">' . $division_name  . '</a>';

    $division_name  .= '<div class="row-options">';
    $division_name  .= '<a href="' . $url . '">' . _l('view') . '</a>';
    $division_name  .= ' | <a href="' . $url . '">' . _l('edit') . '</a>';

   
    $division_name  .= ' | <a href="' . admin_url('attendance_log/manage/' . $aRow['atten_his_id']) . '" class="text-danger _delete">' . _l('delete') . '</a>';

    $division_name  .= '</div>';

    $row[] = $division_name ;

    $row[] = $aRow['att_time'];

    
    $row['DT_RowClass'] = 'has-row-options';

    $row = hooks()->apply_filters('attendancelog_table_row_data', $row, $aRow);

    $output['aaData'][] = $row;
}
