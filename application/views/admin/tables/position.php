<?php

defined('BASEPATH') or exit('No direct script access allowed');

// $hasPermissionDelete = has_permission('customers', '', 'delete');

// $custom_fields = get_table_custom_fields('customers');
$this->ci->db->query("SET sql_mode = ''");

$aColumns = [
    'pos_id',
    'position_name',
    'position_details',
];

$sIndexColumn = 'pos_id';
$sTable       = db_prefix().'position';
$where        = [];
// Add blank where all filter can be stored
$filter = [];

$join = [
    
];

$result = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where, [
]);

$output  = $result['output'];
$rResult = $result['rResult'];

foreach ($rResult as $aRow) {
    $row = [];

    // Position
    $row[] = $aRow['pos_id'];
    $name = $aRow['position_name'];
    $name .= '<div class="row-options">';

    if (is_admin()) {
        $name .= '<a href="' . admin_url('employee/edit_position/' . $aRow['pos_id']) . '">' . _l('edit') . '</a>';
    }
    if (is_admin()) {
        $name .= ' | <a href="' . admin_url('employee/delete_position/' . $aRow['pos_id']) . '" class="text-danger _delete">' . _l('delete') . '</a>';
    }
    $name .= '</div>';
    $row[] = $name;

    $row[] = $aRow['position_details'];
    

    $row['DT_RowClass'] = 'has-row-options';

    $row = hooks()->apply_filters('position_table_row_data', $row, $aRow);

    $output['aaData'][] = $row;
}
