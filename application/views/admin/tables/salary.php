<?php

    defined('BASEPATH') or exit('No direct script access allowed');

    $aColumns = [
        db_prefix() . 'employee.emp_id_number as id',
        '(SELECT CONCAT(first_name, " ", last_name) FROM '. db_prefix() .'employee WHERE emp_id='. db_prefix() .'employee_salary_payment.employee_id) as emp',
        '(SELECT GROUP_CONCAT(name SEPARATOR ", ") FROM ' . db_prefix() . 'employeesalarypayment_project JOIN ' . db_prefix() . 'projects ON ' . db_prefix() . 'projects.id = ' . db_prefix() . 'employeesalarypayment_project.project_id WHERE emp_sal_pay_id = ' . db_prefix() . 'employee_salary_payment.emp_sal_pay_id ORDER BY project_id ASC) as projects',
        db_prefix() . 'salary_sheet_generate.name as salary_name',
        'total_working_hours',
        'working_period',
        'overtime',
        'salary_setup_benefits',
        'salary_setup_bonus',
        db_prefix() . 'employee.rate as emp_rate',
        'total_salary',
        ];

    $sIndexColumn = 'emp_sal_pay_id';
    $sTable       = db_prefix() . 'employee_salary_payment';

    $join = [
        'LEFT JOIN ' . db_prefix() . 'employee ON ' . db_prefix() . 'employee.emp_id = ' . db_prefix() . 'employee_salary_payment.employee_id',
        'LEFT JOIN ' . db_prefix() . 'currencies ON ' . db_prefix() . 'currencies.id = ' . db_prefix() . 'employee_salary_payment.currency',
        'LEFT JOIN ' . db_prefix() . 'salary_sheet_generate ON ' . db_prefix() . 'salary_sheet_generate.ssg_id = ' . db_prefix() . 'employee_salary_payment.salary_name_id',
    ];

    $where  = [];

    $result = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where, [
        'emp_sal_pay_id',
        db_prefix() . 'employee.last_name as last_name',
        'emp_id',
        db_prefix(). 'currencies.name as currency_name',
    ]);

    $output  = $result['output'];
    $rResult = $result['rResult'];

    foreach ($rResult as $aRow) {
        $row = [];

        $data = '<a href="' . admin_url('payroll/paymentview_edit/' . $aRow['emp_sal_pay_id']) . '">' . $aRow['id'] . '</a>';
        $data .= '<div class="row-options">';
        $data .= '<a href="' . admin_url('payroll/delete_salary/' . $aRow['emp_sal_pay_id']) . '" class="text-danger _delete">' . _l('delete') . '</a>';
        $data .= '</div>';
        $row[] = $data;

        $row[] = $aRow['emp'];

        $row[] = $aRow['projects'];

        $row[] = $aRow['salary_name'];

        $row[] = $aRow['total_working_hours'];

        $row[] = $aRow['working_period'];

        $row[] = $aRow['overtime'] == 0 ? '' : $aRow['overtime'];

        $row[] = $aRow['salary_setup_benefits'] == 0 ? '' : number_format($aRow['salary_setup_benefits'], 2);

        $row[] = $aRow['salary_setup_bonus'] == 0 ? '' : number_format($aRow['salary_setup_bonus'], 2);

        $row[] = number_format($aRow['emp_rate'], 2);

        $row[] = app_format_money($aRow['total_salary'], $aRow['currency_name']);

        $row['DT_RowClass'] = 'has-row-options';

        $row = hooks()->apply_filters('salary_table_row_data', $row, $aRow);

        $output['aaData'][] = $row;
    }
