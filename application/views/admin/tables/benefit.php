<?php

defined('BASEPATH') or exit('No direct script access allowed');

// $hasPermissionDelete = has_permission('customers', '', 'delete');

// $custom_fields = get_table_custom_fields('customers');
// $this->ci->db->query("SET sql_mode = ''");

$aColumns = [
    'benefit_id',
    'benefit_name',
    'benefit_description',
    'benefit_type',
    'default_amount',
    'status',
];

$sIndexColumn = 'benefit_id';
$sTable       = db_prefix().'benefit';
$join = [
    'LEFT JOIN ' . db_prefix() . 'currencies ON ' . db_prefix() . 'currencies.id = ' . db_prefix() . 'benefit.currency',
];
$where = hooks()->apply_filters('benefit_table_sql_where', []);

$i            = 0;

$result = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where, [
    db_prefix(). 'currencies.name as currency_name',
]);

$output  = $result['output'];
$rResult = $result['rResult'];

foreach ($rResult as $aRow) {
    $row = [];
    for ($i = 0; $i < count($aColumns); $i++) {
        if (strpos($aColumns[$i], 'as') !== false && !isset($aRow[$aColumns[$i]])) {
            $_data = $aRow[strafter($aColumns[$i], 'as ')];
        } else {
            $_data = $aRow[$aColumns[$i]];
        }
        if ($aColumns[$i] == 'benefit_name') {
            if ($_data != null) {
                // $_data = $aRow[$aColumns[$i]];
                $_data = '<strong>'.strtoupper($aRow['benefit_name']).'</strong>';
                $_data .= '<div class="row-options">';
                $_data .= '<a href="' . admin_url('payroll/edit_salary_setup/' . $aRow['benefit_id']) . '">' . _l('edit') . '</a>';
                $_data .= ' | <a href="' . admin_url('payroll/delete_salary_setup/' . $aRow['benefit_id']) . '" class="text-danger _delete">' . _l('delete') . '</a>';

                $_data .= '</div>';
            }
        } elseif ($aColumns[$i] == 'benefit_description') {
            if ($_data != null) {
                $_data = $aRow[$aColumns[$i]];
            }
        } elseif ($aColumns[$i] == 'benefit_type') {
            if ($_data != null) {
                if($aRow[$aColumns[$i]] == 1) {
                    $_data = 'Add';
                } elseif($aRow[$aColumns[$i]] == 2) {
                    $_data = 'Deduct';
                } else {
                    $_data = '';
                }
            }
        } elseif ($aColumns[$i] == 'default_amount') {
            if ($_data != 0) {
                $_data = app_format_money($aRow[$aColumns[$i]], $aRow['currency_name']);
            } else {
                $_data = '';
            }
        } elseif ($aColumns[$i] == 'status') {
            if ($_data != null) {
                if($aRow[$aColumns[$i]] == 1) {
                    $_data = '<span class="label label-success">Active</span>';
                } elseif($aRow[$aColumns[$i]] == 2) {
                    $_data = '<span class="label label-default">Inactive</span>';
                } else {
                    $_data = '';
                }
            }
        }

        $row[] = $_data;
    }

    $row['DT_RowClass'] = 'has-row-options';
    $output['aaData'][] = $row;
}