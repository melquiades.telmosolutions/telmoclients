<?php

    defined('BASEPATH') or exit('No direct script access allowed');

    $aColumns = [
        db_prefix() . 'employee.emp_id_number as id',
        '(SELECT CONCAT(first_name, " ", last_name) FROM '. db_prefix() .'employee WHERE emp_id='. db_prefix() .'employee_salary_payment.employee_id) as emp',
        'salary_setup_benefits',
        'salary_setup_bonus',
        db_prefix() . 'salary_sheet_generate.name as salary_name',
        'total_salary',
        ];

    $sIndexColumn = 'emp_sal_pay_id';
    $sTable       = db_prefix() . 'employee_salary_payment';

    $join = [
        'LEFT JOIN ' . db_prefix() . 'employee ON ' . db_prefix() . 'employee.emp_id = ' . db_prefix() . 'employee_salary_payment.employee_id',
        'LEFT JOIN ' . db_prefix() . 'currencies ON ' . db_prefix() . 'currencies.id = ' . db_prefix() . 'employee_salary_payment.currency',
        'LEFT JOIN ' . db_prefix() . 'salary_sheet_generate ON ' . db_prefix() . 'salary_sheet_generate.ssg_id = ' . db_prefix() . 'employee_salary_payment.salary_name_id',
    ];

    $where  = [];

    $result = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where, [
        'emp_sal_pay_id',
        db_prefix() . 'employee.last_name as last_name',
        'emp_id',
        db_prefix(). 'currencies.name as currency_name',
    ]);

    $output  = $result['output'];
    $rResult = $result['rResult'];

    foreach ($rResult as $aRow) {
        $row = [];

        $row[] = '<a href="' . admin_url('payroll/paymentview_edit/' . $aRow['emp_sal_pay_id']) . '">' . $aRow['id'] . '</a>';

        $row[] = $aRow['emp'];

        $row[] = $aRow['salary_setup_benefits'] == 0 ? '' : number_format($aRow['salary_setup_benefits'], 2);
        
        $row[] = $aRow['salary_setup_bonus'] == 0 ? '' : number_format($aRow['salary_setup_bonus'], 2);
        
        $row[] = $aRow['salary_name'];

        $row[] = app_format_money($aRow['total_salary'], $aRow['currency_name']);

        $row['DT_RowClass'] = 'has-row-options';

        $row = hooks()->apply_filters('salary_table_row_data', $row, $aRow);

        $output['aaData'][] = $row;
    }
