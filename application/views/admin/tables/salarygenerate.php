<?php

defined('BASEPATH') or exit('No direct script access allowed');

// $hasPermissionDelete = has_permission('customers', '', 'delete');

// $custom_fields = get_table_custom_fields('customers');
// $this->ci->db->query("SET sql_mode = ''");

$aColumns = [
    'ssg_id',
    'name',
    'start_date',
    'end_date',
    'generated_by',
];

$sIndexColumn = 'ssg_id';
$sTable       = db_prefix().'salary_sheet_generate';
$join = [];
$where = hooks()->apply_filters('salarygenerate_table_sql_where', []);

$i            = 0;

$result = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where, []);

$output  = $result['output'];
$rResult = $result['rResult'];

foreach ($rResult as $aRow) {
    $row = [];
    for ($i = 0; $i < count($aColumns); $i++) {
        if (strpos($aColumns[$i], 'as') !== false && !isset($aRow[$aColumns[$i]])) {
            $_data = $aRow[strafter($aColumns[$i], 'as ')];
        } else {
            $_data = $aRow[$aColumns[$i]];
        }
        if ($aColumns[$i] == 'name') {
            if ($_data != null) {
                $data = '<a href="' . admin_url('payroll/salary_date/' . $aRow['ssg_id']) . '">' . $aRow[$aColumns[$i]] . '</a>';
                $data .= '<div class="row-options">';
                $data .= '<a href="' . admin_url('payroll/delete_salaryname/' . $aRow['ssg_id']) . '" class="text-danger _delete">' . _l('delete') . '</a>';
                $data .= '</div>';
                $_data = $data;
            }
        } elseif ($aColumns[$i] == 'start_date') {
            if ($_data != null) {
                $_data = _d($aRow[$aColumns[$i]]);
            }
        } elseif ($aColumns[$i] == 'end_date') {
            if ($_data != null) {
                $_data = _d($aRow[$aColumns[$i]]);
            }
        } elseif ($aColumns[$i] == 'generated_by') {
            if ($_data != 0) {
                $_data = get_staff_full_name($aRow[$aColumns[$i]]);
            } else {
                $_data = '';
            }
        }

        $row[] = $_data;
    }

    $row['DT_RowClass'] = 'has-row-options';
    $output['aaData'][] = $row;
}