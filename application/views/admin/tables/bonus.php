<?php

defined('BASEPATH') or exit('No direct script access allowed');

// $hasPermissionDelete = has_permission('customers', '', 'delete');

// $custom_fields = get_table_custom_fields('customers');
// $this->ci->db->query("SET sql_mode = ''");

$aColumns = [
    'bonus_id',
    'bonus_name',
    '(SELECT first_name FROM '. db_prefix() .'employee WHERE emp_id=bonus_emp_id) as fname',
    '(SELECT last_name FROM '. db_prefix() .'employee WHERE emp_id=bonus_emp_id) as lname',
    'bonus_amount',
    'bonus_date',
];

$sIndexColumn = 'bonus_id';
$sTable       = db_prefix().'bonus_setup';
$join = [
    'LEFT JOIN ' . db_prefix() . 'employee ON ' . db_prefix() . 'employee.emp_id = ' . db_prefix() . 'bonus_setup.bonus_emp_id',
    'LEFT JOIN ' . db_prefix() . 'currencies ON ' . db_prefix() . 'currencies.id = ' . db_prefix() . 'bonus_setup.currency',
];
$where = [];

$result = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where, [
    db_prefix(). 'employee.emp_id as employee_id',
    db_prefix(). 'currencies.name as currency_name',
]);

$output  = $result['output'];
$rResult = $result['rResult'];

foreach ($rResult as $aRow) {
    $row = [];

    $row[] = $aRow['bonus_id'];

    $bonusname = '<a href="' . admin_url('payroll/bonussetup_view/' . $aRow['bonus_id']) . '">' . $aRow['bonus_name'] . '</a>';
    $bonusname .= '<div class="row-options">';
    $bonusname .= '<a href="' . admin_url('payroll/bonussetup_edit/' . $aRow['bonus_id']) . '">' . _l('edit') . '</a>';
    $bonusname .= ' | <a href="' . admin_url('payroll/delete_bonus/' . $aRow['bonus_id']) . '" class="text-danger _delete">' . _l('delete') . '</a>';
    $bonusname .= '</div>';

    $row[] = $bonusname;
    
    $emp_name  = $aRow['fname'].' '.$aRow['lname'];
    $url = admin_url('employee/view_emp/' . $aRow['employee_id']);

    $emp_name  = '<a href="' . $url . '" target="_blank">' . $emp_name  . '</a>';

    $row[] = $emp_name;

    $row[] = app_format_money($aRow['bonus_amount'], $aRow['currency_name']);

    $my_pos = strpos($aRow['bonus_date'], '/');
    $my_sub = substr($aRow['bonus_date'], 0, $my_pos); // month
    switch ($my_sub)
    {
        case "01":
            $my_sub = 'January';
            break;
        case "02":
            $my_sub = 'February';
            break;
        case "03":
            $my_sub = 'March';
            break;
        case "04":
            $my_sub = 'April';
            break;
        case "05":
            $my_sub = 'May';
            break;
        case "06":
            $my_sub = 'June';
            break;
        case "07":
            $my_sub = 'July';
            break;
        case "08":
            $my_sub = 'August';
            break;
        case "09":
            $my_sub = 'September';
            break;
        case "10":
            $my_sub = 'October';
            break;
        case "11":
            $my_sub = 'November';
            break;
        case "12":
            $my_sub = 'December';
            break;
    }
    $my_len = strlen($aRow['bonus_date']);
    $mysub  = substr($aRow['bonus_date'], $my_pos+1, $my_len); // year
    $row[] = $my_sub . ' ' . $mysub;

    $row['DT_RowClass'] = 'has-row-options';
    $row = hooks()->apply_filters('bonussetup_table_row_data', $row, $aRow);
    $output['aaData'][] = $row;
}