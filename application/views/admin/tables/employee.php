<?php

defined('BASEPATH') or exit('No direct script access allowed');

// $has_permission_delete = has_permission('staff', '', 'delete');

// $custom_fields = get_custom_fields('staff', [
//     'show_on_table' => 1,
//     ]);
$aColumns = [
    'emp_id_number',
    'picture',
    'first_name',
    '(SELECT position_name FROM ' . db_prefix() . 'position WHERE pos_id=' . db_prefix() . 'employee.position_id) as positon',
    'email',
    'hire_date',
    'rate_type',
    'rate',
    ];
$sIndexColumn = 'emp_id';
$sTable       = db_prefix().'employee';
$join         = [
    'LEFT JOIN ' . db_prefix() . 'currencies ON ' . db_prefix() . 'currencies.id = ' . db_prefix() . 'employee.currency',
];
$i            = 0;

$where = hooks()->apply_filters('employee_table_sql_where', []);

$result = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where, [
    'picture',
    'last_name',
    'emp_id',
    'emp_id_number',
    db_prefix(). 'currencies.name as currency_name',
    ]);

$output  = $result['output'];
$rResult = $result['rResult'];

foreach ($rResult as $aRow) {
    $row = [];
    for ($i = 0; $i < count($aColumns); $i++) {
        if (strpos($aColumns[$i], 'as') !== false && !isset($aRow[$aColumns[$i]])) {
            $_data = $aRow[strafter($aColumns[$i], 'as ')];
        } else {
            $_data = $aRow[$aColumns[$i]];
        } 
        if ($aColumns[$i] == 'position') {
            if ($_data != null) {
                $_data = _d($aRow[$aColumns[$i]]);
            }
        } elseif ($aColumns[$i] == 'email') {
            if ($_data != null) {
                $_data = '<a href="mailto:' . $_data . '">' . $_data . '</a>';
            }
        } elseif ($aColumns[$i] == 'hire_date') {
            if ($_data != null) {
                $_data = _d($aRow[$aColumns[$i]]);
            }
        } elseif ($aColumns[$i] == 'rate') {
            if ($_data != 0) {
                $_data = app_format_money($aRow[$aColumns[$i]], $aRow['currency_name']);
            } else {
                $_data = '';
            }
        }
        elseif ($aColumns[$i] == 'picture') {
            if($aRow['picture'] != '') {
                $_data = '<a href="' . admin_url('employee/profile/' . $aRow['emp_id']) . '"> <img src="' . base_url($aRow['picture']) . '" width="45" height="44" style="border-radius:50%;border-top-left-radius: 50% 50%;border-top-right-radius: 50% 50%;border-bottom-right-radius: 50% 50%;border-bottom-left-radius: 50% 50%;"> </a>';
            } else {
                $_data = '<a href="' . admin_url('employee/profile/' . $aRow['emp_id']) . '"> <img src="' . base_url('assets/images/user-placeholder.jpg') . '" width="45" height="44" style="border-radius:50%;border-top-left-radius: 50% 50%;border-top-right-radius: 50% 50%;border-bottom-right-radius: 50% 50%;border-bottom-left-radius: 50% 50%;"> </a>';
            }
        }
        elseif ($aColumns[$i] == 'first_name') {
            $_data = ' <a href="' . admin_url('employee/view_emp/' . $aRow['emp_id']) . '">' . $aRow['first_name'] . ' ' . $aRow['last_name'] . '</a>';
            $_data .= '<div class="row-options">';
            $_data .= '<a href="' . admin_url('employee/view_emp/' . $aRow['emp_id']) . '">' . _l('view') . '</a>';
            $_data .= ' | <a href="' . admin_url('employee/edit_emp/' . $aRow['emp_id']) . '">' . _l('edit') . '</a>';
                $_data .= ' | <a href="' . admin_url('employee/delete_emp/' . $aRow['emp_id']) . '" class="text-danger _delete">' . _l('delete') . '</a>';

            $_data .= '</div>';
        }
        elseif ($aColumns[$i] == 'rate_type') {
            if($aRow['rate_type'] == 1) {
                $_data = 'Hourly';
            } elseif($aRow['rate_type'] == 2) {
                $_data = 'Daily';
            } else {
                $_data = '';
            }
        }
        $row[] = $_data;
    }

    $row['DT_RowClass'] = 'has-row-options';
    $output['aaData'][] = $row;
}
