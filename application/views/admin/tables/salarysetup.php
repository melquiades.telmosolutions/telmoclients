<?php

defined('BASEPATH') or exit('No direct script access allowed');

// $hasPermissionDelete = has_permission('customers', '', 'delete');

// $custom_fields = get_table_custom_fields('customers');
// $this->ci->db->query("SET sql_mode = ''");

$aColumns = [
    db_prefix() . 'employee.emp_id_number',
    '(SELECT CONCAT(first_name, " ", last_name) FROM '. db_prefix() .'employee WHERE emp_id='. db_prefix() .'employee_salary_setup.employee_id) as employee',
    // 'month_year',
    'sal_type',
    '(SELECT rate FROM '. db_prefix() .'employee WHERE emp_id='. db_prefix() .'employee_salary_setup.employee_id) as emp_rate',
    'total',
];

$sIndexColumn = 'e_s_s_id';
$sTable       = db_prefix().'employee_salary_setup';
$join = [
    'LEFT JOIN ' . db_prefix() . 'currencies ON ' . db_prefix() . 'currencies.id = ' . db_prefix() . 'employee_salary_setup.currency',
    'LEFT JOIN ' . db_prefix() . 'employee ON ' . db_prefix() . 'employee.emp_id = ' . db_prefix() . 'employee_salary_setup.employee_id',
];
$where = hooks()->apply_filters('salarysetup_table_sql_where', []);

$i            = 0;

$result = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where, [
    'emp_id',
    'emp_id_number',
    'employee_id',
    db_prefix(). 'currencies.name as currency_name',
], 'GROUP by employee_id');

$output  = $result['output'];
$rResult = $result['rResult'];

foreach ($rResult as $aRow) {
    $row = [];
    for ($i = 0; $i < count($aColumns); $i++) {
        if (strpos($aColumns[$i], 'as') !== false && !isset($aRow[$aColumns[$i]])) {
            $_data = $aRow[strafter($aColumns[$i], 'as ')];
        } else {
            $_data = $aRow[$aColumns[$i]];
            $_data .= '<div class="row-options">';
            $_data .= '<a href="' . admin_url('payroll/view_ss/' . $aRow['employee_id']) . '">' . _l('view') . '</a>';
            $_data .= ' | <a href="' . admin_url('payroll/edit_ss/' . $aRow['employee_id']) . '">' . _l('edit') . '</a>';
            $_data .= ' | <a href="' . admin_url('payroll/delete_ss/' . $aRow['employee_id']) . '" class="text-danger _delete">' . _l('delete') . '</a>';
            $_data .= '</div>';
        }
        if ($aColumns[$i] == 'employee') {
            if ($_data != null) {
                $_data = $aRow[$aColumns[$i]];
            }
        } 
        // elseif ($aColumns[$i] == 'month_year') {
        //     if ($_data != null) {
        //         $_data = $aRow[$aColumns[$i]];
        //     } else {
        //         $_data = '';
        //     }
        // } 
        elseif ($aColumns[$i] == 'sal_type') {
            if ($_data != null) {
                if($aRow[$aColumns[$i]] == 1) {
                    $_data = 'Hourly';
                } elseif($aRow[$aColumns[$i]] == 2) {
                    $_data = 'Daily';
                } else {
                    $_data = '';
                }
            }
        } elseif ($aColumns[$i] == 'emp_rate') {
            if ($_data != 0) {
                $_data = number_format($aRow[$aColumns[$i]], 2);
            } else {
                $_data = '';
            }
        } elseif ($aColumns[$i] == 'total') {
            if ($_data != null) {
                $_data = app_format_money($aRow[$aColumns[$i]], $aRow['currency_name']);
            }
        }

        $row[] = $_data;
    }

    $row['DT_RowClass'] = 'has-row-options';
    $output['aaData'][] = $row;
}