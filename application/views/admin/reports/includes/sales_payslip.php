<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<div id="payslip" class="hide">
   <table class="table table-payslip scroll-responsive">
      <thead>
         <tr>
            <th><?php echo '#'; ?></th>
            <th><?php echo _l('employee'); ?></th>
            <th><?php echo _l('date'); ?></th>
            <th><?php echo _l('rate'); ?></th>
            <th><?php echo _l('payment_mode'); ?></th>
            <th><?php echo _l('tot_salary'); ?></th>
         </tr>
      </thead>
      <tbody></tbody>
      <tfoot>
         <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td class="total_salary"></td>
         </tr>
      </tfoot>
   </table>
</div>