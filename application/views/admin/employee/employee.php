<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<style>
  td {
    font-weight: bold;
  }
</style>
<div id="wrapper">
   <div class="content">
      <div class="row">
         <div class="col-md-12">
            <div class="panel_s mbot10">
              <div class="row">
                <div class="col-sm-5">
                  <div class="panel-body">
                    <center>
                      <button class="btn btn-default"><a href="<?php echo admin_url('employee/export_emp/') . $employees->emp_id;?>" target="_blank"><i class="fa fa-file-pdf-o"></i>&nbsp;&nbsp;Export</a></button> <br><hr>
                      <?php if(!empty($employees->picture)) { ?>
                        <img src="<?php echo base_url($employees->picture); ?>" width=150px; height=150px; class=img-circle>
                      <?php } else { ?>
                        <img src="<?php echo base_url(); ?>assets/images/user-placeholder.jpg" width=150px; height=150px; class=img-circle>
                      <?php } ?>
                      <hr>
                      <h3><?php echo $employees->first_name, ' ', $employees->last_name; ?></h3>
                      <h5><?php $dept = $this->db->select('department_name')->from(db_prefix() . 'department')->where('dept_id', $employees->dept_id)->get()->row(); echo $dept->department_name; ?></h5>
                      <?php if(!empty($employees->email)) { ?><i class="fa fa-envelope" aria-hidden="true" style="display: inline;"></i>&nbsp;<h5 style="display: inline;"><?php echo $employees->email ?></h5><?php }  if(!empty($employees->phone)) { ?>&emsp;<i class="fa fa-mobile" aria-hidden="true" style="display: inline; font-size:20px"></i>&nbsp;<h5 style="display: inline;"><?php echo $employees->phone; } ?></h5>
                    </center>
                  </div>
                  <div class="panel-body">
                    <table class="table table-hover" width="100%">
                      <caption style="text-align: center; font-size: 25px"><?php echo _l('personal_info');?></caption>
                      <tr>
                        <th><?php echo _l('employee_id_num')?></th>
                        <td><?php echo $employees->emp_id_number;?></td>
                      </tr>
                      <tr>
                        <th><?php echo _l('emp_id')?></th>
                        <td><?php echo $employees->first_name." ".$employees->middle_name." ".$employees->last_name;?></td>
                      </tr>
                      <tr>
                        <th><?php echo _l('nav_mobile')?></th>
                        <td><?php echo $employees->phone ;?></td>
                      </tr>
                      <tr>
                        <th><?php echo _l('nav_email')?></th>
                        <td><?php echo $employees->email  ;?></td>
                      </tr>
                      <tr>
                        <td colspan="2">ADDRESS</td>
                      </tr>
                      <tr>
                        <th><?php echo _l('nav_houseno')?></th>
                        <td><?php echo $employees->house_no  ;?></td>
                      </tr>
                      <tr>
                        <th><?php echo _l('nav_streetname')?></th>
                        <td><?php echo $employees->street_name  ;?></td>
                      </tr>
                      <tr>
                        <th><?php echo _l('nav_subdivision')?></th>
                        <td><?php echo $employees->subdivision  ;?></td>
                      </tr>
                      <tr>
                        <th><?php echo _l('nav_barangay')?></th>
                        <td><?php echo $employees->barangay  ;?></td>
                      </tr>
                      <tr>
                        <th><?php echo _l('nav_city')?></th>
                        <td><?php echo $employees->city ;?></td>
                      </tr>
                      <tr>
                        <th><?php echo _l('nav_province')?></th>
                        <td><?php echo $employees->province  ;?></td>
                      </tr>
                      <tr>
                        <th><?php echo _l('Country')?></th>
                        <td><?php $c = $this->db->select('short_name')->from(db_prefix() . 'countries')->where('country_id', $employees->country)->get()->row(); echo $c->short_name ;?></td>
                      </tr>
                      <tr>
                        <th><?php echo _l('nav_zip_code')?></th>
                        <td><?php if($employees->zip == 0) { echo '' ; } else { echo $employees->zip; }?></td>
                      </tr>
                    </table>
                    <hr>
                    <table class="table table-hover" width="100%">
                      <caption style="text-align: center; font-size: 25px"><?php echo _l('nav_biographical_info');?></caption>
                      <tr>
                        <th><?php echo _l('nav_dob')?></th>
                        <td><?php if($employees->dob == '0000-00-00') { echo ''; } else { echo $employees->dob; } ?></td>
                      </tr>
                      <tr>
                        <th><?php echo _l('nav_gender')?></th>
                        <td><?php if($employees->gender == 1) echo 'Male'; elseif($employees->gender == 2) echo 'Female'; else echo '';?></td>
                      </tr>
                      <tr>
                        <th><?php echo _l('nav_marital_status')?></th>
                        <td><?php $m = $this->db->select('marital_sta')->from(db_prefix() . 'marital_info')->where('id', $employees->marital_status)->get()->row(); echo $m->marital_sta; if($employees->marital_status == 0) echo ''; ?></td>
                      </tr>
                      <tr>
                        <th><?php echo _l('spouse_name')?></th>
                         <td><?php echo $employees->spouse_fname .' '. $employees->spouse_lname;?></td>
                      </tr>
                      <tr>
                        <th><?php echo _l('nav_nochildren')?></th>
                         <td><?php echo ($employees->no_children == 0 ? '' : $employees->no_children);?></td>
                      </tr>
                       <tr>
                        <th><?php echo _l('nav_bloodtype')?></th>
                         <td><?php echo $employees->blood_type ;?></td>
                      </tr>
                     
                    </table>
                  </div>
                  
                </div>

                <div class="col-sm-7">
                  <div class="panel-body">
                    <table class="table table-hover" width="100%">
                      <caption  style="text-align: center; font-size: 25px"><?php echo _l('nav_positional_info');?></caption>
                      <tr>
                        <th><?php echo _l('nav_department')?></th>
                        <td><?php echo $dept->department_name;?></td>
                      </tr>
                      <tr>
                        <th><?php echo _l('nav_position')?></th>
                        <td><?php $p = $this->db->select('position_name')->from(db_prefix() . 'position')->where('pos_id', $employees->position_id)->get()->row(); echo $p->position_name ;?></td>
                      </tr>
                      <tr>
                        <th><?php echo _l('nav_duty_type')?></th>
                        <td><?php $dt = $this->db->select('type_name')->from(db_prefix() . 'duty_type')->where('id', $employees->duty_type)->get()->row(); echo $dt->type_name; ?></td>
                      </tr>
                       <tr>
                        <th><?php echo _l('nav_hireddate')?></th>
                        <td><?php if($employees->hire_date == '0000-00-00') { echo ''; } else { echo $employees->hire_date; } ?></td>
                      </tr>
                       <tr>
                        <th><?php echo _l('nav_rate_type')?></th>
                        <td><?php if($employees->rate_type == 1) echo 'Hourly'; elseif($employees->rate_type == 2) echo 'Daily'; else echo '';?></td>
                      </tr>
                       <tr>
                        <th><?php echo _l('rate')?></th>
                        <td><?php if($employees->rate == 0) echo ''; else echo app_format_money($employees->rate, $currency->symbol); ?></td>
                      </tr>
                       <tr>
                        <th><?php echo _l('nav_pay_frequency')?></th>
                        <td><?php if($employees->skill_index == 0) echo ''; else echo $employees->skill_index;?></td>
                      </tr>
                      <tr>
                        <th><?php echo _l('supervisor')?></th>
                        <td><?php $s = $this->db->where('emp_id', $employees->super_visor_id)->get(db_prefix() . 'employee')->row(); echo $s->first_name, ' ', $s->last_name;?></td>
                      </tr>
                      <tr>
                        <th><?php echo _l('nav_supervisor_report')?></th>
                        <td><?php echo $employees->supervisor_report;?></td>
                      </tr>
                    </table>
                  </div>
                  <div class="panel-body">
                    <table class="table table-hover" width="100%">
                      <caption style="text-align: center; font-size: 25px"><?php echo _l('nav_benefits');?></caption>
                      <caption style="text-align: left; font-size: 15px"><?php echo _l('sss');?></caption>
                      <tr>
                        <th><?php echo _l('nav_benefit_id')?></th>
                        <td><?php if($emp_benefit->benefit_sss_number == 0) echo ''; else echo $emp_benefit->benefit_sss_number;?></td>
                      </tr>
                      <tr>
                        <th><?php echo _l('nav_benefit_acc_date')?></th>
                        <td><?php if($emp_benefit->benefit_sss_date == '0000-00-00') echo ''; else echo $emp_benefit->benefit_sss_date;?></td>
                      </tr>
                      <tr>
                        <th><?php echo _l('nav_benefit_status')?></th>
                        <td><?php if($emp_benefit->benefit_sss_status == 1) echo 'Active'; elseif($emp_benefit->benefit_sss_status == 2) echo 'Inactive'; else echo ''; ?></td>
                      </tr>
                    </table>
                    <table class="table table-hover" width="100%">
                      <caption style="text-align: left; font-size: 15px"><?php echo _l('pi');?></caption>
                      <tr>
                        <th><?php echo _l('nav_benefit_id')?></th>
                        <td><?php if($emp_benefit->benefit_pi_number == 0) echo ''; else echo $emp_benefit->benefit_pi_number;?></td>
                      </tr>
                      <tr>
                        <th><?php echo _l('nav_benefit_acc_date')?></th>
                        <td><?php if($emp_benefit->benefit_pi_date == '0000-00-00') echo ''; else echo $emp_benefit->benefit_pi_date;?></td>
                      </tr>
                      <tr>
                        <th><?php echo _l('nav_benefit_status')?></th>
                        <td><?php if($emp_benefit->benefit_pi_status == 1) echo 'Active'; elseif($emp_benefit->benefit_pi_status == 2) echo 'Inactive'; else echo ''; ?></td>
                      </tr>
                    </table>
                    <table class="table table-hover" width="100%">
                      <caption style="text-align: left; font-size: 15px"><?php echo _l('ph');?></caption>
                      <tr>
                        <th><?php echo _l('nav_benefit_id')?></th>
                        <td><?php if($emp_benefit->benefit_ph_number == 0) echo ''; else echo $emp_benefit->benefit_ph_number;?></td>
                      </tr>
                      <tr>
                        <th><?php echo _l('nav_benefit_acc_date')?></th>
                        <td><?php if($emp_benefit->benefit_ph_date == '0000-00-00') echo ''; else echo $emp_benefit->benefit_ph_date;?></td>
                      </tr>
                      <tr>
                        <th><?php echo _l('nav_benefit_status')?></th>
                        <td><?php if($emp_benefit->benefit_ph_status == 1) echo 'Active'; elseif($emp_benefit->benefit_ph_status == 2) echo 'Inactive'; else echo ''; ?></td>
                      </tr>
                    </table>
                  </div>
                  <div class="panel-body">
                    <table class="table table-hover" width="100%">
                      <caption style="text-align: center; font-size: 25px"><?php echo _l('nav_emerg_contact');?></caption>
                      <tr>
                        <th><?php echo _l('nav_emerg_contact_person')?></th>
                        <td><?php echo $employees->emrg_contact_person;?></td>
                      </tr>
                      <tr>
                        <th><?php echo _l('nav_emerg_contact')?></th>
                        <td><?php echo $employees->emerg_contct;?></td>
                      </tr>
                      <tr>
                        <th><?php echo _l('nav_emerg_contact_relation')?></th>
                        <td><?php echo $employees->emgr_contct_relation;?></td>
                      </tr>
                    </table>
                  </div>
                </div>
              </div>
            </div>
         </div>
      </div>
   </div>
</div>
<?php init_tail(); ?>
<script>
</script>
</body>
</html>
