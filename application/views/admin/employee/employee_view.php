<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
   <div class="content">
      <div class="row">
         <div class="col-md-12">
            <div class="panel_s mbot10">
               <div class="panel-body _buttons">
                  <a href="<?php echo admin_url('employee/viewEmhistory'); ?>" class="btn btn-info"><?php echo _l('new_employee'); ?></a>
               </div>
            </div>
            <div class="row">
               <div class="col-md-12">
                  <div class="panel_s">
                     <div class="panel-body">
                      <div class="clearfix"></div>
                      <?php
                        $table_data = array(
                          _l('employee_id_num'),
                          _l('image'),
                          _l('clients_list_full_name'),
                          _l('position_emp'),
                          _l('client_email'),
                          _l('hireddate'),
                          _l('nav_rate_type'),
                          _l('rate'),
                        );
                        // $custom_fields = get_custom_fields('staff',array('show_on_table'=>1));
                        // foreach($custom_fields as $field){
                        //   array_push($table_data,$field['name']);
                        // }
                        render_datatable($table_data,'employee');
                      ?>
                    </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<?php init_tail(); ?>
<script>
   $(function(){
    var EmployeeServerParams = {};
    initDataTable('.table-employee', window.location.href, [0], [0], EmployeeServerParams,<?php echo hooks()->apply_filters('employee_table_default_order', json_encode(array(0,'desc'))); ?>);
  });
</script>
</body>
</html>
