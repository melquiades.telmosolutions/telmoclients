<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html>
<head>
  <title><?php echo isset($title) ? $title : get_option('companyname'); ?></title>
  <link href="<?php echo base_url('assets/plugins/bootstrap/css/bootstrap.min.css') ?>" rel="stylesheet">
  <link href="<?php echo base_url('assets/plugins/font-awesome/css/font-awesome.min.css') ?>" rel="stylesheet">
  <script type="text/javascript" src="<?php echo base_url('assets/plugins/bootstrap/js/bootstrap.min.js') ?>"></script>
  <script type="text/javascript" src="<?php echo base_url('assets/plugins/jquery-ui/jquery-ui.min.js') ?>"></script>
</head>
<style>
  td {
    font-weight: bold;
  }
  th {
    font-weight: none;
  }
  .none {
    border:  1px solid white;
  }
  .tdt {
    border-top: 1px solid white;
  }
  .tdb {
    border-bottom: 1px solid white;
  }
  .tdr {
    border-right: 1px solid white;
  }
  .tdl {
    border-left: 1px solid white;
  }
  .tblborder {
    border-top: 1px solid #D3D3D3;
    border-right: 1px solid white;
    border-bottom: 1px solid #D3D3D3;
    border-left: 1px solid white;
  }
</style>
<body>
  <div class="emp_tbl_left">
    <table class="table" table-layout: fixed; style="width: 50%;">
      <tr>
        <th>
          <center>
            <?php if(!empty($employees->picture)) { ?>
              <img src="<?php echo base_url($employees->picture); ?>" width="90" height="100">
            <?php } else { ?>
              <img src="<?php echo base_url(); ?>assets/images/user-placeholder.jpg" width="90" height="100">
            <?php } ?>
            <hr>
          </center>
        </th>
      </tr>
      <tr><th style="text-align: center;"><h3><?php echo $employees->first_name, ' ', $employees->last_name; ?></h3></th></tr>
      <tr><td style="text-align: center; font-weight: none;"><h5><?php $dept = $this->db->select('department_name')->from(db_prefix() . 'department')->where('dept_id', $employees->dept_id)->get()->row(); echo $dept->department_name; ?></h5></td></tr>
      <tr><td style="text-align: center; font-weight: none;"><?php if(!empty($employees->email)) { ?><h5 style="display: inline;"><?php echo $employees->email ?></h5><?php }  if(!empty($employees->phone)) { ?><h5 style="display: inline;"><?php echo $employees->phone; } ?></h5></td></tr>
    </table>
    <table class="table2" table-layout: fixed; border="1" style="width: 40%; margin-right: 3%;">
      <caption style="font-size: 25px;padding-left: 9%;"><?php echo _l('personal_info');?></caption>
      <tr>
        <th class="tblborder" style="padding: 7px; width: 50%;"><?php echo _l('employee_id_num')?></th>
        <td class="tblborder"><?php echo $employees->emp_id_number;?></td>
      </tr>
      <tr>
        <th class="tblborder" style="padding: 7px;"><?php echo _l('emp_id')?></th>
        <td class="tblborder"><?php echo $employees->first_name." ".$employees->middle_name." ".$employees->last_name;?></td>
      </tr>
      <tr>
        <th class="tblborder" style="padding: 7px;"><?php echo _l('nav_mobile')?></th>
        <td class="tblborder"><?php echo $employees->phone ;?></td>
      </tr>
      <tr>
        <th class="tblborder" style="padding: 7px;"><?php echo _l('nav_email')?></th>
        <td class="tblborder"><?php echo $employees->email  ;?></td>
      </tr>
      <tr>
        <td class="tblborder" style="padding: 7px;" colspan="2">ADDRESS</td>
      </tr>
      <tr>
        <th class="tblborder" style="padding: 7px;"><?php echo _l('nav_houseno')?></th>
        <td class="tblborder"><?php echo $employees->house_no  ;?></td>
      </tr>
      <tr>
        <th class="tblborder" style="padding: 7px;"><?php echo _l('nav_streetname')?></th>
        <td class="tblborder"><?php echo $employees->street_name  ;?></td>
      </tr>
      <tr>
        <th class="tblborder" style="padding: 7px;"><?php echo _l('nav_subdivision')?></th>
        <td class="tblborder"><?php echo $employees->subdivision  ;?></td>
      </tr>
      <tr>
        <th class="tblborder" style="padding: 7px;"><?php echo _l('nav_barangay')?></th>
        <td class="tblborder"><?php echo $employees->barangay  ;?></td>
      </tr>
      <tr>
        <th class="tblborder" style="padding: 7px;"><?php echo _l('nav_city')?></th>
        <td class="tblborder"><?php echo $employees->city ;?></td>
      </tr>
      <tr>
        <th class="tblborder" style="padding: 7px;"><?php echo _l('nav_province')?></th>
        <td class="tblborder"><?php echo $employees->province  ;?></td>
      </tr>
      <tr>
        <th class="tdl tdr tblborder" style="padding: 7px;"><?php echo _l('Country')?></th>
        <td class="tblborder"><?php $c = $this->db->select('short_name')->from(db_prefix() . 'countries')->where('country_id', $employees->country)->get()->row(); echo $c->short_name ;?></td>
      </tr>
      <tr>
        <th class="tdl tdr tdb tblborder" style="padding: 7px;"><?php echo _l('nav_zip_code')?></th>
        <td class="none"><?php if($employees->zip == 0) { echo '' ; } else { echo $employees->zip; }?></td>
      </tr>
    </table>
    <hr style="width: 50%; text-align: left;">
    <table class="table3" table-layout: fixed; border="1" style="width: 50%;">
      <caption style="font-size: 25px;padding-left: 5%;"><?php echo _l('nav_biographical_info');?></caption>
      <tr>
        <th class="tblborder" style="padding: 7px; width: 50%;"><?php echo _l('nav_dob')?></th>
        <td class="tblborder"><?php if($employees->dob == '0000-00-00') { echo ''; } else { echo $employees->dob; } ?></td>
      </tr>
      <tr>
        <th class="tblborder" style="padding: 7px;"><?php echo _l('nav_gender')?></th>
        <td class="tblborder"><?php if($employees->gender == 1) echo 'Male'; elseif($employees->gender == 2) echo 'Female'; else echo '';?></td>
      </tr>
      <tr>
        <th class="tblborder" style="padding: 7px;"><?php echo _l('nav_marital_status')?></th>
        <td class="tblborder"><?php $m = $this->db->select('marital_sta')->from(db_prefix() . 'marital_info')->where('id', $employees->marital_status)->get()->row(); echo $m->marital_sta; if($employees->marital_status == 0) echo ''; ?></td>
      </tr>
      <tr>
        <th class="tblborder" style="padding: 7px;"><?php echo _l('spouse_name')?></th>
        <td class="tblborder"><?php echo $employees->spouse_fname .' '. $employees->spouse_lname;?></td>
      </tr>
      <tr>
        <th class="tblborder" style="padding: 7px;"><?php echo _l('nav_nochildren')?></th>
        <td class="tblborder"><?php echo ($employees->no_children == 0 ? '' : $employees->no_children);?></td>
      </tr>
      <tr>
        <th class="tblborder" style="padding: 7px;"><?php echo _l('nav_bloodtype')?></th>
        <td class="tblborder"><?php echo $employees->blood_type ;?></td>
      </tr>
    </table>
  </div>
  <div class="emp_tbl_right" style="margin: -140% 0 0 55%;">
    <table class="table4" table-layout: fixed; border="1" style="width: 100%;">
      <caption style="text-align: center;font-size: 25px;padding-left: 4%;"><?php echo _l('nav_positional_info');?></caption>
      <tr>
        <th class="tblborder" style="padding: 7px; width: 50%;"><?php echo _l('nav_department')?></th>
        <td class="tblborder"><?php echo $dept->department_name;?></td>
      </tr>
      <tr>
        <th class="tblborder" style="padding: 7px;"><?php echo _l('nav_position')?></th>
        <td class="tblborder"><?php $p = $this->db->select('position_name')->from(db_prefix() . 'position')->where('pos_id', $employees->position_id)->get()->row(); echo $p->position_name ;?></td>
      </tr>
      <tr>
        <th class="tblborder" style="padding: 7px;"><?php echo _l('nav_duty_type')?></th>
        <td class="tblborder"><?php $dt = $this->db->select('type_name')->from(db_prefix() . 'duty_type')->where('id', $employees->duty_type)->get()->row(); echo $dt->type_name; ?></td>
      </tr>
       <tr>
        <th class="tblborder" style="padding: 7px;"><?php echo _l('nav_hireddate')?></th>
        <td class="tblborder"><?php if($employees->hire_date == '0000-00-00') { echo ''; } else { echo $employees->hire_date; } ?></td>
      </tr>
       <tr>
        <th class="tblborder" style="padding: 7px;"><?php echo _l('nav_rate_type')?></th>
        <td class="tblborder"><?php if($employees->rate_type == 1) echo 'Hourly'; elseif($employees->rate_type == 2) echo 'Daily'; else echo '';?></td>
      </tr>
       <tr>
        <th class="tblborder" style="padding: 7px;"><?php echo _l('rate')?></th>
        <td class="tblborder"><?php if($employees->rate == 0) echo ''; else echo app_format_money($employees->rate, $currency->symbol); ?></td>
      </tr>
       <tr>
        <th class="tblborder" style="padding: 7px;"><?php echo _l('nav_pay_frequency')?></th>
        <td class="tblborder"><?php if($employees->skill_index == 0) echo ''; else echo $employees->skill_index;?></td>
      </tr>
      <tr>
        <th class="tblborder" style="padding: 7px;"><?php echo _l('supervisor')?></th>
        <td class="tblborder"><?php $s = $this->db->where('emp_id', $employees->super_visor_id)->get(db_prefix() . 'employee')->row(); echo $s->first_name, ' ', $s->last_name;?></td>
      </tr>
      <tr>
        <th class="tblborder" style="padding: 7px;"><?php echo _l('nav_supervisor_report')?></th>
        <td class="tblborder"><?php echo $employees->supervisor_report;?></td>
      </tr>
    </table>
    <table class="table5" table-layout: fixed; border="1" style="width: 100%;">
      <caption style="text-align: center;font-size: 25px;padding-left: 4%;"><?php echo _l('nav_benefits');?></caption>
      <tr>
        <td class="tdt tdl tdr" style="border-bottom: 1px solid #D3D3D3;padding: 7px; "><caption style="text-align: left; font-size: 15px"><?php echo _l('sss');?></caption></td>
      </tr>
      <tr>
        <th class="tblborder" style="padding: 7px; width: 50%;"><?php echo _l('nav_benefit_id')?></th>
        <td class="tblborder"><?php if($emp_benefit->benefit_sss_number == 0) echo ''; else echo $emp_benefit->benefit_sss_number;?></td>
      </tr>
      <tr>
        <th class="tblborder" style="padding: 7px;"><?php echo _l('nav_benefit_acc_date')?></th>
        <td class="tblborder"><?php if($emp_benefit->benefit_sss_date == '0000-00-00') echo ''; else echo $emp_benefit->benefit_sss_date;?></td>
      </tr>
      <tr>
        <th class="tblborder" style="padding: 7px;"><?php echo _l('nav_benefit_status')?></th>
        <td class="tblborder"><?php if($emp_benefit->benefit_sss_status == 1) echo 'Active'; elseif($emp_benefit->benefit_sss_status == 2) echo 'Inactive'; else echo ''; ?></td>
      </tr>
    </table>
    <br>
    <table class="table6" table-layout: fixed; border="1" style="width: 100%;">
      <tr>
        <td class="tdt tdl tdr" style="border-bottom: 1px solid #D3D3D3;padding: 7px; "><caption style="text-align: left; font-size: 15px"><?php echo _l('pi');?></caption></td>
      </tr>
      <tr>
        <th class="tblborder" style="padding: 7px; width: 50%;"><?php echo _l('nav_benefit_id')?></th>
        <td class="tblborder"><?php if($emp_benefit->benefit_pi_number == 0) echo ''; else echo $emp_benefit->benefit_pi_number;?></td>
      </tr>
      <tr>
        <th class="tblborder" style="padding: 7px;"><?php echo _l('nav_benefit_acc_date')?></th>
        <td class="tblborder"><?php if($emp_benefit->benefit_pi_date == '0000-00-00') echo ''; else echo $emp_benefit->benefit_pi_date;?></td>
      </tr>
      <tr>
        <th class="tblborder" style="padding: 7px;"><?php echo _l('nav_benefit_status')?></th>
        <td class="tblborder"><?php if($emp_benefit->benefit_pi_status == 1) echo 'Active'; elseif($emp_benefit->benefit_pi_status == 2) echo 'Inactive'; else echo ''; ?></td>
      </tr>
    </table>
    <br>
    <table class="table7" table-layout: fixed; border="1" style="width: 100%;">
      <tr>
        <td class="tdt tdl tdr" style="border-bottom: 1px solid #D3D3D3;padding: 7px; "><caption style="text-align: left; font-size: 15px"><?php echo _l('ph');?></caption></td>
      </tr>
      <tr>
        <th class="tblborder" style="padding: 7px; width: 50%;"><?php echo _l('nav_benefit_id')?></th>
        <td class="tblborder"><?php if($emp_benefit->benefit_ph_number == 0) echo ''; else echo $emp_benefit->benefit_ph_number;?></td>
      </tr>
      <tr>
        <th class="tblborder" style="padding: 7px;"><?php echo _l('nav_benefit_acc_date')?></th>
        <td class="tblborder"><?php if($emp_benefit->benefit_ph_date == '0000-00-00') echo ''; else echo $emp_benefit->benefit_ph_date;?></td>
      </tr>
      <tr>
        <th class="tblborder" style="padding: 7px;"><?php echo _l('nav_benefit_status')?></th>
        <td class="tblborder"><?php if($emp_benefit->benefit_ph_status == 1) echo 'Active'; elseif($emp_benefit->benefit_ph_status == 2) echo 'Inactive'; else echo ''; ?></td>
      </tr>
    </table>

    <table class="table8" table-layout: fixed; border="1" style="width: 100%;">
      <caption style="text-align: center;font-size: 25px;padding-left: 4%;"><?php echo _l('nav_emerg_contact');?></caption>
      <tr>
        <th class="tblborder" style="padding: 7px; width: 50%;"><?php echo _l('nav_emerg_contact_person')?></th>
        <td class="tblborder"><?php echo $employees->emrg_contact_person;?></td>
      </tr>
      <tr>
        <th class="tblborder" style="padding: 7px;"><?php echo _l('nav_emerg_contact')?></th>
        <td class="tblborder"><?php echo $employees->emerg_contct;?></td>
      </tr>
      <tr>
        <th class="tblborder" style="padding: 7px;"><?php echo _l('nav_emerg_contact_relation')?></th>
        <td class="tblborder"><?php echo $employees->emgr_contct_relation;?></td>
      </tr>
    </table>
  </div>
</body>
</html>