<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html>
<head>
  <title>Monthly Log Details</title>
 <link href="<?php echo base_url('assets/plugins/bootstrap/css/bootstrap.min.css') ?>" rel="stylesheet">
  <link href="<?php echo base_url('assets/plugins/font-awesome/css/font-awesome.min.css') ?>" rel="stylesheet">
  <script type="text/javascript" src="<?php echo base_url('assets/plugins/bootstrap/js/bootstrap.min.js') ?>"></script>
  <script type="text/javascript" src="<?php echo base_url('assets/plugins/jquery-ui/jquery-ui.min.js') ?>"></script>
</head>
<body>
    <div class="company-logo">
        <?php echo pdf_logo_url(); ?>
    </div>
    <?php echo get_option('invoice_company_address'); ?>
  <br><br><br>
  <table class="table" id="table2" style="margin-bottom: 0;margin-top: -20px;">
    <tr>
      <th>
        <h6><strong>Project/s:</strong></h6>
      </th>
      <th>
        <h6><strong>Attendance Sheet Number:</strong></h6>
      </th>
      <th>
        <h6><strong>Work Setup:</strong></h6>
      </th>
    </tr>
    <tr>
      <td>
        <?php
          $getproj = $this->db->query("SELECT * FROM ".db_prefix()."attendance_project WHERE uid='".$show_pdf->uid."' AND (att_time_min BETWEEN '".$show_pdf->min_time."' AND '".$show_pdf->max_time."' AND att_time_max BETWEEN '".$show_pdf->min_time."' AND '".$show_pdf->max_time."') GROUP BY project_id ")->result_array();
          foreach($getproj as $get) {
            $getproj_ = $this->db->query("SELECT * FROM tblprojects WHERE id='".$get['project_id']."' ")->row();
            echo "&emsp;&emsp;&emsp;<b style='padding-bottom: 3px;'>" .  $getproj_->name . "<br></b>";
          }
        ?>
      </td>
      <td>
        <?php
          $getref = $this->db->query("SELECT * FROM ".db_prefix()."attendance_history WHERE MONTH(att_time) = '".$show_pdf->month_log."' AND uid='".$show_pdf->uid."' GROUP BY number ")->result_array();
          foreach ($getref as $getr) {
            echo "&emsp;&emsp;&emsp;<b style='padding-bottom: 3px;'>" . sales_number_format($getr['number'], 1, $getr['prefix'], date('Y-m-d')) . " <br></b>";
          }
        ?>
      </td>
      <td>
        <?php
          $getloc = $this->db->query("SELECT * FROM ".db_prefix()."attendance_history WHERE MONTH(att_time) = '".$show_pdf->month_log."' AND uid='".$show_pdf->uid."' GROUP BY location_id ")->result_array();
          foreach($getloc as $getl) {
            $getl['location_id'] != '' ? $getl['location_id'] == 1 ? $loc = 'Office Work' : $loc = 'Work At Home' : $loc = '';
            echo "&emsp;&emsp;&emsp;<b style='padding-bottom: 3px;'>".  $loc . " <br></b>";
          }
        ?>
      </td>
    </tr>
  </table>
<br>
<table class="table" id="table" border="1" style="font-size: 11px">
     <tdead>
        <tr>
          <td style="padding: 7px;text-align: center;" colspan="23">MONTHLY LOGSHEET</td>
          <td style="padding: 7px;text-align: center;" colspan="8">
            Date From:&nbsp;&nbsp;&nbsp;<b style='border-bottom: solid 1px #000000;display: inline;padding-bottom: 3px;'><?php echo date("M d, Y", strtotime($show_pdf->min_time))?></b>&nbsp;&nbsp;  to &nbsp;&nbsp; <b style='border-bottom: solid 1px #000000;display: inline;padding-bottom: 3px;'><?php echo date("M d, Y", strtotime($show_pdf->max_time))?></b>
          </td>
        </tr>
    </thead>
    <tbody>
      <tr>
        <td style="padding: 5px;text-align: center;">Name of Employee / Employee ID</td>
        <td style="padding: 5px;text-align: center;" colspan="30">DATES</td>
      </tr>
      <?php
        $log_month = date("m", strtotime($show_pdf->min_time));
        $log_year = date("Y", strtotime($show_pdf->min_time));
        $month_ld = date("t", strtotime($show_pdf->min_time));
        $id = 1;
        $getemp = $this->db->query("SELECT * FROM ".db_prefix()."attendance_history WHERE MONTH(att_time) = '".$show_pdf->month_log."' AND uid='".$show_pdf->uid."' GROUP BY uid ")->result_array();
        foreach ($getemp as $value) {
          
          $query2 = $this->db->query("SELECT * FROM tblemployee WHERE emp_id='".$value['uid']."' ");
          $showname = $query2->row();

          echo '<tr>' .
          '<td rowspan="15" style="padding: 5px;text-align:left;padding-left:3px;font-weight:bold;">' .$id. '.  ' .$showname->first_name. ' ' .$showname->middle_name. ' ' .$showname->last_name. '</td>'.
          '<td style="padding: 5px;text-align: center;font-weight:bold;" colspan="3">'.$log_month.'/01</td>'.
          '<td style="padding: 5px;text-align: center;font-weight:bold;" colspan="3">'.$log_month.'/02</td>'.
          '<td style="padding: 5px;text-align: center;font-weight:bold;" colspan="3">'.$log_month.'/03</td>'.
          '<td style="padding: 5px;text-align: center;font-weight:bold;" colspan="3">'.$log_month.'/04</td>'.
          '<td style="padding: 5px;text-align: center;font-weight:bold;" colspan="3">'.$log_month.'/05</td>'.
          '<td style="padding: 5px;text-align: center;font-weight:bold;" colspan="3">'.$log_month.'/06</td>'.
          '<td style="padding: 5px;text-align: center;font-weight:bold;" colspan="3">'.$log_month.'/07</td>'.
          '<td style="padding: 5px;text-align: center;font-weight:bold;" colspan="3">'.$log_month.'/08</td>'.
          '<td style="padding: 5px;text-align: center;font-weight:bold;" colspan="3">'.$log_month.'/09</td>'.
          '<td style="padding: 5px;text-align: center;font-weight:bold;" colspan="3">'.$log_month.'/10</td>'.
          '</tr>';
          echo '<tr>' .
          '<td style="padding: 3px;text-align: center;font-weight:bold;" colspan="3">'; $day1 = $log_year.'-'.$log_month.'-01'; $d1 = date("l", strtotime($day1)); echo ($d1 == "Sunday") ? '<span style="color:red;">'.$d1.'</span>' : $d1; echo '</td>'.
          '<td style="padding: 3px;text-align: center;font-weight:bold;" colspan="3">'; $day2 = $log_year.'-'.$log_month.'-02'; $d2 = date("l", strtotime($day2)); echo ($d2 == "Sunday") ? '<span style="color:red;">'.$d2.'</span>' : $d2; echo '</td>'.
          '<td style="padding: 3px;text-align: center;font-weight:bold;" colspan="3">'; $day3 = $log_year.'-'.$log_month.'-03'; $d3 = date("l", strtotime($day3)); echo ($d3 == "Sunday") ? '<span style="color:red;">'.$d3.'</span>' : $d3; echo '</td>'.
          '<td style="padding: 3px;text-align: center;font-weight:bold;" colspan="3">'; $day4 = $log_year.'-'.$log_month.'-04'; $d4 = date("l", strtotime($day4)); echo ($d4 == "Sunday") ? '<span style="color:red;">'.$d4.'</span>' : $d4; echo '</td>'.
          '<td style="padding: 3px;text-align: center;font-weight:bold;" colspan="3">'; $day5 = $log_year.'-'.$log_month.'-05'; $d5 = date("l", strtotime($day5)); echo ($d5 == "Sunday") ? '<span style="color:red;">'.$d5.'</span>' : $d5; echo '</td>'.
          '<td style="padding: 3px;text-align: center;font-weight:bold;" colspan="3">'; $day6 = $log_year.'-'.$log_month.'-06'; $d6 = date("l", strtotime($day6)); echo ($d6 == "Sunday") ? '<span style="color:red;">'.$d6.'</span>' : $d6; echo '</td>'.
          '<td style="padding: 3px;text-align: center;font-weight:bold;" colspan="3">'; $day7 = $log_year.'-'.$log_month.'-07'; $d7 = date("l", strtotime($day7)); echo ($d7 == "Sunday") ? '<span style="color:red;">'.$d7.'</span>' : $d7; echo '</td>'.
          '<td style="padding: 3px;text-align: center;font-weight:bold;" colspan="3">'; $day8 = $log_year.'-'.$log_month.'-08'; $d8 = date("l", strtotime($day8)); echo ($d8 == "Sunday") ? '<span style="color:red;">'.$d8.'</span>' : $d8; echo '</td>'.
          '<td style="padding: 3px;text-align: center;font-weight:bold;" colspan="3">'; $day9 = $log_year.'-'.$log_month.'-09'; $d9 = date("l", strtotime($day9)); echo ($d9 == "Sunday") ? '<span style="color:red;">'.$d9.'</span>' : $d9; echo '</td>'.
          '<td style="padding: 3px;text-align: center;font-weight:bold;" colspan="3">'; $day10 = $log_year.'-'.$log_month.'-010'; $d10 = date("l", strtotime($day10)); echo ($d10 == "Sunday") ? '<span style="color:red;">'.$d10.'</span>' : $d10; echo '</td>'.
          '</tr>';
          echo  '<tr>'.
          '<td style="padding: 5px;text-align: center;" >AM</td>'.
          '<td style="padding: 5px;text-align: center;" >PM</td>'.
          '<td style="padding: 5px;text-align: center;" >OT</td>'.
          '<td style="padding: 5px;text-align: center;" >AM</td>'.
          '<td style="padding: 5px;text-align: center;" >PM</td>'.
          '<td style="padding: 5px;text-align: center;" >OT</td>'.
          '<td style="padding: 5px;text-align: center;" >AM</td>'.
          '<td style="padding: 5px;text-align: center;" >PM</td>'.
          '<td style="padding: 5px;text-align: center;" >OT</td>'.
          '<td style="padding: 5px;text-align: center;" >AM</td>'.
          '<td style="padding: 5px;text-align: center;" >PM</td>'.
          '<td style="padding: 5px;text-align: center;" >OT</td>'.
          '<td style="padding: 5px;text-align: center;" >AM</td>'.
          '<td style="padding: 5px;text-align: center;" >PM</td>'.
          '<td style="padding: 5px;text-align: center;" >OT</td>'.
          '<td style="padding: 5px;text-align: center;" >AM</td>'.
          '<td style="padding: 5px;text-align: center;" >PM</td>'.
          '<td style="padding: 5px;text-align: center;" >OT</td>'.
          '<td style="padding: 5px;text-align: center;" >AM</td>'.
          '<td style="padding: 5px;text-align: center;" >PM</td>'.
          '<td style="padding: 5px;text-align: center;" >OT</td>'.
          '<td style="padding: 5px;text-align: center;" >AM</td>'.
          '<td style="padding: 5px;text-align: center;" >PM</td>'.
          '<td style="padding: 5px;text-align: center;" >OT</td>'.
          '<td style="padding: 5px;text-align: center;" >AM</td>'.
          '<td style="padding: 5px;text-align: center;" >PM</td>'.
          '<td style="padding: 5px;text-align: center;" >OT</td>'.
          '<td style="padding: 5px;text-align: center;" >AM</td>'.
          '<td style="padding: 5px;text-align: center;" >PM</td>'.
          '<td style="padding: 5px;text-align: center;" >OT</td>'.
          '</tr>';
          echo  '<tr>'.
          $arrayday1 = ['1','2','3','4','5','6','7','8','9','10'];
          for ($i=0; $i<10; $i++) { 
            $showday = $this->db->query("SELECT * FROM tblattendance_history WHERE MONTH(att_time)='".$show_pdf->month_log."' AND uid='".$showname->emp_id."' AND DAY(att_time)='".$arrayday1[$i]."' GROUP BY att_time ")->row();
            if(count($showday) > 0) {
              $am = '<b style="color:red;font-size: 12px">✘</b>';
              $pm = '<b style="color:red;font-size: 12px">✘</b>';
            } else {
              $am = '<b style="color:white;font-size: 12px;">.</b>';
              $pm = '<b style="color:white;font-size: 12px;">.</b>';
            }
            if ($showday->period == 'Whole Day'){ 
              $am = '<b style="color:green;font-size: 12px">✔</b>';
              $pm = '<b style="color:green;font-size: 12px">✔</b>';  }
            if ($showday->period == 'AM'){ 
              $am = '<b style="color:green;font-size: 12px">✔</b>';  } 
            if ($showday->period == 'PM'){ 
              $pm = '<b style="color:green;font-size: 12px">✔</b>';  }

            if ($showday->period == 'awol'){ 
              $am = '<b style="color:red;font-size: 12px">✘</b>';
              $pm = '<b style="color:red;font-size: 12px">✘</b>'; }
            if ($showday->period == 'onleave'){ 
              $am = '<b style="color:green;font-size: 12px">~</b>';
              $pm = '<b style="color:green;font-size: 12px">~</b>'; }
            if ($showday->period == 'sick'){ 
              $am = '<b style="color:red;font-size: 12px">!</b>';
              $pm = '<b style="color:red;font-size: 12px">!</b>'; }
            if ($showday->period == 'excused'){ 
              $am = '<b style="color:green;font-size: 12px">—</b>';
              $pm = '<b style="color:green;font-size: 12px">—</b>'; }
            if ($showday->period == 'holiday' || !empty($showday->holiday_type)){ 
              $am = '<b style="color:red;font-size: 12px">•</b>';
              $pm = '<b style="color:red;font-size: 12px">•</b>'; }
            
            echo '<td style="padding: 5px;text-align: center;" >'.$am.'</td>'.
            '<td style="padding: 5px;text-align: center;" >'.$pm.'</td>'.
            '<td style="padding: 5px;text-align: center;" >'.$showday->overtime.'</td>';
          }
          echo '</tr>';

          echo '<tr>'.
          '<td style="padding: 5px;text-align: center;font-weight:bold;" colspan="3">'.$log_month.'/11</td>'.
          '<td style="padding: 5px;text-align: center;font-weight:bold;" colspan="3">'.$log_month.'/12</td>'.
          '<td style="padding: 5px;text-align: center;font-weight:bold;" colspan="3">'.$log_month.'/13</td>'.
          '<td style="padding: 5px;text-align: center;font-weight:bold;" colspan="3">'.$log_month.'/14</td>'.
          '<td style="padding: 5px;text-align: center;font-weight:bold;" colspan="3">'.$log_month.'/15</td>'.
          '<td style="padding: 5px;text-align: center;font-weight:bold;" colspan="3">'.$log_month.'/16</td>'.
          '<td style="padding: 5px;text-align: center;font-weight:bold;" colspan="3">'.$log_month.'/17</td>'.
          '<td style="padding: 5px;text-align: center;font-weight:bold;" colspan="3">'.$log_month.'/18</td>'.
          '<td style="padding: 5px;text-align: center;font-weight:bold;" colspan="3">'.$log_month.'/19</td>'.
          '<td style="padding: 5px;text-align: center;font-weight:bold;" colspan="3">'.$log_month.'/20</td>'.
          '</tr>';
          echo '<tr>' .
          '<td style="padding: 3px;text-align: center;font-weight:bold;" colspan="3">'; $day11 = $log_year.'-'.$log_month.'-11'; $d11 = date("l", strtotime($day11)); echo ($d11 == "Sunday") ? '<span style="color:red;">'.$d11.'</span>' : $d11; echo '</td>'.
          '<td style="padding: 3px;text-align: center;font-weight:bold;" colspan="3">'; $day12 = $log_year.'-'.$log_month.'-12'; $d12 = date("l", strtotime($day12)); echo ($d12 == "Sunday") ? '<span style="color:red;">'.$d12.'</span>' : $d12; echo '</td>'.
          '<td style="padding: 3px;text-align: center;font-weight:bold;" colspan="3">'; $day13 = $log_year.'-'.$log_month.'-13'; $d13 = date("l", strtotime($day13)); echo ($d13 == "Sunday") ? '<span style="color:red;">'.$d13.'</span>' : $d13; echo '</td>'.
          '<td style="padding: 3px;text-align: center;font-weight:bold;" colspan="3">'; $day14 = $log_year.'-'.$log_month.'-14'; $d14 = date("l", strtotime($day14)); echo ($d14 == "Sunday") ? '<span style="color:red;">'.$d14.'</span>' : $d14; echo '</td>'.
          '<td style="padding: 3px;text-align: center;font-weight:bold;" colspan="3">'; $day15 = $log_year.'-'.$log_month.'-15'; $d15 = date("l", strtotime($day15)); echo ($d15 == "Sunday") ? '<span style="color:red;">'.$d15.'</span>' : $d15; echo '</td>'.
          '<td style="padding: 3px;text-align: center;font-weight:bold;" colspan="3">'; $day16 = $log_year.'-'.$log_month.'-16'; $d16 = date("l", strtotime($day16)); echo ($d16 == "Sunday") ? '<span style="color:red;">'.$d16.'</span>' : $d16; echo '</td>'.
          '<td style="padding: 3px;text-align: center;font-weight:bold;" colspan="3">'; $day17 = $log_year.'-'.$log_month.'-17'; $d17 = date("l", strtotime($day17)); echo ($d17 == "Sunday") ? '<span style="color:red;">'.$d17.'</span>' : $d17; echo '</td>'.
          '<td style="padding: 3px;text-align: center;font-weight:bold;" colspan="3">'; $day18 = $log_year.'-'.$log_month.'-18'; $d18 = date("l", strtotime($day18)); echo ($d18 == "Sunday") ? '<span style="color:red;">'.$d18.'</span>' : $d18; echo '</td>'.
          '<td style="padding: 3px;text-align: center;font-weight:bold;" colspan="3">'; $day19 = $log_year.'-'.$log_month.'-19'; $d19 = date("l", strtotime($day19)); echo ($d19 == "Sunday") ? '<span style="color:red;">'.$d19.'</span>' : $d19; echo '</td>'.
          '<td style="padding: 3px;text-align: center;font-weight:bold;" colspan="3">'; $day20 = $log_year.'-'.$log_month.'-20'; $d20 = date("l", strtotime($day20)); echo ($d20 == "Sunday") ? '<span style="color:red;">'.$d20.'</span>' : $d20; echo '</td>'.
          '</tr>';
          echo  '<tr>'.
          '<td style="padding: 5px;text-align: center;" >AM</td>'.
          '<td style="padding: 5px;text-align: center;" >PM</td>'.
          '<td style="padding: 5px;text-align: center;" >OT</td>'.
          '<td style="padding: 5px;text-align: center;" >AM</td>'.
          '<td style="padding: 5px;text-align: center;" >PM</td>'.
          '<td style="padding: 5px;text-align: center;" >OT</td>'.
          '<td style="padding: 5px;text-align: center;" >AM</td>'.
          '<td style="padding: 5px;text-align: center;" >PM</td>'.
          '<td style="padding: 5px;text-align: center;" >OT</td>'.
          '<td style="padding: 5px;text-align: center;" >AM</td>'.
          '<td style="padding: 5px;text-align: center;" >PM</td>'.
          '<td style="padding: 5px;text-align: center;" >OT</td>'.
          '<td style="padding: 5px;text-align: center;" >AM</td>'.
          '<td style="padding: 5px;text-align: center;" >PM</td>'.
          '<td style="padding: 5px;text-align: center;" >OT</td>'.
          '<td style="padding: 5px;text-align: center;" >AM</td>'.
          '<td style="padding: 5px;text-align: center;" >PM</td>'.
          '<td style="padding: 5px;text-align: center;" >OT</td>'.
          '<td style="padding: 5px;text-align: center;" >AM</td>'.
          '<td style="padding: 5px;text-align: center;" >PM</td>'.
          '<td style="padding: 5px;text-align: center;" >OT</td>'.
          '<td style="padding: 5px;text-align: center;" >AM</td>'.
          '<td style="padding: 5px;text-align: center;" >PM</td>'.
          '<td style="padding: 5px;text-align: center;" >OT</td>'.
          '<td style="padding: 5px;text-align: center;" >AM</td>'.
          '<td style="padding: 5px;text-align: center;" >PM</td>'.
          '<td style="padding: 5px;text-align: center;" >OT</td>'.
          '<td style="padding: 5px;text-align: center;" >AM</td>'.
          '<td style="padding: 5px;text-align: center;" >PM</td>'.
          '<td style="padding: 5px;text-align: center;" >OT</td>'.
          '</tr>';
          echo  '<tr>'.
          $arrayday2 = ['11','12','13','14','15','16','17','18','19','20'];
          for ($i=0; $i<10; $i++) { 
            $showday = $this->db->query("SELECT * FROM tblattendance_history WHERE MONTH(att_time)='".$show_pdf->month_log."' AND uid='".$showname->emp_id."' AND DAY(att_time)='".$arrayday2[$i]."' GROUP BY att_time ")->row();
            if(count($showday) > 0) {
              $am = '<b style="color:red;font-size: 12px">✘</b>';
              $pm = '<b style="color:red;font-size: 12px">✘</b>';
            } else {
              $am = '<b style="color:white;font-size: 12px;">.</b>';
              $pm = '<b style="color:white;font-size: 12px;">.</b>';
            }
            if ($showday->period == 'Whole Day'){ 
              $am = '<b style="color:green;font-size: 12px">✔</b>';
              $pm = '<b style="color:green;font-size: 12px">✔</b>';  }
            if ($showday->period == 'AM'){ 
              $am = '<b style="color:green;font-size: 12px">✔</b>';  } 
            if ($showday->period == 'PM'){ 
              $pm = '<b style="color:green;font-size: 12px">✔</b>';  }

            if ($showday->period == 'awol'){ 
              $am = '<b style="color:red;font-size: 12px">✘</b>';
              $pm = '<b style="color:red;font-size: 12px">✘</b>'; }
            if ($showday->period == 'onleave'){ 
              $am = '<b style="color:green;font-size: 12px">~</b>';
              $pm = '<b style="color:green;font-size: 12px">~</b>'; }
            if ($showday->period == 'sick'){ 
              $am = '<b style="color:red;font-size: 12px">!</b>';
              $pm = '<b style="color:red;font-size: 12px">!</b>'; }
            if ($showday->period == 'excused'){ 
              $am = '<b style="color:green;font-size: 12px">—</b>';
              $pm = '<b style="color:green;font-size: 12px">—</b>'; }
            if ($showday->period == 'holiday' || !empty($showday->holiday_type)){ 
              $am = '<b style="color:red;font-size: 12px">•</b>';
              $pm = '<b style="color:red;font-size: 12px">•</b>'; }
            
            echo '<td style="padding: 5px;text-align: center;" >'.$am.'</td>'.
            '<td style="padding: 5px;text-align: center;" >'.$pm.'</td>'.
            '<td style="padding: 5px;text-align: center;" >'.$showday->overtime.'</td>';
          }
          echo '</tr>';

          echo '<tr>'.
          '<td style="padding: 5px;text-align: center;font-weight:bold;" colspan="3">'.$log_month.'/21</td>'.
          '<td style="padding: 5px;text-align: center;font-weight:bold;" colspan="3">'.$log_month.'/22</td>'.
          '<td style="padding: 5px;text-align: center;font-weight:bold;" colspan="3">'.$log_month.'/23</td>'.
          '<td style="padding: 5px;text-align: center;font-weight:bold;" colspan="3">'.$log_month.'/24</td>'.
          '<td style="padding: 5px;text-align: center;font-weight:bold;" colspan="3">'.$log_month.'/25</td>'.
          '<td style="padding: 5px;text-align: center;font-weight:bold;" colspan="3">'.$log_month.'/26</td>'.
          '<td style="padding: 5px;text-align: center;font-weight:bold;" colspan="3">'.$log_month.'/27</td>'.
          '<td style="padding: 5px;text-align: center;font-weight:bold;" colspan="3">'.$log_month.'/28</td>'.
          '<td style="padding: 5px;text-align: center;font-weight:bold;" colspan="3">'.$log_month.'/29</td>'.
          '<td style="padding: 5px;text-align: center;font-weight:bold;" colspan="3">'.$log_month.'/30</td>'.
          '</tr>';
          echo '<tr>' .
          '<td style="padding: 3px;text-align: center;font-weight:bold;" colspan="3">'; $day21 = $log_year.'-'.$log_month.'-21'; $d21 = date("l", strtotime($day21)); echo ($d21 == "Sunday") ? '<span style="color:red;">'.$d21.'</span>' : $d21; echo '</td>'.
          '<td style="padding: 3px;text-align: center;font-weight:bold;" colspan="3">'; $day22 = $log_year.'-'.$log_month.'-22'; $d22 = date("l", strtotime($day22)); echo ($d22 == "Sunday") ? '<span style="color:red;">'.$d22.'</span>' : $d22; echo '</td>'.
          '<td style="padding: 3px;text-align: center;font-weight:bold;" colspan="3">'; $day23 = $log_year.'-'.$log_month.'-23'; $d23 = date("l", strtotime($day23)); echo ($d23 == "Sunday") ? '<span style="color:red;">'.$d23.'</span>' : $d23; echo '</td>'.
          '<td style="padding: 3px;text-align: center;font-weight:bold;" colspan="3">'; $day24 = $log_year.'-'.$log_month.'-24'; $d24 = date("l", strtotime($day24)); echo ($d24 == "Sunday") ? '<span style="color:red;">'.$d24.'</span>' : $d24; echo '</td>'.
          '<td style="padding: 3px;text-align: center;font-weight:bold;" colspan="3">'; $day25 = $log_year.'-'.$log_month.'-25'; $d25 = date("l", strtotime($day25)); echo ($d25 == "Sunday") ? '<span style="color:red;">'.$d25.'</span>' : $d25; echo '</td>'.
          '<td style="padding: 3px;text-align: center;font-weight:bold;" colspan="3">'; $day26 = $log_year.'-'.$log_month.'-26'; $d26 = date("l", strtotime($day26)); echo ($d26 == "Sunday") ? '<span style="color:red;">'.$d26.'</span>' : $d26; echo '</td>'.
          '<td style="padding: 3px;text-align: center;font-weight:bold;" colspan="3">'; $day27 = $log_year.'-'.$log_month.'-27'; $d27 = date("l", strtotime($day27)); echo ($d27 == "Sunday") ? '<span style="color:red;">'.$d27.'</span>' : $d27; echo '</td>'.
          '<td style="padding: 3px;text-align: center;font-weight:bold;" colspan="3">'; $day28 = $log_year.'-'.$log_month.'-28'; $d28 = date("l", strtotime($day28)); echo ($d28 == "Sunday") ? '<span style="color:red;">'.$d28.'</span>' : $d28; echo '</td>'.
          '<td style="padding: 3px;text-align: center;font-weight:bold;" colspan="3">'; $day29 = $log_year.'-'.$log_month.'-29'; $d29 = date("l", strtotime($day29)); echo ($d29 == "Sunday") ? '<span style="color:red;">'.$d29.'</span>' : $d29; echo '</td>'.
          '<td style="padding: 3px;text-align: center;font-weight:bold;" colspan="3">'; $day30 = $log_year.'-'.$log_month.'-30'; $d30 = date("l", strtotime($day30)); echo ($d30 == "Sunday") ? '<span style="color:red;">'.$d30.'</span>' : $d30; echo '</td>'.
          '</tr>';
          echo '<tr>'.
          '<td style="padding: 5px;text-align: center;" >AM</td>'.
          '<td style="padding: 5px;text-align: center;" >PM</td>'.
          '<td style="padding: 5px;text-align: center;" >OT</td>'.
          '<td style="padding: 5px;text-align: center;" >AM</td>'.
          '<td style="padding: 5px;text-align: center;" >PM</td>'.
          '<td style="padding: 5px;text-align: center;" >OT</td>'.
          '<td style="padding: 5px;text-align: center;" >AM</td>'.
          '<td style="padding: 5px;text-align: center;" >PM</td>'.
          '<td style="padding: 5px;text-align: center;" >OT</td>'.
          '<td style="padding: 5px;text-align: center;" >AM</td>'.
          '<td style="padding: 5px;text-align: center;" >PM</td>'.
          '<td style="padding: 5px;text-align: center;" >OT</td>'.
          '<td style="padding: 5px;text-align: center;" >AM</td>'.
          '<td style="padding: 5px;text-align: center;" >PM</td>'.
          '<td style="padding: 5px;text-align: center;" >OT</td>'.
          '<td style="padding: 5px;text-align: center;" >AM</td>'.
          '<td style="padding: 5px;text-align: center;" >PM</td>'.
          '<td style="padding: 5px;text-align: center;" >OT</td>'.
          '<td style="padding: 5px;text-align: center;" >AM</td>'.
          '<td style="padding: 5px;text-align: center;" >PM</td>'.
          '<td style="padding: 5px;text-align: center;" >OT</td>'.
          '<td style="padding: 5px;text-align: center;" >AM</td>'.
          '<td style="padding: 5px;text-align: center;" >PM</td>'.
          '<td style="padding: 5px;text-align: center;" >OT</td>'.
          '<td style="padding: 5px;text-align: center;" >AM</td>'.
          '<td style="padding: 5px;text-align: center;" >PM</td>'.
          '<td style="padding: 5px;text-align: center;" >OT</td>'.
          '<td style="padding: 5px;text-align: center;" >AM</td>'.
          '<td style="padding: 5px;text-align: center;" >PM</td>'.
          '<td style="padding: 5px;text-align: center;" >OT</td>'.
          '</tr>'.
          '<tr>';
          $arrayday3 = ['21','22','23','24','25','26','27','28','29','30'];
          for ($i=0; $i<10; $i++) { 
            $showday = $this->db->query("SELECT * FROM tblattendance_history WHERE MONTH(att_time)='".$show_pdf->month_log."' AND uid='".$showname->emp_id."' AND DAY(att_time)='".$arrayday3[$i]."' GROUP BY att_time ")->row();
            if(count($showday) > 0) {
              $am = '<b style="color:red;font-size: 12px">✘</b>';
              $pm = '<b style="color:red;font-size: 12px">✘</b>';
            } else {
              $am = '<b style="color:white;font-size: 12px;">.</b>';
              $pm = '<b style="color:white;font-size: 12px;">.</b>';
            }
            if ($showday->period == 'Whole Day'){ 
              $am = '<b style="color:green;font-size: 12px">✔</b>';
              $pm = '<b style="color:green;font-size: 12px">✔</b>';  }
            if ($showday->period == 'AM'){ 
              $am = '<b style="color:green;font-size: 12px">✔</b>';  } 
            if ($showday->period == 'PM'){ 
              $pm = '<b style="color:green;font-size: 12px">✔</b>';  }

            if ($showday->period == 'awol'){ 
              $am = '<b style="color:red;font-size: 12px">✘</b>';
              $pm = '<b style="color:red;font-size: 12px">✘</b>'; }
            if ($showday->period == 'onleave'){ 
              $am = '<b style="color:green;font-size: 12px">~</b>';
              $pm = '<b style="color:green;font-size: 12px">~</b>'; }
            if ($showday->period == 'sick'){ 
              $am = '<b style="color:red;font-size: 12px">!</b>';
              $pm = '<b style="color:red;font-size: 12px">!</b>'; }
            if ($showday->period == 'excused'){ 
              $am = '<b style="color:green;font-size: 12px">—</b>';
              $pm = '<b style="color:green;font-size: 12px">—</b>'; }
            if ($showday->period == 'holiday' || !empty($showday->holiday_type)){ 
              $am = '<b style="color:red;font-size: 12px">•</b>';
              $pm = '<b style="color:red;font-size: 12px">•</b>'; }
            
            echo '<td style="padding: 5px;text-align: center;" >'.$am.'</td>'.
            '<td style="padding: 5px;text-align: center;" >'.$pm.'</td>'.
            '<td style="padding: 5px;text-align: center;" >'.$showday->overtime.'</td>';
          } 
          echo '</tr>';
          if($month_ld == 31) {
            echo '<tr>'.
              '<td style="padding: 5px;text-align: center;font-weight:bold;" colspan="3">'.$log_month.'/31</td>'.
              '<td></td>'.
              '<td></td>'.
              '<td></td>'.
              '<td></td>'.
              '<td></td>'.
              '<td></td>'.
              '<td></td>'.
              '<td></td>'.
              '<td></td>'.
              '<td></td>'.
              '<td></td>'.
              '<td></td>'.
              '<td></td>'.
              '<td></td>'.
              '<td></td>'.
              '<td></td>'.
              '<td></td>'.
              '<td></td>'.
              '<td></td>'.
              '<td></td>'.
              '<td></td>'.
              '<td></td>'.
              '<td></td>'.
              '<td></td>'.
              '<td></td>'.
              '<td></td>'.
              '<td></td>'.
              '<td></td>'.
            '</tr>';
            echo '<tr>' .
              '<td style="padding: 3px;text-align: center;font-weight:bold;" colspan="3">'; $day31 = $log_year.'-'.$log_month.'-31'; $d31 = date("l", strtotime($day31)); echo ($d31 == "Sunday") ? '<span style="color:red;">'.$d31.'</span>' : $d31; echo '</td>'.
              '<td></td>'.
              '<td></td>'.
              '<td></td>'.
              '<td></td>'.
              '<td></td>'.
              '<td></td>'.
              '<td></td>'.
              '<td></td>'.
              '<td></td>'.
              '<td></td>'.
              '<td></td>'.
              '<td></td>'.
              '<td></td>'.
              '<td></td>'.
              '<td></td>'.
              '<td></td>'.
              '<td></td>'.
              '<td></td>'.
              '<td></td>'.
              '<td></td>'.
              '<td></td>'.
              '<td></td>'.
              '<td></td>'.
              '<td></td>'.
              '<td></td>'.
              '<td></td>'.
              '<td></td>'.
            '</tr>';
            echo '<tr>'.
              '<td style="padding: 5px;text-align: center;" >AM</td>'.
              '<td style="padding: 5px;text-align: center;" >PM</td>'.
              '<td style="padding: 5px;text-align: center;" >OT</td>'.
              '<td></td>'.
              '<td></td>'.
              '<td></td>'.
              '<td></td>'.
              '<td></td>'.
              '<td></td>'.
              '<td></td>'.
              '<td></td>'.
              '<td></td>'.
              '<td></td>'.
              '<td></td>'.
              '<td></td>'.
              '<td></td>'.
              '<td></td>'.
              '<td></td>'.
              '<td></td>'.
              '<td></td>'.
              '<td></td>'.
              '<td></td>'.
              '<td></td>'.
              '<td></td>'.
              '<td></td>'.
              '<td></td>'.
              '<td></td>'.
              '<td></td>'.
              '<td></td>'.
              '<td></td>'.
              '<td></td>'.
              '<td></td>'.
            '</tr>'.
            '<tr>';
            $showday = $this->db->query("SELECT * FROM tblattendance_history WHERE MONTH(att_time)='".$show_pdf->month_log."' AND uid='".$showname->emp_id."' AND DAY(att_time)='31' GROUP BY att_time ")->row();
            if(count($showday) > 0) {
              $am = '<b style="color:red;font-size: 12px">✘</b>';
              $pm = '<b style="color:red;font-size: 12px">✘</b>';
            } else {
              $am = '<b style="color:white;font-size: 12px;">.</b>';
              $pm = '<b style="color:white;font-size: 12px;">.</b>';
            }
            if ($showday->period == 'Whole Day'){ 
              $am = '<b style="color:green;font-size: 12px">✔</b>';
              $pm = '<b style="color:green;font-size: 12px">✔</b>';  }
            if ($showday->period == 'AM'){ 
              $am = '<b style="color:green;font-size: 12px">✔</b>';  } 
            if ($showday->period == 'PM'){ 
              $pm = '<b style="color:green;font-size: 12px">✔</b>';  }

            if ($showday->period == 'awol'){ 
              $am = '<b style="color:red;font-size: 12px">✘</b>';
              $pm = '<b style="color:red;font-size: 12px">✘</b>'; }
            if ($showday->period == 'onleave'){ 
              $am = '<b style="color:green;font-size: 12px">~</b>';
              $pm = '<b style="color:green;font-size: 12px">~</b>'; }
            if ($showday->period == 'sick'){ 
              $am = '<b style="color:red;font-size: 12px">!</b>';
              $pm = '<b style="color:red;font-size: 12px">!</b>'; }
            if ($showday->period == 'excused'){ 
              $am = '<b style="color:green;font-size: 12px">—</b>';
              $pm = '<b style="color:green;font-size: 12px">—</b>'; }
            if ($showday->period == 'holiday' || !empty($showday->holiday_type)){ 
              $am = '<b style="color:red;font-size: 12px">•</b>';
              $pm = '<b style="color:red;font-size: 12px">•</b>'; }
            
            echo '<td></td>'.
            '<td style="padding: 5px;text-align: center;" >'.$am.'</td>'.
            '<td style="padding: 5px;text-align: center;" >'.$pm.'</td>'.
            '<td style="padding: 5px;text-align: center;" >'.$showday->overtime.'</td>'.
            '<td></td>'.
            '<td></td>'.
            '<td></td>'.
            '<td></td>'.
            '<td></td>'.
            '<td></td>'.
            '<td></td>'.
            '<td></td>'.
            '<td></td>'.
            '<td></td>'.
            '<td></td>'.
            '<td></td>'.
            '<td></td>'.
            '<td></td>'.
            '<td></td>'.
            '<td></td>'.
            '<td></td>'.
            '<td></td>'.
            '<td></td>'.
            '<td></td>'.
            '<td></td>'.
            '<td></td>'.
            '<td></td>'.
            '<td></td>'.
            '<td></td>'.
            '<td></td>'.
            '<td></td>'.
            '<td></td>'.
            '</tr>';
          }

          $id = $id + 1; 
        }
      ?>
    </tbody>
</table>
  <table class="table" id="table2">
    <tr>
      <th style="text-align: left;">
         <h6><strong>Facilitated by:</strong></h6> <br> &nbsp;&nbsp;&nbsp; John Kenneth Telmo &nbsp;&nbsp;&nbsp;
         <h6 style="margin-left: 22px"><b style='border-top: solid 1px #000000;display: inline;padding-top: 5px;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Sole-Proprietor &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></h6>
      </th>
      <th style="text-align: left;">
         <h6><strong>Noted by:</strong></h6> <br> &nbsp;&nbsp;&nbsp; John Kenneth Telmo &nbsp;&nbsp;&nbsp;
         <h6 style="margin-left: 22px"><b style='border-top: solid 1px #000000;display: inline;padding-top: 5px;'>&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;</b></h6>
      </th>
      <th style="text-align: left;">
         <h6><strong>Legend:</strong></h6> <br>
         <h6 style="margin-left: 30px">
            <b style="color:green;font-size: 12px">✔</b>&nbsp;&nbsp;Present<br><br>
            <b style="color:red;font-size: 12px">✘</b>&nbsp;&nbsp;Absent Without Leave<br><br>
            <b style="color:green;font-size: 12px">~</b>&nbsp;&nbsp;On Leave<br><br>
            <b style="color:red;font-size: 12px">!</b>&nbsp;&nbsp;&nbsp;Sick<br><br>
            <b style="color:green;font-size: 12px">—</b>&nbsp;&nbsp;Excused<br><br>
            <b style="color:red;font-size: 12px">•</b>&nbsp;&nbsp;&nbsp;Holiday<br><br>
         </h6>
      </th>
    </tr>
  </table>

</body>
</html>
