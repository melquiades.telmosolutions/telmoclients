<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
   <div class="content">
      <div class="row">
         <div class="col-md-12">
            <div class="panel_s mbot10">
              <div class="panel-body">
                <?php if(isset($emp_s_s) || isset($e_emp_s_s)) { ?>
                  <a style="text-decoration: none;" href="<?php echo admin_url('payroll/create_s_setup_') ?>">
                    <button type="button" class="btn btn-info">
                      <?php echo _l('add_salary_setup'); ?>
                    </button>
                  </a>
                <?php } else { ?>
                  <button type="button" class="btn btn-info modal-md" id="add_new" data-target="#adds" data-toggle="modal">
                      <?php echo _l('add_salary_setup'); ?>
                    </button>
                <?php } ?>
              </div>
            </div>
            <div id="adds" class="modal fade" role="dialog">
              <div class="modal-dialog modal-lg">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <strong><?php echo _l('salary_setup');?></strong>
                  </div>
                  <?php if(isset($e_emp_s_s)) { echo form_open(admin_url('payroll/update_s_setup/'.$e_emp_s_s->employee_id),array('id'=>'salary_setup_form')); } else { echo form_open(admin_url('payroll/create_s_setup'),array('id'=>'salary_setup_form')); } ?>
                    <div class="modal-body">
                      <div class="form-group row">
                        <div class="col-sm-6">
                          <div class="form-group">
                            <span style='color:red;'>*</span> <label for="employee_name"><?php echo _l('employee_name'); ?></label>

                            <input type="hidden" name="view_id" id="view_id" value="<?php if(isset($emp_s_s)) echo $emp_s_s->employee_id; ?>">
                            <input type="hidden" name="edit_id" id="edit_id" value="<?php if(isset($e_emp_s_s)) echo $e_emp_s_s->employee_id; ?>">

                            <select name="employee_name" class="form-control" id="ename" required>
                              <option value="">Nothing Selected</option>
                              <?php foreach($ss_employee as $row_emp) { ?>
                                <option <?php if($emp_s_s->employee_id == $row_emp['emp_id']) { echo 'selected'; } elseif ($e_emp_s_s->employee_id == $row_emp['emp_id']) { echo 'selected'; } ?> value="<?php echo $row_emp['emp_id'] ?>"><?php echo $row_emp['first_name'] . ' ' . $row_emp['last_name']; ?></option>
                              <?php } ?>
                            </select>
                          </div>
                        </div>
                        <div class="hide col-sm-3">
                            <label for="month_year"><?php echo _l('s_m'); ?></label>
                            <input type="text" class="form-control monthYearPicker" name="month_year" placeholder="<?php echo _l('salary_month') ?>" id="month_year" required>
                        </div>
                        <div class="col-sm-3">
                          <label for="basic"><?php echo _l('rate'); ?></label>
                          <input type="number" id="basic" name="basic" class="form-control" placeholder=".00" value="<?php echo (isset($emp_s_s) ? number_format($emp_s_s->rate, 2) : ''); echo (isset($e_emp_s_s) ? number_format($e_emp_s_s->rate, 2) : ''); ?>" disabled>
                        </div>
                        <div class="col-sm-3">
                          <div class="form-group">
                            <input type="hidden" name="setup_rate_type_id" id="setup_rate_type_id" value="<?php echo (isset($emp_s_s) ? $emp_s_s->rate_type : ''); echo (isset($e_emp_s_s) ? $e_emp_s_s->rate_type : ''); ?>">
                            <label for="payment_modes"><?php echo _l('payment_mode'); ?></label>
                            <select name="payment_modes" class="form-control selectpicker" id="pmodes">
                              <option value="">Nothing Selected</option>
                              <?php foreach($pay_modes as $row_pm) { ?>
                                <option <?php if($emp_s_s->paymentmode == $row_pm['id']) { echo 'selected'; } elseif ($e_emp_s_s->paymentmode == $row_pm['id']) { echo 'selected'; } ?> value="<?php echo $row_pm['id']; ?>"><?php echo $row_pm['name']; ?></option>
                              <?php } ?>
                            </select>
                          </div>
                        </div>
                        <div class="col-md-12">
                          <hr>
                          <div class="panel-body">
                            <table style="border: 1px solid black; margin-top: -20px;" width="100%">
                              <div class="row">
                                <td class="col-md-6" style="text-align: left;border: 1px solid black; padding-bottom: 25px;">
                                  <h4  style="text-decoration: underline;font-weight: bold;padding-top:20px;text-align: center"><?php echo _l('ss_addition'),'/s';?></h4><br>
                                  <table id="add">
                                    <?php $x=0; if(isset($emp_s_s)) { 
                                      $bonusq = $this->db->where('employee_id', $emp_s_s->emp_id)->where('type', 'bonus')->join(db_prefix() . 'bonus_setup', db_prefix() . 'bonus_setup.bonus_id =' . db_prefix() . 'employee_salary_setup.salary_type_id')->get(db_prefix().'employee_salary_setup')->result_array();
                                      foreach($bonusq as $rowbonus) {
                                        if(!empty($rowbonus['bonus_name'])) { ?>
                                        <tr>
                                          <th style="padding: 10px;"><?php echo $rowbonus['bonus_name']; ?></th>
                                          <td style="padding: 10px;">
                                            <input type="text" name="bonus[<?php echo $rowbonus['bonus_id']; ?>]" class="form-control addamount" id="bonusid" onkeyup="summary()" value="<?php echo $rowbonus['amount'], '.00'; ?>">
                                          </td>
                                        </tr>
                                      <?php } }
                                      foreach($emp_ss_amount_o as $row_sl) { ?> 
                                      <tr>
                                        <th style="padding: 10px;"><?php echo $row_sl['benefit_name'], ' <small>(' . $currency->symbol . ')</small>';?></th>
                                        <td style="padding: 10px;">
                                          <input type="number" name="amount[<?php echo $row_sl['benefit_id']; ?>]" class="form-control addamount" placeholder=".00" onkeyup="summary()" id="add_<?php echo $x;?>" value="<?php echo (isset($emp_ss_amount_o) ? $row_sl['amount'].'.00' : ''); ?>">
                                        </td>
                                      </tr>
                                    <?php } } else if(isset($e_emp_s_s)) { 
                                      $bonusq = $this->db->where('employee_id', $e_emp_s_s->emp_id)->where('type', 'bonus')->join(db_prefix() . 'bonus_setup', db_prefix() . 'bonus_setup.bonus_id =' . db_prefix() . 'employee_salary_setup.salary_type_id')->get(db_prefix().'employee_salary_setup')->result_array();
                                      foreach($bonusq as $key => $rowbonus) {
                                        if(!empty($rowbonus['bonus_name'])) { ?>
                                          <tr>
                                            <th style="padding: 10px;"><div class="div_<?php echo $key; ?>"><?php echo $rowbonus['bonus_name']; ?></div>
                                            </th>
                                            <td style="padding: 10px;"><div class="div_<?php echo $key; ?>">
                                              <input type="text" name="bonus[<?php echo $rowbonus['bonus_id']; ?>]" class="form-control addamount" id="bonusid" onkeyup="summary()" value="<?php echo $rowbonus['amount'], '.00'; ?>"></div>
                                            </td>
                                            <td><div class="div_<?php echo $key; ?>">
                                              <span style="cursor:pointer;display:inline-block;" onclick="removeBtn_(<?php echo $key; ?>)"><i class="fa fa-window-close" style="font-size:20px;color:red"></i></span></div>
                                            </td>
                                          </tr>
                                          <input type="hidden" name="bonusid_id" class="bonusid_id<?php echo $key; ?>" value="<?php echo $rowbonus['bonus_id']; ?>">
                                        <input type="hidden" name="remove_id[]" class="remove_id<?php echo $key; ?>">
                                      <?php } $bon[] = $rowbonus['salary_type_id']; }
                                      $bon_imp = implode(',', $bon);
                                      $where1 = date('m/Y');
                                      $where2 = date('m/Y', strtotime("-2 MONTH"));
                                      if(!empty($bon_imp)) {
                                        $bonus_mod = $this->db->query("SELECT * FROM " . db_prefix() . "bonus_setup WHERE bonus_id NOT IN (" . $bon_imp . ") AND (bonus_date > '" . $where2 . "' AND bonus_date <= '" . $where1 . "') AND bonus_emp_id = " . $e_emp_s_s->emp_id . " ")->result_array(); } 
                                      else {
                                        $bonus_mod = $this->db->query("SELECT * FROM " . db_prefix() . "bonus_setup WHERE (bonus_date > '" . $where2 . "' AND bonus_date <= '" . $where1 . "') AND bonus_emp_id = " . $e_emp_s_s->emp_id . " ")->result_array();
                                      }
                                      foreach($bonus_mod as $key => $bonusmod) { ?>
                                        <tr>
                                          <th style="padding: 10px;" class="bonusnameid"><div class="div<?php echo $key; ?>"><?php echo $bonusmod['bonus_name']; ?></div>
                                          </th>
                                          <td style="padding: 10px;" class="bonusamountid"><div class="div<?php echo $key; ?>">
                                            <input type="text" name="bonus[<?php echo $bonusmod['bonus_id']; ?>]" class="form-control addamount bonusid" onkeyup="summary()" value="<?php echo $bonusmod['bonus_amount'];?>.00" style="border: 1px solid green;"></div>
                                          </td>
                                          <td><div class="div<?php echo $key; ?>">
                                            <span style="cursor:pointer;display:inline-block;" onclick="remove_Btn(<?php echo $key; ?>)"><i class="fa fa-window-close" style="font-size:20px;color:red"></i></span></div>
                                          </td>
                                        </tr>
                                        <input type="hidden" name="bonusidid" class="bonusidid<?php echo $key; ?>" value="<?php echo $bonusmod['bonus_id']; ?>">
                                        <input type="hidden" name="removeid[]" class="removeid<?php echo $key; ?>">
                                      <?php } ?>
                                      <!-- <input type="hidden" id="query1" value="<?php echo count($bonusq); ?>">
                                      <input type="hidden" id="query2" value="<?php echo count($bonus_mod); ?>">
                                      <input type="hidden" name="removeid[]" id="removeid"> -->
                                      <?php foreach($e_emp_ss_amount_o as $row_sl) { ?>
                                      <tr>
                                        <th style="padding: 10px;"><?php echo $row_sl['benefit_name'], ' <small>(' . $currency->symbol . ')</small>';?></th>
                                        <td style="padding: 10px;">
                                          <input type="number" name="amount[<?php echo $row_sl['benefit_id']; ?>]" class="form-control addamount" placeholder=".00" onkeyup="summary()" id="add_<?php echo $x;?>" value="<?php echo (isset($e_emp_ss_amount_o) ? $row_sl['amount'].'.00' : ''); ?>">
                                        </td>
                                      </tr>
                                    <?php } } else { ?>
                                      <?php foreach($salary_type_o as $row_sl) { ?>
                                      <tr>
                                        <th style="padding: 10px;"><?php echo $row_sl['benefit_name'], ' <small>(' . $currency->symbol . ')</small>';?></th>
                                        <td style="padding: 10px;">
                                          <input type="number" name="amount[<?php echo $row_sl['benefit_id']; ?>]" class="form-control addamount" placeholder=".00" onkeyup="summary()" id="add_<?php echo $x;?>" value="<?php echo ($row_sl['default_amount'] != 0) ? number_format($row_sl['default_amount'], 2) : ''; ?>">
                                          <!-- <?php if($row_sl['benefit_name'] == 'Overtime') { echo 'readonly'; } ?>  -->
                                        </td>
                                      </tr>
                                    <?php } ?> <div class="tr_add_bonus"></div> <?php } $x++; ?>
                                    
                                  </table>
                                </td>
                                <td class="col-md-6" style="text-align: left; padding-bottom: 25px;">
                                  <h4 style="text-decoration: underline;font-weight: bold;text-align: center"><?php echo _l('ss_deduction'),'/s';?></h4><br>
                                  <table id="dduct">
                                    <?php $y=0; if(isset($emp_s_s)) { foreach($emp_ss_amount_z as $row_sl) { ?> 
                                      <tr class="deda">
                                        <th style="padding: 10px;"><?php echo $row_sl['benefit_name'], ' <small>(' . $currency->symbol . ')</small>';?></th>
                                        <td style="padding: 10px;">
                                          <input type="number" name="amount[<?php echo $row_sl['benefit_id']; ?>]" class="form-control deducamount" placeholder=".00" onkeyup="summary()" id="add_<?php echo $y;?>" value="<?php echo (isset($emp_ss_amount_z) ? $row_sl['amount'].'.00' : ''); ?>">
                                        </td>
                                      </tr>
                                    <?php } } elseif(isset($e_emp_s_s)) { foreach($e_emp_ss_amount_z as $row_sl) { ?>
                                      <tr class="deda">
                                        <th style="padding: 10px;"><?php echo $row_sl['benefit_name'], ' <small>(' . $currency->symbol . ')</small>';?></th>
                                        <td style="padding: 10px;">
                                          <input type="number" name="amount[<?php echo $row_sl['benefit_id']; ?>]" class="form-control deducamount" placeholder=".00" onkeyup="summary()" id="add_<?php echo $y;?>" value="<?php echo (isset($e_emp_ss_amount_z) ? $row_sl['amount'].'.00' : ''); ?>">
                                        </td>
                                      </tr>
                                    <?php } } else { foreach($salary_type_z as $row_sl) { ?>
                                      <tr class="deda">
                                        <th style="padding: 10px;"><?php echo $row_sl['benefit_name'], ' <small>(' . $currency->symbol . ')</small>';?></th>
                                        <td style="padding: 10px;">
                                          <input type="number" name="amount[<?php echo $row_sl['benefit_id']; ?>]" class="form-control deducamount" placeholder=".00" onkeyup="summary()" id="add_<?php echo $y;?>" value="<?php echo ($row_sl['default_amount'] != 0) ? number_format($row_sl['default_amount'], 2) : ''; ?>">
                                        </td>
                                      </tr>
                                    <?php } } $y++; ?>
                                  </table>
                                </td>
                              </div>
                            </table>
                          </div>
                          <div class="form-group" style="margin-bottom: -20px;">
                            <div class="row">
                              <div class="col-sm-1">
                                <h4><?php echo _l('total_');?></h4>
                              </div>
                              <div class="col-sm-5">
                                <?php echo render_input('add_total', '', '', 'number', array('placeholder'=>_l('peso'), 'readonly'=>true), '', '', array('id'=>'add_total')); ?>
                              </div>
                              <div class="col-sm-5">
                                <?php echo render_input('deduct_total', '', '', 'number', array('placeholder'=>_l('peso'), 'readonly'=>true), '', '', array('id'=>'deduct_total')); ?>
                              </div>
                            </div>
                          </div>
                          <hr>
                          <div class="form-group">
                            <label for="total"><?php echo _l('total') ?></label>
                            <input type="text" name="total" id="total" class="form-control" value="<?php echo (isset($emp_s_s) ? $emp_s_s->total.'.00' : ''); echo (isset($e_emp_s_s) ? $e_emp_s_s->total.'.00' : ''); ?>" readonly>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="modal-footer">
                      <div class="form-group text-right">
                        <button type="close" class="btn btn-default w-md m-b-5" data-dismiss="modal"><?php echo (isset($emp_s_s) ? _l('close') :_l('cancel')) ; ?></button>
                        <button type="submit" data-form="salary_setup_form" id="add_btn" onclick="ss_submit()" class="btn btn-success w-md m-b-5" autocomplete="off" data-loading-text="<?php echo _l('wait_text'); ?>"><?php echo isset($e_emp_s_s) ? _l('u_salary_setup') : _l('add_salary_setup'); ?></button>
                      </div>
                    </div>
                  <?php echo form_close(); ?>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="panel_s">
                  <div class="panel-body">

                    <?php
                      $table_data = array(
                        _l('employee_id_num'),
                        _l('name'),
                        // _l('month'),
                        _l('nav_rate_type'),
                        _l('rate'),
                        _l('total'),
                      );
                      render_datatable($table_data,'salarysetup');
                    ?>
                  </div>
                </div>
              </div>
            </div>
         </div>
      </div>
   </div>
</div>
<?php init_tail(); ?>
<script>
  function ss_submit() {
    var emp = $('#ename').val();
    if(!empty(emp)) {
      $("#salary_setup_form").submit();
    }
  }

  function removeBtn(i){
    $('.tr_'+i).remove();
    $('#add_total').val('');
    summary();
  }

  function removeBtn_(i){
    $('.div_'+i).remove();
    var bid = $('.bonusid_id'+i).val();
    $('.remove_id'+i).val(bid);
    $('#add_total').val('');
    summary();
  }
  function remove_Btn(i){
    $('.div'+i).remove();
    var bid = $('.bonusidid'+i).val();
    $('.removeid'+i).val(bid);
    $('#add_total').val('');
    summary();
  }

  function summary() {
    var emp = $('select[name="employee_name"]').val();
    if(!empty(emp)) {
      var add = 0;
      var deduct = 0;
      // var basic = parseInt($('#basic').val());
      $(".addamount").each(function() {
        var value_add = this.value;
        isNaN(value_add) || 0 == (value_add).length || (add += parseFloat(value_add * 1))
        document.getElementById('add_total').value=add;
      });
      $(".deducamount").each(function() {
        var value_ded = this.value;
        isNaN(value_ded) || 0 == (value_ded).length || (deduct += parseFloat(value_ded * 1))
        document.getElementById('deduct_total').value=deduct;
       });
      document.getElementById('total').value=add-deduct;
    }
  }

  $(function(){
    $('.addamount').prop('disabled', true);
    $('.deducamount').prop('disabled', true);

    var url = window.location.pathname;
    var id = url.substring(url.lastIndexOf('/') + 1);
    if(!empty(id) && $.isNumeric(id)) {
      $('#adds').modal('show');
    }

    var gs = $('#total').val();
    var ei = $('#edit_id').val();
    var vi = $('#view_id').val();

    if(!empty($('.addamount').val()) || !empty($('.deducamount').val())) {
      var add = 0;
      var deduct = 0;

      $(".addamount").each(function() {
        var value_add = this.value;
        isNaN(value_add) || 0 == (value_add).length || (add += parseFloat(value_add * 1))
        document.getElementById('add_total').value=add;
      });
      $(".deducamount").each(function() {
        var value_ded = this.value;
        isNaN(value_ded) || 0 == (value_ded).length || (deduct += parseFloat(value_ded * 1))
        document.getElementById('deduct_total').value=deduct;
       });
      document.getElementById('total').value=add-deduct;
    }

    // view page
    if(!empty(gs) && !empty(id) && !empty(vi)) {
      $('.form-control').prop('disabled', true);
      $('#add_btn').addClass('hide');

      var add = 0;
      var deduct = 0;

      $(".addamount").each(function() {
        var value_add = this.value;
        isNaN(value_add) || 0 == (value_add).length || (add += parseFloat(value_add * 1))
        document.getElementById('add_total').value=add;
      });
      $(".deducamount").each(function() {
        var value_ded = this.value;
        isNaN(value_ded) || 0 == (value_ded).length || (deduct += parseFloat(value_ded * 1))
        document.getElementById('deduct_total').value=deduct;
       });
      document.getElementById('total').value=add-deduct;

      // on click add salary setup button 
      $('#add_new').on('click', function() {
        $('.form-control').val('');
        $('#add_btn').removeClass('hide');
        $('#ename').prop('disabled', false);
        $('#view_id').val('');
      });
    }
    // edit page
    else if(!empty(gs) && !empty(id) && !empty(ei)) {
      $('#ename').prop('disabled', 'disabled');
      $('.addamount').prop('disabled', false);
      $('.deducamount').prop('disabled', false);
      var add = 0;
      var deduct = 0;

      $(".addamount").each(function() {
        var value_add = this.value;
        isNaN(value_add) || 0 == (value_add).length || (add += parseFloat(value_add * 1))
        document.getElementById('add_total').value=add;
      });
      $(".deducamount").each(function() {
        var value_ded = this.value;
        isNaN(value_ded) || 0 == (value_ded).length || (deduct += parseFloat(value_ded * 1))
        document.getElementById('deduct_total').value=deduct;
       });
      document.getElementById('total').value=add-deduct;
    }

    // select employee
    $('select[name="employee_name"]').on('change',function(){
      if($('.bonusnameid').length > 0 && $('.bonusamountid').length > 0) {
        $('.tr_id').remove();
        $('#add_total').val('');
        summary();
      }
      var emp = $(this).val();
      $('.addamount').prop('disabled', false);
      $('.deducamount').prop('disabled', false);
      if(emp != '') {
        $.ajax({
          url: admin_url + 'payroll/emp_rate_type',
          type: 'post',
          dataType: 'json',
          data: {emp : emp},
          success: function(data) {
            document.getElementById('setup_rate_type_id').value=data.rate_type;
            document.getElementById('basic').value=data.rate+'.00';
            $.ajax({
              url: admin_url + 'payroll/emp_rate_type_bonus',
              type: 'post',
              dataType: 'html',
              data: {emp : emp},
              success: function(data) {
                $('.tr_add_bonus').html(data);
                summary();
              }
            });
          }
        });
      } else {
        $('#setup_rate_type_id').val('');
        $('#basic').val('');
      }
    });

    var SalarySetupServerParams = {};
    initDataTable('.table-salarysetup', window.location.href, [0], [0], SalarySetupServerParams,<?php echo hooks()->apply_filters('salarysetup_table_default_order', json_encode(array(0,'desc'))); ?>);
    
    $('.monthYearPicker').datepicker({
      autoclose: true,
      minViewMode: 1,
      format: 'MM yyyy'
    }).on('changeDate', function(selected){
      startDate = new Date(selected.date.valueOf());
      startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
    }); 
  });
</script>
</body>
</html>
