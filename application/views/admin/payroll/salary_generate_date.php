<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
   <div class="content">
      <div class="row">
         <div class="col-md-12">
            <div class="row">
               <div class="col-md-12">
                  <div class="panel_s">
                     <div class="panel-body">
                      <div class="clearfix"></div>
                      <?php
                        $table_data = array(
                          _l('employee_id_num'),
                          _l('employee'),
                          _l('benefit'),
                          _l('bonus'),
                          _l('date'),
                          _l('tot_salary'),
                        );
                        render_datatable($table_data,'salarydate');
                      ?>
                    </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<?php init_tail(); ?>
<script>
   $(function(){
    var SalaryDateServerParams = {};
    initDataTable('.table-salarydate', window.location.href, [0], [0], SalaryDateServerParams,<?php echo hooks()->apply_filters('salarydate_table_default_order', json_encode(array(4,'desc'))); ?>);
  });
</script>
</body>
</html>
