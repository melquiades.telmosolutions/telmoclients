<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
  <div class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="panel_s mbot10">
          <div class="panel-body">
            <div class="row">
              <div class="col-sm-4">
                <div class="panel-body" style="padding-bottom: 10%;">
                  <div class="form-group">
                    <label for="salary_type"><?php echo _l('salary_type'); ?></label>
                    <select name="salary_type" id="salary_type" class="form-control selectpicker">
                      <option value=""></option>
                      <option value="month">Month</option>
                      <option value="period">Period</option>
                    </select>
                  </div>
                  <hr> <br><br>

                  <div class="hide form-group month">
                    <?php echo form_open_multipart(admin_url('payroll/generate_salary_month'),array('class'=>'sg_month_form','id'=>'sg_month_form')); ?>
                      <label for="salary_name"><?php echo _l('s_m'); ?></label>
                      <input type="text" class="form-control monthYearPicker" name="salary_name" placeholder="<?php echo _l('salary_month') ?>" id="salary_name" required>
                      <br>
                      <center>
                        <button type="reset" class="btn btn-default"><?php echo _l('reset'); ?></button>
                        <button type="submit" data-form="sg_month_form" class="btn btn-success" onclick="submit_m()" autocomplete="off" data-loading-text="<?php echo _l('wait_text'); ?>"><?php echo _l('generate'); ?></button>
                      </center>
                    <?php echo form_close(); ?>
                  </div>

                  <div class="hide form-group period">
                    <?php echo form_open_multipart(admin_url('payroll/generate_salary_period'),array('class'=>'sg_period_form','id'=>'sg_period_form')); ?>
                    <form>
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label for="date1"><?php echo _l('from'); ?></label>
                        <input type="text" name="date1" class="form-control datepicker" placeholder="mm-dd-yyyy" required>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label for="date2"><?php echo _l('to'); ?></label>
                        <input type="text" name="date2" class="form-control datepicker" placeholder="mm-dd-yyyy" required>
                      </div>
                    </div>
                    <br>
                    <center>
                      <button type="reset" class="btn btn-default"><?php echo _l('reset'); ?></button>
                      <button type="submit" data-form="sg_period_form" onclick="submit_p()" class="btn btn-success" autocomplete="off" data-loading-text="<?php echo _l('wait_text'); ?>"><?php echo _l('generate'); ?></button>
                    </center>
                    </form>

                    <?php 
                      foreach($q as $qq) { ?>
                        <input type="text" name="name" value="<?php echo $qq['name']; ?>" class="form-control">
                    <?php } ?>
                    <?php echo form_close(); ?>
                  </div>
                </div>
              </div>
              <div class="col-sm-8">
                <div class="panel-body">
                  <?php
                    $table_data = array(
                      _l('id'),
                      _l('name'),
                      _l('start_date'),
                      _l('end_date'),
                      _l('generated_by'),
                    );
                    render_datatable($table_data,'salarygenerate');
                  ?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php init_tail(); ?>
<script src="<?php echo base_url();?>assets/js/bootstrap-datepicker.js"></script>
<link  rel="stylesheet" href="<?php echo base_url();?>assets/css/datepicker3.css">
<script>
  $(function(){
    var SalaryGenerateServerParams = {};
    initDataTable('.table-salarygenerate', window.location.href, [0], [0], SalaryGenerateServerParams,<?php echo hooks()->apply_filters('salarygenerate_table_default_order', json_encode(array(0,'desc'))); ?>);

    $('.monthYearPicker').datepicker({
      autoclose: true,
      minViewMode: 1,
      format: 'MM yyyy'
    }).on('changeDate', function(selected){
      startDate = new Date(selected.date.valueOf());
      startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
    }); 

    $('select[name="salary_type"]').on('change',function(){
      var sl = $('#salary_type').val();

      if(sl == 'month') {
        $('.month').removeClass('hide');
        $('.period').addClass('hide');
      }else if(sl == 'week') {
        $('.month').addClass('hide');
        $('.period').addClass('hide');
      }  else if(sl == 'period') {
        $('.month').addClass('hide');
        $('.period').removeClass('hide');
      } else {
        $('.month').addClass('hide');
        $('.period').addClass('hide');
      }
    });

    function submit_m() {
      $("#sg_month_form").submit();
    }
    function submit_p() {
      $("#sg_period_form").submit();
    }
  });
</script>
</body>
</html>