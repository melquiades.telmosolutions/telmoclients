<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
   <div class="content">
      <div class="row">
         <div class="col-md-12">
            <div class="panel_s mbot10">
              <div class="panel-body">
                <button type="button" class="btn btn-info modal-md" id="add_new" data-target="#adds" data-toggle="modal">
                  <?php echo _l('add_salary_setup');?>
                </button>
              </div>
            </div>
            <div id="adds" class="modal fade" role="dialog">
              <div class="modal-dialog modal-lg">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <strong><?php echo _l('salary_setup');?></strong>
                  </div>
                  <?php echo form_open(admin_url('payroll/create_s_setup'),array('id'=>'salary_setup_form')); ?>
                    <div class="modal-body">
                      <div class="form-group row">
                        <div class="col-sm-6">
                          <div class="form-group">
                            <span style='color:red;'>*</span> <label for="employee_name"><?php echo _l('employee_name'); ?></label>

                            <select name="employee_name" class="form-control" id="ename" required>
                              <option value="">Nothing Selected</option>
                              <?php foreach($ss_employee as $row_emp) { ?>
                                <option value="<?php echo $row_emp['emp_id'] ?>"><?php echo $row_emp['first_name'] . ' ' . $row_emp['last_name']; ?></option>
                              <?php } ?>
                            </select>
                          </div>
                        </div>
                        <div class="hide col-sm-3">
                            <label for="month_year"><?php echo _l('s_m'); ?></label>
                            <input type="text" class="form-control monthYearPicker" name="month_year" placeholder="<?php echo _l('salary_month') ?>" id="month_year" required>
                        </div>
                        <div class="col-sm-3">
                          <label for="basic"><?php echo _l('rate'); ?></label>
                          <input type="number" id="basic" name="basic" class="form-control" placeholder="<?php echo _l('peso'); ?>" disabled>
                        </div>
                        <div class="col-sm-3">
                          <div class="form-group">
                            <input type="hidden" name="setup_rate_type_id" id="setup_rate_type_id">
                            <label for="payment_modes"><?php echo _l('payment_mode'); ?></label>
                            <select name="payment_modes" class="form-control selectpicker" id="pmodes">
                              <option value="">Nothing Selected</option>
                              <?php foreach($pay_modes as $row_pm) { ?>
                                <option value="<?php echo $row_pm['id']; ?>"><?php echo $row_pm['name']; ?></option>
                              <?php } ?>
                            </select>
                          </div>
                        </div>
                        <div class="col-md-12">
                          <hr>
                          <div class="panel-body">
                            <table style="border: 1px solid black; margin-top: -20px;" width="100%">
                              <div class="row">
                                <td class="col-md-6" style="text-align: left;border: 1px solid black; padding-bottom: 25px;">
                                  <h4  style="text-decoration: underline;font-weight: bold;padding-top:20px;text-align: center"><?php echo _l('ss_addition'),'/s';?></h4><br>
                                  <table id="add">
                                    <?php $x=0; ?>
                                      <?php foreach($salary_type_o as $row_sl) { ?>
                                      <tr>
                                        <th style="padding: 10px;"><?php echo $row_sl['benefit_name'], ' <small>(' . $currency->symbol . ')</small>';?></th>
                                        <td style="padding: 10px;">
                                          <input type="number" name="amount[<?php echo $row_sl['benefit_id']; ?>]" class="form-control addamount" placeholder=".00" onkeyup="summary()" id="add_<?php echo $x;?>" value="<?php echo ($row_sl['default_amount'] != 0) ? number_format($row_sl['default_amount'], 2) : ''; ?>">
                                        </td>
                                      </tr>
                                    <?php } ?> <div class="tr_add_bonus"></div> <?php $x++; ?>
                                  </table>
                                </td>
                                <td class="col-md-6" style="text-align: left; padding-bottom: 25px;">
                                  <h4 style="text-decoration: underline;font-weight: bold;text-align: center"><?php echo _l('ss_deduction'),'/s';?></h4><br>
                                  <table id="dduct">
                                    <?php $y=0; foreach($salary_type_z as $row_sl) { ?>
                                      <tr class="deda">
                                        <th style="padding: 10px;"><?php echo $row_sl['benefit_name'], ' <small>(' . $currency->symbol . ')</small>';?></th>
                                        <td style="padding: 10px;">
                                          <input type="number" name="amount[<?php echo $row_sl['benefit_id']; ?>]" class="form-control deducamount" placeholder=".00" onkeyup="summary()" id="add_<?php echo $y;?>" value="<?php echo ($row_sl['default_amount'] != 0) ? number_format($row_sl['default_amount'], 2) : ''; ?>">
                                        </td>
                                      </tr>
                                    <?php } $y++; ?>
                                  </table>
                                </td>
                              </div>
                            </table>
                          </div>
                          <div class="form-group" style="margin-bottom: -20px;">
                            <div class="row">
                              <div class="col-sm-1">
                                <h4><?php echo _l('total_');?></h4>
                              </div>
                              <div class="col-sm-5">
                                <?php echo render_input('add_total', '', '', 'number', array('placeholder'=>_l('peso'), 'readonly'=>true), '', '', array('id'=>'add_total')); ?>
                              </div>
                              <div class="col-sm-5">
                                <?php echo render_input('deduct_total', '', '', 'number', array('placeholder'=>_l('peso'), 'readonly'=>true), '', '', array('id'=>'deduct_total')); ?>
                              </div>
                            </div>
                          </div>
                          <hr>
                          <div class="form-group">
                            <label for="total"><?php echo _l('total') ?></label>
                            <input type="text" name="total" id="total" class="form-control" placeholder="<?php echo _l('peso'); ?>" readonly>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="modal-footer">
                      <div class="form-group text-right">
                        <button type="close" class="btn btn-default w-md m-b-5" data-dismiss="modal"><?php echo (isset($emp_s_s) ? _l('close') :_l('cancel')) ; ?></button>
                        <button type="submit" data-form="salary_setup_form" id="add_btn" onclick="ss_submit()" class="btn btn-success w-md m-b-5" autocomplete="off" data-loading-text="<?php echo _l('wait_text'); ?>"><?php echo _l('add_salary_setup'); ?></button>
                      </div>
                    </div>
                  <?php echo form_close(); ?>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="panel_s">
                  <div class="panel-body">

                    <?php
                      $table_data = array(
                        _l('employee_id_num'),
                        _l('name'),
                        _l('nav_rate_type'),
                        _l('rate'),
                        _l('total'),
                      );
                      render_datatable($table_data,'salarysetup');
                    ?>
                  </div>
                </div>
              </div>
            </div>
         </div>
      </div>
   </div>
</div>
<?php init_tail(); ?>
<script>
  function ss_submit() {
    var emp = $('#ename').val();
    if(!empty(emp)) {
      $("#salary_setup_form").submit();
    }
  }

  function removeBtn(i){
    $('.tr_'+i).remove();
    $('#add_total').val('');
    summary();
  }

  function summary() {
    var emp = $('select[name="employee_name"]').val();
    if(!empty(emp)) {
      var add = 0;
      var deduct = 0;
      // var basic = parseInt($('#basic').val());
      $(".addamount").each(function() {
        var value_add = this.value;
        isNaN(value_add) || 0 == (value_add).length || (add += parseFloat(value_add * 1))
        document.getElementById('add_total').value=add;
      });
      $(".deducamount").each(function() {
        var value_ded = this.value;
        isNaN(value_ded) || 0 == (value_ded).length || (deduct += parseFloat(value_ded * 1))
        document.getElementById('deduct_total').value=deduct;
       });
      document.getElementById('total').value=add-deduct;
    }
  }

  $(function(){
    $('#adds'). modal('show');

    $('.addamount').prop('disabled', true);
    $('.deducamount').prop('disabled', true);


    if(!empty($('.addamount').val()) || !empty($('.deducamount').val())) {
      var add = 0;
      var deduct = 0;

      $(".addamount").each(function() {
        var value_add = this.value;
        isNaN(value_add) || 0 == (value_add).length || (add += parseFloat(value_add * 1))
        document.getElementById('add_total').value=add;
      });
      $(".deducamount").each(function() {
        var value_ded = this.value;
        isNaN(value_ded) || 0 == (value_ded).length || (deduct += parseFloat(value_ded * 1))
        document.getElementById('deduct_total').value=deduct;
       });
      document.getElementById('total').value=add-deduct;
    }

    // select employee
    $('select[name="employee_name"]').on('change',function(){
      if($('.bonusnameid').length > 0 && $('.bonusamountid').length > 0) {
        $('.tr_id').remove();
        $('#add_total').val('');
        summary();
      }
      var emp = $(this).val();
      $('.addamount').prop('disabled', false);
      $('.deducamount').prop('disabled', false);
      if(emp != '') {
        $.ajax({
            url: admin_url + 'payroll/emp_rate_type',
            type: 'post',
            dataType: 'json',
            data: {emp : emp},
            success: function(data) {
              document.getElementById('setup_rate_type_id').value=data.rate_type;
              document.getElementById('basic').value=data.rate+'.00';
              $.ajax({
                url: admin_url + 'payroll/emp_rate_type_bonus',
                type: 'post',
                dataType: 'html',
                data: {emp : emp},
                success: function(data) {
                  $('.tr_add_bonus').html(data);
                  summary();
                }
              });
            }
        });
      } else {
        $('#setup_rate_type_id').val('');
        $('#basic').val('');
      }
    });

    var SalarySetupServerParams = {};
    initDataTable('.table-salarysetup', window.location.href, [0], [0], SalarySetupServerParams,<?php echo hooks()->apply_filters('salarysetup_table_default_order', json_encode(array(0,'desc'))); ?>);
    
    $('.monthYearPicker').datepicker({
      autoclose: true,
      minViewMode: 1,
      format: 'MM yyyy'
    }).on('changeDate', function(selected){
      startDate = new Date(selected.date.valueOf());
      startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
    }); 
  });
</script>
</body>
</html>
