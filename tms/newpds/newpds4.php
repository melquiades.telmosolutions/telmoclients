<?php 
$eID = $_GET['eID'];
?> 
<div class="content-box-large">
	<div class="panel-heading">
		<center><h1 class="page-head-line"><p class="text-gray-dark">Personal Data Sheet</p></h1></center>
		 	<hr>
	</div>
	<div class="panel-heading" >
		<center><h5><b>V. Work Experience</b></h5></center>
		<hr>
	</div>
	<div class="row">
		<div class="col-md-3">
			</div>
			<div class="col-sm-6">
				<div class="content-box-large">
					<div class="panel-body">
			               <div class="form-group">
                              	<form role="form" action="" method="post">    
									<label>Inclusive Dates</label>
									<br/>
									<label>From</label>
                                    <input name="fromdate" class="form-control" type="date">
                                    <br>
									<label>To</label>
                                    <input name="todate" class="form-control" type="date">
									<br/>
                                    <label>Position Title (Write in full)</label>
                                    <input name="position" class="form-control" type="text" required>
                                    <br>    							
                                    <label>DEPARTMENT/AGENCY/OFFICE/COMPANY (write in full)</label>
                                    <input name="doac" class="form-control" type="text">
									<br>	
                                    <label>Monthly Salary</label>
                                    <input name="salary" class="form-control" type="text">
									<br>		
									<label>Salary Grade & Step Increment (Format *00.0*)</label>
                                    <input name="sg" class="form-control" type="text">
											
									<label>Status of Appointment</label>
                                    <input name="status" class="form-control" type="text">
											
									<label>Gov't Service (Yes/No)</label>
                                    <input name="govservice" class="form-control" type="text">
							</div>								
			        </div>

			        <div class="already">
			        	<input name="eID" value="<?php echo $eID; ?>" class="form-control" type="hidden">
						<button type="submit" class="btn btn-info" name="save">Save</button>
						<button type="submit" class="btn btn-info" name="next" formnovalidate>Next 5/8</button>
						</form>
			        </div>
			    </div>
			</div>
		</div>
</div>
<?php
	if(isset($_POST['next'])){
		echo "<script> window.location = \"index.php?page=newpds5&eID=$eID\"</script>";
	}elseif(isset($_POST['save'])){
		//Civil Service Eligibility
		$position = mysql_real_escape_string(utf8_decode($_POST['position']));
		$fromdate = $_POST['fromdate'];
		$todate = $_POST['todate'];
		$doac = mysql_real_escape_string(utf8_decode($_POST['doac']));
		$salary = mysql_real_escape_string(utf8_decode($_POST['salary']));
		$sg = mysql_real_escape_string(utf8_decode($_POST['sg']));
		$status = mysql_real_escape_string(utf8_decode($_POST['status']));
		$govservice = mysql_real_escape_string(utf8_decode($_POST['govservice']));
		$eID = $_POST['eID'];	
		$sql = "SELECT `id`, `bioID`, `surname`, `firstname`, `middlename`, `namext` FROM `personalinfo` WHERE `bioID`=$eID";			
		$query = mysql_query($sql);		
		while($row = mysql_fetch_assoc($query)){
			$id = $row['id'];
			$eID = $row['bioID'];
			$fullname = mysql_real_escape_string($row['surname'].", ".$row['firstname']." ".$row['namext']." ".$row['middlename']);
		}
		$sql = "INSERT INTO `workexp`  (`id`, `bioID`, `fullname`, `fromdate`, `todate`, `position`, `doac`, `salary`, `sg`, `status`, `govservice`)
									VALUES ('$id', '$eID', '$fullname', '$fromdate', '$todate', '$position', '$doac', '$salary', '$sg', '$status', '$govservice')";
		$query = mysql_query($sql);
		echo "<script type = \"text/javascript\">
											alert(\"Success!.\");
											window.location = \"index.php?page=newpds4&eID=$eID\"
										  </script>";
	}
					
?>