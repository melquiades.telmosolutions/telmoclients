<?php 
$eID = $_GET['eID'];
?>
<div class="content-box-large">
	<div class="panel-heading">
		<center><h1 class="page-head-line"><p class="text-gray-dark">Personal Data Sheet</p></h1></center>
		 	<hr>
	</div>
	<div class="panel-heading" >
		<h5><b>III. Educational Background</b></h5>
		<hr>
	</div>
	<div class="row">
	 	<div class="panel-body">
	 		<div class="col-md-12 panel-info">
	 			<div class="content-box-large">
				<form role="form" action="" method="post">
					<div class="row">
						<div id="rootwizard">
							<div class="navbar">
								<div class="navbar-inner">
								    <div class="container">
								    	<table  class="table table-striped table-bordered table-hover">
								    		<thead class="container">
								    			<tr class="nav nav-pills">
								    				<td class="active"><center><a href="#tab1" data-toggle="tab"><b>Elementary</b></a></td>
								    				<td><center><a href="#tab2" data-toggle="tab"><b>Secondary</b></a></center></td>
								    				<td><center><a href="#tab3" data-toggle="tab"><b>Vocational/Trade Course</b></a></center></td>
								    				<td><center><a href="#tab4" data-toggle="tab"><b>College</b></a></td>
								    				<td><center><a href="#tab5" data-toggle="tab"><b>Graduate Studies I</b></a></center></td>
								    				<td><center><a href="#tab6" data-toggle="tab"><b>Graduate Studies II</b></a></center></td>
								    			</tr>
								    		</thead>
								    	</table>
								    </div>
								</div>
							</div>
							<div class="tab-content">
								<div class="tab-pane active" id="tab1">
									<div class="form-group">
										<div class="col-sm-6">
											<label>Elementary</label>
											<input name="eschool" class="form-control" placeholder="Name of School" type="text" />
											<br>
											<label>Yr. Graduated</label>
											<input name="eyeargrad" class="form-control"  placeholder="Year" type="text" />
											<br>
											<label>Highest Grade (if not graduated)</label>
											<input name="ehgrade" class="form-control"  type="text" />
										</div>
										<div class="col-sm-6">
											<label>From Year</label>
											<input name="efyear" class="form-control"  placeholder="Year" type="text" />
											<br>
											<label>To Year</label>
											<input name="etyear" class="form-control" placeholder="Year" type="text" />
											<br>
											<label>Scholarship/Academic Honors</label>
											<input name="eacad" class="form-control"  type="text" />
										</div>
									</div>
								</div>
								<div class="tab-pane" id="tab2">
									<div class="form-group">
										<div class="col-sm-6">
											<label>Secondary</label>
											<input name="sschool" class="form-control" placeholder="Name of School" type="text" />
											<br>
											<label>Yr. Graduated</label>
											<input name="syeargrad" class="form-control" placeholder="Year" type="text" />
											<br>
											<label>Highest Level (if not graduated)</label>
											<input name="shgrade" class="form-control"  type="text" />
										</div>
										<div class="col-sm-6">
											<label>From Year</label>
											<input name="sfyear" class="form-control" placeholder="Year" type="text" />
											<br>
											<label>To Year</label>
											<input name="styear" class="form-control" placeholder="Year" type="text" />
											<br>
											<label>Scholarship/Academic Honors</label>
											<input name="sacad" class="form-control"  type="text" />
										</div>
									</div>
								</div>
								<div class="tab-pane" id="tab3">
									<div class="form-group">
										<div class="col-sm-6">
											<label>Vocational/Trade Course</label>
											<input name="vschool" class="form-control" placeholder="Name of School" type="text" />
											<br>
											<label>Degree Course</label>
											<input name="vyeargrad" class="form-control" placeholder="Year" type="text" />
											<br>
											<label>Highest Level (if not graduated)</label>
											<input name="vhgrade" class="form-control"  type="text" />
										</div>
										<div class="col-sm-6">
											<label>From Year</label>
											<input name="vfyear" class="form-control" placeholder="Year" type="text" />
											<br>
											<label>To Year</label>
											<input name="vtyear" class="form-control" placeholder="Year" type="text" />
											<br>
											<label>Scholarship/Academic Honors</label>
											<input name="vacad" class="form-control"  type="text" />
										</div>
									</div>
								</div>
								<div class="tab-pane" id="tab4">
									<div class="form-group">
										<div class="col-sm-6">
											<label>College</label>
											<input name="cschool" class="form-control" placeholder="Name of School" type="text" />
											<br>
											<label>Degree Course</label>
											<input name="cdcschool" class="form-control"  type="text" />
											<br>
											<label>Yr. Graduated</label>
											<input name="cyeargrad" class="form-control" placeholder="Year" type="text" />
											<br>
											<label>Highest Level (if not graduated)</label>
											<input name="chgrade" class="form-control"  type="text" />
										</div>
										<div class="col-sm-6">
											<label>From Year</label>
											<input name="cfyear" class="form-control" placeholder="Year" type="text" />
											<br>
											<label>To Year</label>
											<input name="ctyear" class="form-control" placeholder="Year" type="text" />
											<br>
											<label>Scholarship/Academic Honors</label>
											<input name="cacad" class="form-control"  type="text" />
										</div>
									</div>
								</div>
								<div class="tab-pane" id="tab5">
									<div class="form-group">
										<div class="col-sm-6">
											<label>Graduate Studies</label>
											<input name="gschool" class="form-control" placeholder="Name of School" type="text" />
											<br>
											<label>Degree Course</label>
											<input name="gdcschool" class="form-control"  type="text" />
											<br>
											<label>Yr. Graduated</label>
											<input name="gyeargrad" class="form-control" placeholder="Year" type="text" />
											<br>
											<label>Highest Level (if not graduated)</label>
											<input name="ghgrade" class="form-control"  type="text" />
										</div>
										<div class="col-sm-6">
											<label>From Year</label>
											<input name="gfyear" class="form-control" placeholder="Year" type="text" />
											<br>
											<label>To Year</label>
											<input name="gtyear" class="form-control" placeholder="Year" type="text" />
											<br>
											<label>Scholarship/Academic Honors</label>
											<input name="gacad" class="form-control"  type="text" />
										</div>
									</div>
								</div>
								<div class="tab-pane" id="tab6">
									<div class="form-group">
										<div class="col-sm-6">
											<label>Graduate Studies II</label>
											<input name="g2school" class="form-control" placeholder="Name of School" type="text" />
											<br>
											<label>Degree Course</label>
											<input name="g2dcschool" class="form-control"  type="text" />
											<br>
											<label>Yr. Graduated</label>
											<input name="g2yeargrad" class="form-control" placeholder="Year" type="text" />
											<br>
											<label>Highest Level (if not graduated)</label>
											<input name="g2hgrade" class="form-control"  type="text" />
										</div>
										<div class="col-sm-6">
											<label>From Year</label>
											<input name="g2fyear" class="form-control" placeholder="Year" type="text" />
											<br>
											<label>To Year</label>
											<input name="g2tyear" class="form-control" placeholder="Year" type="text" />
											<br>
											<label>Scholarship/Academic Honors</label>
											<input name="g2acad" class="form-control"  type="text" />
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<br>
				</div>
				<input name="eID" value="<?php echo $eID; ?>" class="form-control" type="hidden">
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<button type="submit" class="btn btn-info" name="submit">Next 3/8</button>	
				</form>                          
            </div>                              
        </div>
    </div>
</div>
<?php
	if(isset($_POST['submit'])){
		$eID = $_POST['eID'];	
		$sql = "SELECT `id`, `bioID`, `surname`, `firstname`, `middlename`, `namext` FROM `personalinfo` WHERE `bioID`=$eID";
		$query = mysql_query($sql);	
		while($row = mysql_fetch_assoc($query)){
			$id = $row['id'];
			$eID = $row['bioID'];
			$fullname = $row['surname'].", ".$row['firstname']." ".$row['namext']." ".$row['middlename'];
			$newName = mysql_real_escape_string($fullname);
		}
		//Educational Background
		$eschool = mysql_real_escape_string(utf8_decode($_POST['eschool']));
		$eyeargrad = mysql_real_escape_string(utf8_decode($_POST['eyeargrad']));
		$ehgrade = mysql_real_escape_string(utf8_decode($_POST['ehgrade']));
		$efyear = mysql_real_escape_string(utf8_decode($_POST['efyear']));
		$etyear = mysql_real_escape_string(utf8_decode($_POST['etyear']));
		$eacad = mysql_real_escape_string(utf8_decode($_POST['eacad']));
		$sschool = mysql_real_escape_string(utf8_decode($_POST['sschool']));
		$syeargrad = mysql_real_escape_string(utf8_decode($_POST['syeargrad']));
		$shgrade = mysql_real_escape_string(utf8_decode($_POST['shgrade']));
		$sfyear = mysql_real_escape_string(utf8_decode($_POST['sfyear']));
		$styear = mysql_real_escape_string(utf8_decode($_POST['styear']));
		$sacad = mysql_real_escape_string(utf8_decode($_POST['sacad']));
		$vschool = mysql_real_escape_string(utf8_decode($_POST['vschool']));
		$vdcschool = mysql_real_escape_string(utf8_decode($_POST['vdcschool']));
		$vyeargrad = mysql_real_escape_string(utf8_decode($_POST['vyeargrad']));
		$vhgrade = mysql_real_escape_string(utf8_decode($_POST['vhgrade']));
		$vfyear = mysql_real_escape_string(utf8_decode($_POST['vfyear']));
		$vtyear = mysql_real_escape_string(utf8_decode($_POST['vtyear']));
		$vacad = mysql_real_escape_string(utf8_decode($_POST['vacad']));
		$cschool = mysql_real_escape_string(utf8_decode($_POST['cschool']));
		$cdcschool = mysql_real_escape_string(utf8_decode($_POST['cdcschool']));
		$cyeargrad = mysql_real_escape_string(utf8_decode($_POST['cyeargrad']));
		$chgrade = mysql_real_escape_string(utf8_decode($_POST['chgrade']));
		$cfyear = mysql_real_escape_string(utf8_decode($_POST['cfyear']));
		$ctyear = mysql_real_escape_string(utf8_decode($_POST['ctyear']));
		$cacad = mysql_real_escape_string(utf8_decode($_POST['cacad']));
		$gschool = mysql_real_escape_string(utf8_decode($_POST['gschool']));
		$gdcschool = mysql_real_escape_string(utf8_decode($_POST['gdcschool']));
		$gyeargrad = mysql_real_escape_string(utf8_decode($_POST['gyeargrad']));
		$ghgrade = mysql_real_escape_string(utf8_decode($_POST['ghgrade']));
		$gfyear = mysql_real_escape_string(utf8_decode($_POST['gfyear']));
		$gtyear = mysql_real_escape_string(utf8_decode($_POST['gtyear']));
		$gacad = mysql_real_escape_string(utf8_decode($_POST['gacad']));
		$g2school = mysql_real_escape_string(utf8_decode($_POST['g2school']));
		$g2dcschool = mysql_real_escape_string(utf8_decode($_POST['g2dcschool']));
		$g2yeargrad = mysql_real_escape_string(utf8_decode($_POST['g2yeargrad']));
		$g2hgrade = mysql_real_escape_string(utf8_decode($_POST['g2hgrade']));
		$g2fyear = mysql_real_escape_string(utf8_decode($_POST['g2fyear']));
		$g2tyear = mysql_real_escape_string(utf8_decode($_POST['g2tyear']));
		$g2acad = mysql_real_escape_string(utf8_decode($_POST['g2acad']));

		$sql = "INSERT INTO `educbg` (`id`, `bioID`, `fullname`, `eschool`, `eyeargrad`, `ehgrade`, `efyear`, `etyear`, `eacad`, `sschool`, `syeargrad`, `shgrade`, `sfyear`, `styear`, `sacad`, `vschool`, `vdcschool`, `vyeargrad`, `vhgrade`, `vfyear`, `vtyear`, `vacad`, `cschool`, `cdcschool`, `cyeargrad`, `chgrade`, `cfyear`, `ctyear`, `cacad`, `gschool`, `gdcschool`, `gyeargrad`, `ghgrade`, `gfyear`, `gtyear`, `gacad`, `g2school`, `g2dcschool`, `g2yeargrad`, `g2hgrade`, `g2fyear`, `g2tyear`, `g2acad`)
							VALUES ('$id', '$eID', '$newName', '$eschool', '$eyeargrad', '$ehgrade', '$efyear', '$etyear', '$eacad', '$sschool', '$syeargrad', '$shgrade', '$sfyear', '$styear', '$sacad', '$vschool', '$vdcschool', '$vyeargrad', '$vhgrade', '$vfyear', '$vtyear', '$vacad', '$cschool', '$cdcschool', '$cyeargrad', '$chgrade', '$cfyear', '$ctyear', '$cacad', '$gschool', '$gdcschool', '$gyeargrad', '$ghgrade', '$gfyear', '$gtyear', '$gacad', '$g2school', '$g2dcschool', '$g2yeargrad', '$g2hgrade', '$g2fyear', '$g2tyear', '$g2acad')";

		$query = mysql_query($sql);

			echo "<script type = \"text/javascript\">
									alert(\"Success!.\");
									window.location = \"index.php?page=newpds3&eID=$eID\"
								  </script>";
		}
?>

