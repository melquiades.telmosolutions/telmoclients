<?php
$id = $_GET['id'];
$select = mysql_query("SELECT * FROM `familybg` WHERE `id` = '$id'");
?>
<div class="content-box-large">
	<div class="panel-heading">
		<center><h1 class="page-head-line"><p class="text-gray-dark">PERSONAL DATA SHEET</h1></center>
		<hr>
		<div class="panel-options">
<?php
	$p = $_GET['p'];
		if($p=='personalinfo' OR $p=='fambg' OR $p=='reduc' OR $p=='cse' OR $p=='workexp' OR $p=='volwork' OR $p=='trainings' OR $p=='skills' OR $p=='recog' OR $p=='orgmem' OR $p=='search'){
			echo '<a href="index.php?page=pdsAction&id='.$id.'&p='.$p.'"class="fa arrow"><< BACK</a>';
		}else if($p=='pds'){
			echo '<a href="index.php?page='.$p.'"class="fa arrow"><< BACK</a>';
		}
?>
		</div>
	</div>
	<form action="pdsEditEd.php" method="post">	
	<div class="panel-body">
		<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered">
			<thead>
				<tr>
					<th colspan="8">II. FAMILY BACKGROUND</th>
				</tr>
			</thead>
<?php
	while ($row1 = mysql_fetch_assoc($select)){
		echo "
			<tbody>
                <tr>
                    <td>Spouse's Surname:</td>
                    <td><input name='ssurname' class='form-control' value='".htmlspecialchars(utf8_encode($row1['ssurname']), ENT_QUOTES)."' type='text'></td>
					<td>Father's Surname:</td>
					<td><input name='fsurname' class='form-control' value='".htmlspecialchars(utf8_encode($row1['fsurname']), ENT_QUOTES)."' type='text'></td>	
				</tr>
				<tr>
						<td>Firstname:</td>
						<td><input name='sfirstname' class='form-control' value='".htmlspecialchars(utf8_encode($row1['sfirstname']), ENT_QUOTES)."' type='text'></td>
						<td>Firstname:</td>
						<td><input name='ffirstname' class='form-control' value='".htmlspecialchars(utf8_encode($row1['ffirstname']), ENT_QUOTES)."' type='text'></td>
				</tr>
				<tr>
						<td>Middlename:</td>
						<td><input name='smiddlename' class='form-control' value='".htmlspecialchars(utf8_encode($row1['smiddlename']), ENT_QUOTES)."' type='text'></td>
						<td>Middlename:</td>
						<td><input name='fmiddlename' class='form-control' value='".htmlspecialchars(utf8_encode($row1['fmiddlename']), ENT_QUOTES)."' type='text'></td>
				</tr>
				<tr>
						<td>Occupation:</td>
						<td><input name='soccupation' class='form-control' value='".htmlspecialchars(utf8_encode($row1['soccupation']), ENT_QUOTES)."' type='text'></td>
						<td colspan='2'>Mother's Maiden Name</td>
				</tr>
				<tr>
						<td>Employer/Bus. Name:</td>
                        <td><input name='semployername' class='form-control' value='".htmlspecialchars(utf8_encode($row1['semployername']), ENT_QUOTES)."' type='text'></td>
						<td>Surname:</td>
						<td><input name='msurname' class='form-control' value='".htmlspecialchars(utf8_encode($row1['msurname']), ENT_QUOTES)."' type='text'></td>
				</tr>
				<tr>
					<td>Business Address:</td>
					<td><input name='sbusaddress' class='form-control' value='".htmlspecialchars(utf8_encode($row1['sbusaddress']), ENT_QUOTES)."' type='text'></td>
					<td>Firstname:</td>
					<td><input name='mfirstname' class='form-control' value='".htmlspecialchars(utf8_encode($row1['mfirstname']), ENT_QUOTES)."' type='text'></td>
				</tr>
				<tr>
					<td>Telephone No.:</td>
					<td><input name='stelephone' class='form-control' value='".htmlspecialchars(utf8_encode($row1['stelephone']), ENT_QUOTES)."' type='text'></td>
					<td>Middlename:</td>
					<td><input name='mmiddlename' class='form-control' value='".htmlspecialchars(utf8_encode($row1['mmiddlename']), ENT_QUOTES)."' type='text'></td>
				</tr>
				<tr>	
					<td>Name Of Children:<br><br>
						<input name='child1' class='form-control' value='".htmlspecialchars(utf8_encode($row1['child1']), ENT_QUOTES)."' type='text'><br>
						<input name='child2' class='form-control' value='".htmlspecialchars(utf8_encode($row1['child2']), ENT_QUOTES)."' type='text'><br>
						<input name='child3' class='form-control' value='".htmlspecialchars(utf8_encode($row1['child3']), ENT_QUOTES)."' type='text'><br>
						<input name='child4' class='form-control' value='".htmlspecialchars(utf8_encode($row1['child4']), ENT_QUOTES)."' type='text'><br>
						<input name='child5' class='form-control' value='".htmlspecialchars(utf8_encode($row1['child5']), ENT_QUOTES)."' type='text'></td>
					<td>Date of Birth(mm/dd/yyyy):<br><br>
						<input name='c1bday' class='form-control' value='".$row1['c1bday']."' type='date'><br>
						<input name='c2bday' class='form-control' value='".$row1['c2bday']."' type='date'><br>
						<input name='c3bday' class='form-control' value='".$row1['c3bday']."' type='date'><br>
						<input name='c4bday' class='form-control' value='".$row1['c4bday']."' type='date'><br>
						<input name='c5bday' class='form-control' value='".$row1['c5bday']."' type='date'></td>	
					<td>Name Of Children:<br><br>
						<input name='child6' class='form-control' value='".htmlspecialchars(utf8_encode($row1['child6']), ENT_QUOTES)."' type='text'><br>
						<input name='child7' class='form-control' value='".htmlspecialchars(utf8_encode($row1['child7']), ENT_QUOTES)."' type='text'><br>
						<input name='child8' class='form-control' value='".htmlspecialchars(utf8_encode($row1['child8']), ENT_QUOTES)."' type='text'><br>
						<input name='child9' class='form-control' value='".htmlspecialchars(utf8_encode($row1['child9']), ENT_QUOTES)."' type='text'><br>
						<input name='child10' class='form-control' value='".htmlspecialchars(utf8_encode($row1['child10']), ENT_QUOTES)."' type='text'></td>
					<td>Date of Birth(mm/dd/yyyy):<br><br>
						<input name='c6bday' class='form-control' value='".$row1['c6bday']."' type='date'><br>
						<input name='c7bday' class='form-control' value='".$row1['c7bday']."' type='date'><br>
						<input name='c8bday' class='form-control' value='".$row1['c8bday']."' type='date'><br>
						<input name='c9bday' class='form-control' value='".$row1['c9bday']."' type='date'><br>
						<input name='c10bday' class='form-control' value='".$row1['c10bday']."' type='date'></td>
				</tr>
					<input name='id' class='form-control' value='".$id."' type='hidden'>
					<input name='p' class='form-control' value='".$p."' type='hidden'>
            </tbody>";}
?>
	</table>
	<button type="submit" class="btn btn-info" name="updateFb">Update</button>
	</form>
	</div>
</div>