<?php
$id = $_GET['id'];
$select = mysql_query("SELECT * FROM `personalinfo` WHERE `id` = '$id'");
?>
<div class="content-box-large">
	<div class="panel-heading">
		<center><h1 class="page-head-line"><p class="text-gray-dark">PERSONAL DATA SHEET</h1></center>
		<hr>
		<div class="panel-options">
<?php
	$p = $_GET['p'];
		if($p=='personalinfo' OR $p=='fambg' OR $p=='reduc' OR $p=='cse' OR $p=='workexp' OR $p=='volwork' OR $p=='trainings' OR $p=='skills' OR $p=='recog' OR $p=='orgmem' OR $p=='search'){
			echo '<a href="index.php?page=pdsAction&id='.$id.'&p='.$p.'"class="fa arrow"><< BACK</a>';
		}else if($p=='pds'){
			echo '<a href="index.php?page='.$p.'"class="fa arrow"><< BACK</a>';
		}
?>
		</div>
	</div>
	<form action="pdsEditEd.php" method="post">	
	<div class="panel-body">
		<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered">
			<thead>
				<tr>
					<th colspan="8">I. PERSONAL INFORMATION</th>
				</tr>
			</thead>
<?php
	while ($row = mysql_fetch_assoc($select)){
		echo "
            <tbody>
                <tr>
                    <td colspan='4'>Surname:<br><br><br>
						Firstname:<br><br><br>
						Middlename:<br><br>
						Name Extension:</td>
                    <td colspan='2'><input name='surname' class='form-control' value='".htmlspecialchars(utf8_encode(ucwords($row['surname'])), ENT_QUOTES)."' type='text'><br>
						<input name='firstname' class='form-control' value='".htmlspecialchars(utf8_encode($row['firstname']), ENT_QUOTES)."' type='text'><br>
						<input name='middlename' class='form-control' value='".htmlspecialchars(utf8_encode($row['middlename']), ENT_QUOTES)."' type='text'><br>
						<input name='namext' class='form-control' value='".htmlspecialchars(utf8_encode($row['namext']), ENT_QUOTES)."' type='text'></td>
					<td>Office:<br><br><br><br>
						Status:<br><br><br><br>
						BIO ID:</td>
					<td>";
				if($user_level==1){				
				echo "	<select name='office' class='form-control'>
						<option value='".$row['office']."'>".strtoupper($row['office'])."</option>
						<option value='cao'>CAO - City Accountants Office</option>
						<option value='cadmo'>C ADMIN - City Administrators Office</option>
						<option value='cagrio'>C AGRI - City Agricultures Office</option>
						<option value='carchto'>C ARCHT - City Architects Office</option>
						<option value='casso'>CASSO - City Assessors Office</option>
						<option value='cbo'>CBO - City Budget Office</option>
						<option value='ccro'>CCRO - City Civil Registrars Office</option>
						<option value='cdo'>CDO - City Division Office</option>
						<option value='ceo'>CEO - City Engineers Office</option>
						<option value='cenro'>C ENRO - City Environment and Natural Resources Office</option>
						<option value='cgso'>CGSO - City General Services Office</option>
						<option value='cio'>CIO - City Information Office</option>
						<option value='cho'>CHO - City Health Office</option> 
						<option value='chcdo'>C HOUSING - City Housing and Community Development Office</option>  
						<option value='cmo'>CMO - City Mayors Office</option>         
						<option value='cpdo'>CPDO - City Planning and Development Office</option>
						<option value='cswdo'>CSWDO - City Social Welfare and Development Office</option>
						<option value='cto'>CTO - City Treasures Office</option>
						<option value='cvo'>CVO - City Veterinary Office</option>
						<option value='iaso'>IASO - Internal Audit Service office</option>
						<option value='clgoo-dilg'>CLGOO-dilg - City Local Government Operations Office/DILG</option> 
						<option value='hrmdo'>HRMDO - Human Resource Management Development Office</option>
						<option value='tomeco'>TOMECO - Traffic Operations Management Enforcement Control Office</option> 
						<option value='eemd'>EEMD - Economic Enterprise Management Division</option>
						<option value='ccdlao'>CCDLAO - City Cooperatives Dev't and Livelihood Assistance Office</option>
						<option value='peso'>PESO - Public Employment Services Office</option> 
						<option value='blpd'>BLPD - Business License and Permits Division</option>
						<option value='ctoo'>CTOO - City Tourism Operations Office</option>   
						<option value='cno'>CNO - City Nutrition Office</option> 
						<option value='masa'>Mayor Alfred Social Action</option>
						<option value='sp'>SP - Sangguniang Panlungsod</option> 
						<option value='tch'>TCH - Tacloban City Hospital</option>
						<option value='tccc'>TCCC - Tacloban City Convenction Center</option>
						<option value='tcso'>TCSO - Tacloban City Sports Office</option>
						<option value='misd'>MISD - Management Information System</option>
						<option value='cpo'>CPO - City Population Office</option>          
						<option value='clo'>CLO - City Legal Office</option>
						<option value='clep'>CLEP - Comprehensive Livelihood and Entrepreneurship Program</option>
						<option value='BAO'>BAO - Brgy. Affairs Office</option>
						<option value='BAC/SUPPLY'>BAC - Bids and Awards Committee</option>
						<option value='MASA'>MASA Office – Special Health Service</option>
						<option value='FLET'>FLET - Fisheries Law Enforcement Team</option>
						<option value='pdao'>PDAO - Persons with disability affairs office</option>
						<option value='CSSO'>CSSO - City Security Service Office</option>
						<option value='CARPOOL'>CARPOOL</option>
						<option value='osca'>OSCA - Office of Senior Citizens Affairs</option>
						<option value='CDRRMO'>CDRRMO - City Disaster Risk Reduction And Management Office</option>
						<option value='TNBT'>TNBT - Tacloban New Bus Terminal</option>
					</select><br><br>
					<select name='status' class='form-control'>
						<option value='".ucwords($row['status'])."'>".ucwords($row['status'])."</option>
						<option value='Regular'>Regular</option>
						<option value='Casual'>Casual</option>
						<option value='Retired'>Retired</option>
						<option value='Resign'>Resign</option>
					</select><br><br>
						<input name='bioID' class='form-control' value='".$row['bioID']."' type='text'></td>";
				}else{
					echo "<b>".strtoupper($row['office'])."</b><br><br><br><br>";
					echo "<b>".ucwords($row['status'])."</b><br><br><br><br>";
					echo "<b>".$row['bioID']."</b>";
				}
				echo "
					</tr>
					<tr>
						<td colspan='4'>Employee ID:</td>";
				if($user_level==1){
				echo "
						<td colspan='2'><input name='employeeID' class='form-control' value='".$row['employeeID']."' type='text'> </td>";
				}else{				
				echo "
						<td colspan='2'>".$row['employeeID']."</td>";
				}
				echo "
						<td>PAG-IBIG ID No.:</td>
						<td><input name='pagibigno' class='form-control' value='".htmlspecialchars(utf8_encode($row['pagibigno']), ENT_QUOTES)."'></td>
					</tr>
					<tr>
						<td colspan='4'>Date of Birth:</td>
                        <td colspan='2'><input name='dobirth' class='form-control' value='".$row['dobirth']."' type='date'></td>
						<td>Philhealth No.:</td>
						<td><input name='philhealthno' class='form-control' value='".htmlspecialchars(utf8_encode($row['philhealthno']), ENT_QUOTES)."' type='text'> </td>
					</tr>
					<tr>
						<td colspan='4'>Place of Birth:</td>
						<td colspan='2'><input name='pobirth' class='form-control' value='".htmlspecialchars(utf8_encode($row['pobirth']), ENT_QUOTES)."' type='text'></td>
						<td>SSS No.:</td>
						<td><input name='sssno' class='form-control' value='".htmlspecialchars(utf8_encode($row['sssno']), ENT_QUOTES)."' type='text'></td>
					</tr>
					<tr>
						<td colspan='4'>Age:</td>
						<td colspan='2'>".$age=date("Y-m-d")-$row['dobirth']."</td>
						<td>TIN:</td>
						<td><input name='tin' class='form-control' value='".htmlspecialchars(utf8_encode($row['tin']), ENT_QUOTES)."' type='text'></td>
					</tr>
					<tr>
						<td colspan='4'>Sex:</td>
						<td colspan='2'><select name='sex' class='form-control' type='text'>
										<option>".ucwords($row['sex'])."</option>
										<option>Male</option>
										<option>Female</option>
										</select></td>
						<td>Residential Address:<br><br>
									Zip Code:</td>
						<td><input name='resaddress' class='form-control' value='".htmlspecialchars(utf8_encode($row['resaddress']), ENT_QUOTES)."' type='text'>
							<input name='reszip' class='form-control' value='".htmlspecialchars(utf8_encode($row['reszip']), ENT_QUOTES)."' type='text'></td>
					<tr>
						<td colspan='4'>Civil Status:</td>
						<td colspan='2'><select name='civilstatus' class='form-control'>
							<option value='".ucwords($row['civilstatus'])."'>".ucwords($row['civilstatus'])."</option>
							<option value='Single'>Single</option>
							<option value='Married'>Married</option>
							<option value='Annulled'>Annulled</option>
							<option value='Widowed'>Widowed</option>
							<option value='Separated'>Separated</option>
							</select></td>
						<td>Telephone No.:</td>
						<td><input name='resphone' class='form-control' value='".htmlspecialchars(utf8_encode($row['resphone']), ENT_QUOTES)."' type='text'></td>
					</tr>
					<tr>
						<td colspan='4'>Citizenship:</td>
						<td colspan='2'><input name='citizenship' class='form-control' value='".htmlspecialchars(ucwords(utf8_encode($row['citizenship'])), ENT_QUOTES)."' type='text'></td>
						<td>Permanent Address:<br><br>
							Zip Code:</td>
						<td><input name='peraddress' class='form-control' value='".htmlspecialchars(utf8_encode($row['peraddress']), ENT_QUOTES)."' type='text'>
						<input name='perzip' class='form-control' value='".htmlspecialchars(utf8_encode($row['perzip']), ENT_QUOTES)."' type='text'></td>
					</tr>
					<tr>
						<td colspan='4'>Height:</td>
						<td colspan='2'><input name='height' class='form-control' value='".htmlspecialchars(utf8_encode($row['height']), ENT_QUOTES)."' type='text'></td>
						<td>Telephone No.:</td>
						<td><input name='perphone' class='form-control' value='".htmlspecialchars(utf8_encode($row['perphone']), ENT_QUOTES)."' type='text'></td>
					</tr>
					<tr>
						<td colspan='4'>Weight:</td>
						<td colspan='2'><input name='weight' class='form-control' value='".htmlspecialchars(utf8_encode($row['weight']), ENT_QUOTES)."' type='text'></td>
						<td>E-Mail Address:</td>
						<td><input name='email' class='form-control' value='".htmlspecialchars(utf8_encode($row['email']), ENT_QUOTES)."' type='text'></td>
					</tr>
					<tr>
						<td colspan='4'>Blood Type:</td>
						<td colspan='2'><input name='blood' class='form-control' value='".htmlspecialchars(utf8_encode($row['blood']), ENT_QUOTES)."' type='text'></td>
						<td>Cellphone No.:</td>
						<td><input name='cellphone' class='form-control' value='".htmlspecialchars(utf8_encode($row['cellphone']), ENT_QUOTES)."' type='text'></td>
					</tr>
					<tr>
						<td colspan='4'>GSIS ID No:</td>
						<td colspan='2'><input name='gsisno' class='form-control' value='".htmlspecialchars(utf8_encode($row['gsisno']), ENT_QUOTES)."' type='text'></td>
						<td>Agency Employee No.:</td>
						<td><input name='employeeno' class='form-control' value='".htmlspecialchars(utf8_encode($row['employeeno']), ENT_QUOTES)."' type='text'></td>
					</tr>
						<input name='id' class='form-control' value='".$id."' type='hidden'>
						<input name='p' class='form-control' value='".$p."' type='hidden'>
        </tbody>";}
?>
	</table>
	<button  type="submit" class="btn btn-info" name="submitPsi">Update</button>
	</form>
	</div>
</div>


