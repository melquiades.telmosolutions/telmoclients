<!DOCTYPE html>
<html>
  <head>
   <title>Telmo Management System</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link rel="stylesheet" type="text/css" href="../media/css/bootstrap.css">
    <!-- styles -->
    <link href="../css/styles.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="login-bg">
  	<div class="header">
	     <div class="container">
	        <div class="row">
	           <div class="col-md-12">
	              <!-- Logo -->
	              <div class="logo">
	                 <h1><a href="login.php">Telmo Management System</a></h1>
	              </div>
	           </div>
	        </div>
	     </div>
	</div>

	<div class="page-content container">
		<div class="row">
			<div class="col-md-4">
			</div>
			<div class="col-md-4 col-md-offset-4">
				<div class="login-wrapper">
			        <div class="box">
			            <div class="content-wrap">
			                <h6>REGISTER NEW EMPLOYEE:</h6>
			                <form role="form" action="" method="post">
				                <input name="employeeID" class="form-control" type="text" placeholder="EMPLOYEE ID #">
				                <input name="password" value="" class="form-control" type="password" placeholder="PASSWORD">
				                <input name="password1" value="" class="form-control" type="password" placeholder="CONFIRM PASSWORD">
				                <div class="action">
				                    <button type="submit" name="submit" class="btn btn-primary signup">REGISTER</button>
			                	</div> 
			                </form>               
			            </div>
			        </div>

			        <div class="already">
			            <p>Have an account already?</p>
			            <a href="login.php">Login</a>
			        </div>
			    </div>
			</div>
		</div>
	</div>
<?php
require_once "../db.php";
	if(isset($_POST['submit'])){
		$EmployeeID = $_POST['employeeID'];
		if($_POST['password']==""){
			echo "<script type = \"text/javascript\">
				alert(\"Please enter your password!!\");
				window.location = \"register.php\"
				</script>";
		}else{
			$password = md5($_POST['password']);
			$password1 = md5($_POST['password1']);
		}
		$searcheID = mysql_query("SELECT  * FROM `personalinfo` WHERE `employeeID` = '".$EmployeeID."' ");
		$eIDcheck = mysql_fetch_assoc($searcheID);
		$name = $eIDcheck['surname'].",".$eIDcheck['firstname']." ".$eIDcheck['middlename']." ".$eIDcheck['namext'];
		$id = $eIDcheck['id'];
		$searchuser = mysql_query("SELECT  * FROM `users` WHERE `employeeID` = '".$EmployeeID."' ");
		$usercheck = mysql_fetch_assoc($searchuser);
						
		if ($EmployeeID ==""){
			echo "<script type = \"text/javascript\">
				alert(\"Enter EmployeeID!!\");
				window.location = \"register.php\"
				</script>";
		}elseif ($EmployeeID != $eIDcheck['employeeID']){
			echo "<script type = \"text/javascript\">
				alert(\"EmployeeID Not VALID!!\");
				window.location = \"register.php\"
				 </script>";
		}else if($EmployeeID == $usercheck['employeeID']){
			echo "<script type = \"text/javascript\">
				alert(\"Employee ID already exists!!\");
				window.location = \"register.php\"
				</script>";
		}else if($password!=$password1){
			echo "<script type = \"text/javascript\">
				alert(\"Password do not match!!\");
				window.location = \"register.php\"
				</script>";
		}else{
			$sql="INSERT INTO `users` (`id`, `idx`, `employeeID`, `name`, `password`, `user_level`, `user_type`) 
									VALUES (NULL, '$id', '$EmployeeID', '$name', '$password', '3', 'a')";
			$query = mysql_query($sql);
			echo "<script type = \"text/javascript\">
				alert(\"Register Success! You can now log in.\");
				window.location = \"login.php\"
				</script>";
		}
	}
?>	
  </body>
</html>