 <footer>
         <div class="container">
         
            <div class="copy text-center">
           <a href='http://www.telmosolutions.com'>Telmo Solutions - Your Business. Our Solutions. &copy; <?php echo date("Y"); ?></a>
            </div>
            
         </div>
      </footer>

      <link href="vendors/datatables/dataTables.bootstrap.css" rel="stylesheet" media="screen">

    <script type="text/javascript" language="javascript" src="media/js/jquery.1.12.3.js">
	</script>
    <script type="text/javascript" language="javascript" src="media/js/jquery.1.12.4.js">
	</script>
	<script src="media/js/dropzone.js"></script>
	<!-- Dropzone.js -->
    <!-- <script src="vendors/dropzone/dist/min/dropzone.min.js"></script> -->
    <!-- <script src="vendors1/dropzone/dist/dropzone.js"></script> -->
	<script type="text/javascript" language="javascript" src="media/js/jquery.dataTables.min.js">
	</script>
	<script type="text/javascript" language="javascript" src="media/js/dataTables.bootstrap4.js">
	</script>
	<script type="text/javascript" language="javascript" src="media/buttons/js/dataTables.buttons.min.js">
	</script>
	<script type="text/javascript" language="javascript" src="media/buttons/js/buttons.bootstrap4.js">
	</script>
	<script type="text/javascript" language="javascript" src="media/js/excel.jszip.min.js">
	</script>
	<script type="text/javascript" language="javascript" src="media/js/pdfmake.min.js">
	</script>
	<script type="text/javascript" language="javascript" src="media/js/vfs.fonts.js">
	</script>
	<script type="text/javascript" language="javascript" src="media/buttons/js/buttons.html5.js">
	</script>
	<script type="text/javascript" language="javascript" src="media/buttons/js/buttons.print.min.js">
	</script>
	<script type="text/javascript" language="javascript" src="media/select/dataTables.select.min.js">
	</script>
	<script type="text/javascript" language="javascript" src="media/buttons/js/buttons.colVis.js">
	</script>
	<script type="text/javascript" language="javascript" src="media/resources/syntax/shCore.js">
	</script>
	<script type="text/javascript" language="javascript" src="media/resources/demo.js">
	</script>
	
	<script src="js/custom.js"></script>
	<!-- <script src="js/forms.js"></script> -->
	<script src="bootstrap/js/bootstrap.min.js"></script>
	
	
	<script type="text/javascript" language="javascript" class="init">
		$(document).ready(function() {
			var table = $('#dataTables').DataTable( {
				"sPagingType":"full_numbers",
				"bJQueryUI":true,
				"lengthMenu":[[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "All"]],
				
				lengthChange: true,
				buttons: [ {
								extend:'excel',
								text: '<font size="2">Excel',
								exportOptions: {
									columns: ':visible',
									modifier: {
										selected: true
									}
								}							
							}, 
							{
								extend:'pdf',
								text: '<font size="2">PDF',
								exportOptions: {
									columns: ':visible',
									modifier: {
										selected: true
									}
								}
							},
							{
				                extend: 'print',
								text: '<font size="2">Print',
								message: '',
								exportOptions: {
									columns: ':visible',
									modifier: {
										selected: true
									}
								}
		            		},
							{
								extend:'excel',
								text: '<font size="2">Excel All',
								exportOptions: {
									columns: ':visible',
								}
							},
							{
								extend:'pdf',
								text: '<font size="2">PDF All',
								exportOptions: {
									columns: ':visible',
								}
							},
							{
								extend:'print',
								text: '<font size="2">Print All',
								message: 'Hello',
								exportOptions: {
									columns: ':visible',
								}
							},
							{
								extend:'colvis',
								text: '<font size="2">Select'
							}
						  ],
						select: true
			} );

			table.buttons().container()
				.appendTo( '#dataTables_wrapper .col-md-6:eq(0)' );
		} );
	</script>
	<script type="text/javascript" language="javascript" class="init">
		$(document).ready(function() {
			var table = $('#userTable').DataTable( {
				"sPagingType":"full_numbers",
				"bJQueryUI":true,
				"lengthMenu":[[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "All"]],
				"order": [[ 2, "asc" ],
                         [ 1, "asc" ]],
				lengthChange: true,
				buttons: [ {
								extend:'excel',
								text: '<font size="2">Excel',
								exportOptions: {
									columns: ':visible',
									modifier: {
										selected: true
									}
								}							
							}, 
							{
								extend:'pdf',
								text: '<font size="2">PDF',
								exportOptions: {
									columns: ':visible',
									modifier: {
										selected: true
									}
								}
							},
							{
				                extend: 'print',
								text: '<font size="2">Print',
								message: '',
								exportOptions: {
									columns: ':visible',
									modifier: {
										selected: true
									}
								}
		            		},
							{
								extend:'excel',
								text: '<font size="2">Excel All',
								exportOptions: {
									columns: ':visible',
								}
							},
							{
								extend:'pdf',
								text: '<font size="2">PDF All',
								exportOptions: {
									columns: ':visible',
								}
							},
							{
								extend:'print',
								text: '<font size="2">Print All',
								message: 'Hello',
								exportOptions: {
									columns: ':visible',
								}
							},
							{
								extend:'colvis',
								text: '<font size="2">Select'
							}
						  ],
						select: true
			} );

			table.buttons().container()
				.appendTo( '#userTable_wrapper .col-md-6:eq(0)' );
		} );
	</script>

	<script type="text/javascript" language="javascript" class="init">
			$.fn.dataTable.ext.search.push(
			    function( settings, data, dataIndex ) {
			        var min = parseInt( $('#min').val(), 10 );
			        var max = parseInt( $('#max').val(), 10 );
			        var age = parseFloat( data[4] ) || 0; // use data for the age column
			 
			        if ( ( isNaN( min ) && isNaN( max ) ) ||
			             ( isNaN( min ) && age <= max ) ||
			             ( min <= age   && isNaN( max ) ) ||
			             ( min <= age   && age <= max ) )
			        {
			            return true;
			        }
			        return false;
			    }
			);
		$(document).ready(function() {
			
			var table = $('#search').DataTable( {
				responsive: true,
				"info":true,
				"bFilter": true,
				"pagingType":"full_numbers",
				"bJQueryUI":true,
				"lengthMenu":[[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
				lengthChange: true,
				initComplete: function () {
		            this.api().columns('.select-filter').every( function () {
		                var column = this;
		                var select = $('<select><option value=""></option></select>')
		                    .appendTo( $(column.footer()).empty() )
		                    .on( 'change', function () {
		                        var val = $.fn.dataTable.util.escapeRegex(
		                            $(this).val()
		                        );
		 
		                        column
		                            .search( val ? '^'+val+'$' : '', true, false )
		                            .draw();
		                    } );
		 
		                column.data().unique().sort().each( function ( d, j ) {
		                    select.append( '<option value="'+d+'">'+d+'</option>' )
		                } );
		            } );
		        },
				buttons: [ {
								extend:'excel',
								text: '<font size="2">Excel',
								exportOptions: {
									columns: ':visible',
									modifier: {
										selected: true
									}
								}
							}, 
							{
								extend:'pdf',
								text: '<font size="2">PDF',
								exportOptions: {
									columns: ':visible',
									modifier: {
										selected: true
									}
								}
							},
							{
				                extend: 'print',
								text: '<font size="2">Print',
								message: 'hello',
								exportOptions: {
									columns: ':visible',
									modifier: {
										selected: true
									}
								}
		            		},
							{
								extend:'excel',
								text: '<font size="2">Excel All',
								exportOptions: {
									columns: ':visible',
								}
							},
							{
								extend:'pdf',
								text: '<font size="2">PDF All',
								exportOptions: {
									columns: ':visible',
								}
							},
							{
								extend:'print',
								text: '<font size="2">Print All',
								message: 'Hello',
								exportOptions: {
									columns: ':visible',
								}
							},
							{
								extend:'colvis',
								text: '<font size="2">Select'
							}
						  ],
						select: true
			} );

			table.buttons().container()
				.appendTo( '#search_wrapper .col-md-6:eq(0)' );

			$('#min, #max').keyup( function() {
		        table.draw();
		    } );
	} );
</script>
<script type="text/javascript" language="javascript" class="init">
	$(document).ready(function() {
			var table = $('table.display').DataTable( {
				"info":false,
				"bFilter": false,
				"paging":false,
				"ordering":false,
				"bJQueryUI":true,
				"lengthMenu":[[ -1], [ "All"]],
				initComplete: function () {
		            this.api().columns('.select-filter').every( function () {
		                var column = this;
		                var select = $('<select><option value=""></option></select>')
		                    .appendTo( $(column.header()).empty() )
		                    .on( 'change', function () {
		                        var val = $.fn.dataTable.util.escapeRegex(
		                            $(this).val()
		                        );
		 
		                        column
		                            .search( val ? '^'+val+'$' : '', true, false )
		                            .draw();
		                    } );
		 
		                column.data().unique().sort().each( function ( d, j ) {
		                    select.append( '<option value="'+d+'">'+d+'</option>' )
		                } );
		            } );
		        },
				// lengthChange: false,
				// buttons: [ 
				// 			{
				// 				extend:'excel',
				// 				text: '<font size="2">Excel',
				// 				exportOptions: {
				// 					columns: ':visible',
				// 				}
				// 			},
				// 			{
				// 				extend:'pdf',
				// 				text: '<font size="2">PDF',
				// 				exportOptions: {
				// 					columns: ':visible',
				// 				}
				// 			},
				// 			{
				// 				extend:'print',
				// 				text: '<font size="2">Print',
				// 				message: 'Hello',
				// 				exportOptions: {
				// 					columns: ':visible',
				// 				}
				// 			},
				// 			{
				// 				extend:'colvis',
				// 				text: '<font size="2">Select'
				// 			}
				// 		  ],
						select: true
			} );

			table.buttons().container()
				.appendTo( '#dtrTable_wrapper .col-md-6:eq(0)' );
		} );	
</script>

  </body>
</html>